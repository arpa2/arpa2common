#
#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020 Adriaan de Groot <groot@kde.org>
#   SPDX-FileCopyrightText: Copyright 2024 Rick van Rein <rick@openfortress.nl>



# Create test programs that encrypt and decrypt with a2bc_* calls
# but that might also skip the encryption and take in a file
#
add_executable (aes256cmp a2bc-compare-aes256.c)
target_link_libraries (aes256cmp cipher)


add_test (a2bc-aes256-1
	aes256cmp
	08f7a63a33b559fb253ca633ac44fe3e
	+fdba1b5b764bc48994f59e66c5038e93bed6e34b5b47aeb719a6e36af3fe0659
	-fdba1b5b764bc48994f59e66c5038e93bed6e34b5b47aeb719a6e36af3fe0659
	08f7a63a33b559fb253ca633ac44fe3e
)

# NIST test vectors for AES come from this publication:
# https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Algorithm-Validation-Program/documents/aes/AESAVS.pdf
# The test key in D.3 has 60 hex digits 0, assuming the others are also 0
add_test (a2bc-aes256-nist-1
	aes256cmp
	80000000000000000000000000000000
	+0000000000000000000000000000000000000000000000000000000000000000
	ddc6bf790c15760d8d9aeb6f9a75fd4e
)
add_test (a2bc-aes256-nist-last
	aes256cmp
	ffffffffffffffffffffffffffffffff
	+0000000000000000000000000000000000000000000000000000000000000000
	acdace8078a32b1a182bfa4987ca1347
)

	

# 
# # A macro to quickly add a test for a SHA256-named file
# #
# macro(sha256test _FILE)
# 	get_filename_component (_HASH "${_FILE}" NAME_WE)
# 	string (SUBSTRING "${_HASH}" 0 8 _HPRE)
# 	add_test (
# 		NAME sha256cmp-${_HPRE}
# 		COMMAND sha256cmp "${_FILE}" "${_HASH}"
# 	)
# endmacro ()
# 
# 
# # Iterate over the sha256 directory, for .test files and
# # turn them into a test, using the main part of the file name
# # as the expected hash over their contents.
# #
# file (GLOB HASHFILES "${CMAKE_CURRENT_SOURCE_DIR}/sha256/*.test")
# foreach (HASHFILE IN LISTS HASHFILES)
# 	sha256test ("${HASHFILE}")
# endforeach ()
