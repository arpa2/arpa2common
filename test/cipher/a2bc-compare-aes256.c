/* a2bc-compare-aes256.c -- Encrypt and decrypt with comparison.
 *
 * General format, though currently only in a trivial pattern:
 *  -  hex32 representation for the data block (1st sets, the rest compares)
 *  - +hex64 for encryption with the given key
 *  - -hex64 for decryption with the given key
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#undef  A2MD_STYLE
#undef  A2MD_STYLE_DEFAULT
#define A2MD_STYLE A2MD_STYLE_LIBSSL_SHA256

#undef  A2BC_STYLE
#undef  A2BC_STYLE_DEFAULT
#define A2BC_STYLE A2BC_STYLE_LIBSSL_AES

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <uuid/uuid.h>

#include <arpa2/digest.h>
#include <arpa2/digest_sha256.h>
#include <arpa2/cipher.h>


bool hex2bytes (char *hex, uint8_t *bytes) {
	while (*hex) {
		unsigned hexval;
		if (sscanf (hex, "%02x", &hexval) != 1) {
			return false;
		}
		*bytes++ = hexval;
		hex += 2;
	}
	return true;
}


void hexaccu (uint8_t *accu0) {
	fprintf (stderr, "Accumulated outcome: %02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\n",
				accu0 [ 0], accu0 [ 1], accu0 [ 2], accu0 [ 3],
				accu0 [ 4], accu0 [ 5], accu0 [ 6], accu0 [ 7],
				accu0 [ 8], accu0 [ 9], accu0 [10], accu0 [11],
				accu0 [12], accu0 [13], accu0 [14], accu0 [15]);
}


void hexerr (char *msg, char *prompt1, uint8_t *data1, char *prompt2, uint8_t *data2) {
	fprintf (stderr, "ERROR: %s\n", msg);
	fprintf (stderr, " - %s: %02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\n",
				prompt1,
				data1 [ 0], data1 [ 1], data1 [ 2], data1 [ 3],
				data1 [ 4], data1 [ 5], data1 [ 6], data1 [ 7],
				data1 [ 8], data1 [ 9], data1 [10], data1 [11],
				data1 [12], data1 [13], data1 [14], data1 [15]);
	fprintf (stderr, " - %s: %02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x\n",
				prompt2,
				data2 [ 0], data2 [ 1], data2 [ 2], data2 [ 3],
				data2 [ 4], data2 [ 5], data2 [ 6], data2 [ 7],
				data2 [ 8], data2 [ 9], data2 [10], data2 [11],
				data2 [12], data2 [13], data2 [14], data2 [15]);
}


int main (int argc, char *argv []) {
	//
	// Check arguments
	char *prog = argv [0];
	if (argc < 3) {
		fprintf (stderr, "Usage: %s hex32accu +hex64enckey|-hex64deckey|hex32accu...\n", prog);
		exit (1);
	}
	uint8_t data [16];
	if ((strlen (argv [1]) != 32) || !hex2bytes (argv [1], data)) {
		fprintf (stderr, "%s: Syntax error: Data could not be parsed as a hex32 from %s", argv [0], argv [1]);
		exit (1);
	}
	//
	// Check constants
	if ((A2BC_BLOCK_SIZE != 16) || (A2BC_KEY_SIZE != 32)) {
		fprintf (stderr, "%s: Fixated test for blocks of 16 bytes and keys of 32 bytes; got %d and %d\n", argv [0], A2BC_BLOCK_SIZE, A2BC_KEY_SIZE);
		exit (1);
	}
	//
	// Iterate over arguments (even check the one we just parsed)
	bool ok = true;
	for (int argi = 1; argi < argc; argi++) {
		char *arg = argv [argi];
		unsigned arglen = strlen (arg);
		fprintf (stderr, "Processing argument: %s # %d\n", arg, arglen);
		//
		// Check the argument length to be either short or long
		if ((arglen != 32) && (arglen != 65)) {
discontinue:
			fprintf (stderr, "%s: Syntax error: arguments are hex32, +hex64, -hex64, unlike %s\n", argv [0], arg);
			ok = false;
			continue;
		}
		//
		// Toggle behaviour between uuid, key+key, key-key
		char form;
		if (arglen == 32) {
			form = '?';
		} else if (*arg == '+') {
			form = '+';
			arg++;
		} else if (*arg == '-') {
			form = '-';
			arg++;
		} else {
			ok = false;
			goto discontinue;
		}
		//
		// Parse the 32 or 64 bytes
		uint8_t binarg [64];
		if (!hex2bytes (arg, binarg)) {
			fprintf (stderr, "%s: Not a hex string: %s\n", argv [0], arg);
			ok = false;
			goto discontinue;
		}
		//
		// Now toggle depending on command
		uint8_t work [16];
		/*uint8_t inkey [32];*/
		struct a2bc_key keyschedule;
		memset (&keyschedule, 0, sizeof (keyschedule));
		switch (form) {
		case '?':
			/* compare data */
			if (memcmp (binarg, data, 16) != 0) {
				hexerr ("Difference between computed and expected accumulator data",
						"computed accumulator", data,
						"expected accumulator", binarg);
				ok = false;
			}
			break;
		case '+':
			if (!a2bc_setkey (binarg, A2BC_KEY_SIZE, &keyschedule)) {
				fprintf (stderr, "ERROR: Failed to set key for encryption\n");
				ok = false;
			}
			a2bc_encrypt (&keyschedule, data, work);
			a2bc_encrypt (&keyschedule, data, data);
			if (memcmp (data, work, A2BC_BLOCK_SIZE) != 0) {
				hexerr ("Difference in comparison between separate and in-place encryption",
						"separate", work,
						"in-place", data);
				ok = false;
			}
			break;
		case '-':
			if (!a2bc_setkey (binarg, A2BC_KEY_SIZE, &keyschedule)) {
				fprintf (stderr, "ERROR: Failed to set key for decryption\n");
				ok = false;
			}
			a2bc_decrypt (&keyschedule, data, work);
			a2bc_decrypt (&keyschedule, data, data);
			if (memcmp (data, work, A2BC_BLOCK_SIZE) != 0) {
				hexerr ("Difference in comparison between separate and in-place decryption",
						"separate", work,
						"in-place", data);
				ok = false;
			}
			break;
		}
		hexaccu (data);
	}
	if (!ok) {
		fprintf (stderr, "%s: Not an overall success\n", prog);
		exit (1);
	}
	//
	// All went well!
	return 0;
}
