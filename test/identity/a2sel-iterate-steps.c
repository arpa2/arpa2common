/* Count abstractions for iterated ARPA2 Selectors.
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/identity.h>
#include <arpa2/except.h>


bool test_parse (char *instr, a2sel_t *tsel) {
	bool ok = a2sel_parse (tsel, instr, strlen (instr));
	log_debug ("%s - %s", ok ? "OK" : "KO", instr);
	log_debug ("   = %s", tsel->txt);
	if (ok) a2sel_detail ("   ", tsel);
	return ok;
}

bool test_abstractions (a2sel_t *left, a2sel_t *right, int expect) {
	uint16_t steps;
	bool stepped_up = a2sel_abstractions (left, right, &steps);
	bool ok;
	if (expect < 0) {
		ok = !stepped_up;
	} else if (expect == 0) {
		ok = stepped_up && (steps == 0);
	} else /* expect > 0 */ {
		ok = stepped_up && (steps  > 0);
	}
	if (!ok) {
		log_error ("Failed on rough abstraction steps; expected %d got %d and %d\nwhile testing %s [???] %s", expect, stepped_up, steps, left->txt, right->txt);
	}
	return ok;
}

int main (int argc, char *argv []) {
	bool ok = true;
	//
	// Test arguments
	if (argc < 2) {
		fprintf (stderr, "Usage: %s <user>@<domain>...\n", argv [0]);
		exit (1);
	}
	//
	// Parse all selectors that were supplied
	a2sel_t tsel [argc];
	for (int argi = 1; argi < argc; argi++) {
		ok = test_parse (argv [argi], tsel + argi) && ok;
	}
	//
	// This will not work with open-ended iterators
	#ifdef ITERATOR_OPEN_ENDED
	log_error ("This test cannot work with open-ended iterators");
	#endif
	//
	// Check that the counts are proper for all iterations
	for (int argi = 1; argi < argc; argi++) {
		for (int argj = 1; argj <argc; argj++) {
			/* expect <0 for impossible, ==0 for same, >0 for higher */
			int expect = argj - argi;
			ok = test_abstractions (&tsel [argi], &tsel [argj], expect) && ok;
		}
	}
	//
	// Exit value
	return (ok ? 0 : 1);
}

