/* Test parser for ARPA2 Group/Member Identities.
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/identity.h>
#include <arpa2/except.h>


int main (int argc, char *argv []) {
	bool ok = true;
	a2id_init ();
	//
	// Test arguments
	if (argc != 5) {
		fprintf (stderr, "Usage: %s <user>@<domain> <nmemb> <userid> <plus_aliases>\n", argv [0]);
		exit (1);
	}
	//
	// Parse arguments
	const char *userid_exp = argv [3];
	const char *palias_exp = argv [4];
	int nmemb = atoi (argv [2]);
	bool success_exp = (nmemb >= 0);
	if (!success_exp) {
		nmemb = -nmemb;
	}
	//
	// Run the parser
	a2act_t act;
	bool success_act = a2act_parse (&act, argv [1], 0, nmemb);
printf ("success_act = %d, success_exp = %d\n", success_act, success_exp);
	ok = ok && (success_act == success_exp);
	if (success_act) {
		int useridlen_exp = strlen (userid_exp);
		int paliaslen_exp = strlen (palias_exp);
		int useridlen_act = act.ofs [A2ID_OFS_PLUS_ALIASES] - act.ofs [A2ID_OFS_USERID];
		int paliaslen_act = act.ofs [A2ID_OFS_OPEN_END] - act.ofs [A2ID_OFS_PLUS_ALIASES];
		const char *userid_act = act.txt + 0;
		const char *palias_act = act.txt + useridlen_act;
printf ("userid_act = %.*s, userid_exp = %.*s\n", useridlen_act, userid_act, useridlen_exp, userid_exp);
printf ("palias_act = %.*s, palias_exp = %.*s\n", paliaslen_act, palias_act, paliaslen_exp, userid_exp);
		ok = ok && (useridlen_exp == useridlen_act);
		ok = ok && (paliaslen_exp == paliaslen_act);
		ok = ok && (0 == memcmp (userid_exp, userid_act, useridlen_act));
		ok = ok && (0 == memcmp (palias_exp, palias_act, paliaslen_act));
	}
	//
	// Exit value
	return (ok ? 0 : 1);
}
