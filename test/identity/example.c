/* example code for IDENTITY.MD */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include <unistd.h>
#include <fcntl.h>

#include <arpa2/identity.h>

int main (int argc, char *argv []) {
assert (argc >= 3);

//
// Initialise arpa2identity
a2id_init ();
//
// Initialise the key for signatures
int keyfd = open ("@CMAKE_CURRENT_SOURCE_DIR@/test.key", O_RDONLY);
if (keyfd >= 0) { 
        a2id_addkey (keyfd);
        close (keyfd);
}


//
// Assume a NUL-terminated remote and local user@domain.name
const char *remote = argv [1];
const char *local  = argv [2];
//
// Parse the identities
bool ok = true;
a2id_t rid, lid;
ok = ok && a2id_parse (&rid, remote, 0);
ok = ok && a2id_parse (&lid, local,  0);
//
// Check basic constraints imposed by the local address
ok = ok && a2id_verify (&lid, a2id_sigdata_base, &rid);
//
// Report whether communication can continue
printf ("Communicating from %s to %s\n", rid.txt, lid.txt);
printf ("%s this address combination\n", ok ? "Accepted" : "Rejected");
//
// Report on signature checks (if any)
if (lid.sigflags & A2ID_SIGFLAG_EXPIRATION) {
	printf ("Local address expiration day %d\n", lid.expireday);
	printf ("Local domain is restricted by the signature\n");
	printf ("Local userid is restricted by the signature\n");
}
if (lid.sigflags & A2ID_SIGFLAG_REMOTE_DOMAIN) {
	printf ("Remote domain is restricted by the signature\n");
}
if (lid.sigflags & A2ID_SIGFLAG_REMOTE_USERID) {
	printf ("Remote userid is restricted by the signature\n");
}
//
// A wrapper around a2id_sigdata_base could add application restrictions
if (lid.sigflags & A2ID_SIGFLAG_SESSIONID) {
	printf ("Application session is restricted by the signature\n");
}
if (lid.sigflags & A2ID_SIGFLAG_SUBJECT) {
	printf ("Application subject is restricted by the signature\n");
}
if (lid.sigflags & A2ID_SIGFLAG_TOPIC) {
	printf ("Application topic   is restricted by the signature\n");
}
//
// Now check if the rid is granted the requested access



//
// Cleanup
a2id_fini ();


}
