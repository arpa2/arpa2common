/* Test parser for ARPA2 Selectors.
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/identity.h>
#include <arpa2/except.h>


bool test (char *instr) {
	a2sel_t tsel;
	bool ok = a2sel_parse (&tsel, instr, strlen (instr));
	log_debug ("%s - %s", ok ? "OK" : "KO", instr);
	log_debug ("   = %s", tsel.txt);
	if (ok) a2sel_detail ("   ", &tsel);
	return ok;
}

int main (int argc, char *argv []) {
	bool ok = true;
	//
	// Test arguments
	if (argc < 2) {
		fprintf (stderr, "Usage: %s <user>@<domain>...\n", argv [0]);
		exit (1);
	}
	//
	// Iterate over all identities supplied
	for (int argi = 1; argi < argc; argi++) {
		if (!test (argv [argi])) {
			ok = false;
		}
	}
	//
	// Exit value
	return (ok ? 0 : 1);
}
