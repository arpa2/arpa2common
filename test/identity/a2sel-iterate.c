/* Iterate ARPA2 Selectors.
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/identity.h>
#include <arpa2/except.h>


bool test_parse (char *instr, a2sel_t *tsel) {
	bool ok = a2sel_parse (tsel, instr, strlen (instr));
	log_debug ("%s - %s", ok ? "OK" : "KO", instr);
	log_debug ("   = %s", tsel->txt);
	if (ok) a2sel_detail ("   ", tsel);
	return ok;
}

bool test_iterate (a2sel_t *tsel, int tselctr) {
	printf ("Iterating %d levels inclusive from %s\n", tselctr, tsel->txt);
	a2sel_t iter;
	bool ok = true;
	a2sel_t *tseli = tsel;
	if (a2sel_iterate_init (tsel, &iter)) do {
		bool ok2 = true;
		if (tselctr-- < 1) {
			log_debug ("Fewer arguments than iteration levels");
			ok2 = false;
		} else {
			a2sel_detail ("IT ", &iter);
			a2sel_detail ("IN ", tseli);
		}
		if (ok2 && (strstr ("+@", iter.txt) != NULL)) {
			log_error ("Unsupported: An open-ended iterator");
			ok2 = false;
		}
		if (ok2 && (0 != memcmp (&iter, tseli, tselctr))) {
			log_debug ("Iterated to another value than provided");
			ok2 = false;
		}
		printf ("%s on matching iterator %s\n", ok2 ? "OK" : "KO", iter.txt);
		ok = ok && ok2;
		tseli++;
	} while (a2sel_iterate_next (tsel, &iter));
	if (ok && (tselctr > 0)) {
		log_debug ("More arguments than iteration levels");
		ok = false;
	}
	return ok;
}

int main (int argc, char *argv []) {
	bool ok = true;
	//
	// Test arguments
	if (argc < 2) {
		fprintf (stderr, "Usage: %s <user>@<domain>...\n", argv [0]);
		exit (1);
	}
	//
	// Parse all selectors that were supplied
	a2sel_t tsel [argc];
	for (int argi = 1; argi < argc; argi++) {
		ok = test_parse (argv [argi], tsel + argi) && ok;
	}
	//
	// This will not work with open-ended iterators
	#ifdef ITERATOR_OPEN_ENDED
	log_error ("This test cannot work with open-ended iterators");
	#endif
	//
	// Check that each selector iterates to find the next
	for (int argi = 1; argi < argc; argi++) {
		ok = test_iterate (tsel + argi, argc - argi) && ok;
	}
	//
	// Exit value
	return (ok ? 0 : 1);
}

