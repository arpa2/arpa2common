/* Iterate to list Group Members.
 *
 * Command arguments:
 *  - three filter strings
 *  - <group>+<member>@<domain> for the sender
 *  - required_marks and forbidden marks, each starting with %
 *  - the Ruleset is a single Rule
 *  - the remainder offers pairs of a member and their delivery address
 *
 * The command calls group_iterate and ensures that it matches the
 * expectations.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>

#ifndef LOG_STYLE
#define LOG_STYLE LOG_STYLE_STDERR
#endif

#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/rules.h>
#include <arpa2/group.h>


struct updata {
	int member_argc;
	char **member_argv;
	bool bad;
};


bool upcall (
                void *updata,
                group_marks marks,
                const a2act_t *sender,
                const a2act_t *recipient,
                const char *delivery,
                unsigned deliverylen) {
	//
	// Log a message
	printf ("Callback for recipient %s, delivery address %.*s\n", recipient->txt, deliverylen, delivery);
	//
	// Map the updata structure
	struct updata *args = (struct updata *) updata;
	//
	// Find the member name and delivery address
	const char *member_exp = "";
	const char *delivery_exp = "";
	if (args->member_argc >= 2) {
		member_exp   = *args->member_argv++;
		delivery_exp = *args->member_argv++;
	} else {
		args->bad = true;
	}
	args->member_argc -= 2;
	unsigned memberlen_exp   = strlen (member_exp  );
	unsigned deliverylen_exp = strlen (delivery_exp);
	//
	// Derive the member name from the recipient
	const char *member_act = recipient->txt + recipient->ofs [A2ID_OFS_ALIASES];
	unsigned memberlen_act = recipient->ofs [A2ID_OFS_OPEN_END] - recipient->ofs [A2ID_OFS_ALIASES];
	//
	// Test the member name
	if ((memberlen_act != memberlen_exp) || (memcmp (member_act, member_exp, memberlen_act) != 0)) {
		fprintf (stderr, "Bad member name, expected %.*s got %.*s\n", memberlen_exp, member_exp, memberlen_act, member_act);
		args->bad = true;
	}
	//
	// Test the delivery address
	if ((deliverylen != deliverylen_exp) || (memcmp (delivery, delivery_exp, deliverylen) != 0)) {
		fprintf (stderr, "Bad delivery address, expected %.*s got %.*s\n", deliverylen_exp, delivery_exp, deliverylen, delivery);
		args->bad = true;
	}
	//
	// Consider delivery
	if (!args->bad) {
		printf ("Delivery to member %.*s (or %s) at %.*s\n", memberlen_act, member_act, recipient->txt, deliverylen, delivery);
	}
	//
	// Continue iteration
	return true;
}


group_marks parse_marks (const char *arg) {
	if (*arg++ != '%') {
		fprintf (stderr, "Start %%MARKS with a percent: %s\n", arg);
		exit (1);
	}
	group_marks retval = 0;
	while (*arg != '\0') {
		if ((*arg < 'A') || (*arg > 'Z')) {
			fprintf (stderr, "Illegal marking character %%%c\n", *arg);
			exit (1);
		}
		retval |= (1 << (*arg++ - 'A'));
	}
	return retval;
}


int main (int argc, char *argv []) {
	//
	// Initialiase
	a2id_init ();
	group_init ();
	//
	// Parse arguments
	if ((argc < 8) || (argc % 2 != 0)) {
		fprintf (stderr, "%s flt1 flt2 flt3 sender %%REQUIRED %%FORBIDDEN grouprule [member delivery...]\n", argv [0]);
		exit (1);
	}
	const char *filters [4];
	filters [0] = argv [1];
	filters [1] = argv [2];
	filters [2] = argv [3];
	filters [3] = NULL;
	a2act_t sender;
	if (!a2act_parse (&sender, argv [4], 0, 1)) {
		log_errno ("Failed to parse sender address %s", argv [4]);
		exit (1);
	}
	group_marks required  = parse_marks (argv [5]);
	group_marks forbidden = parse_marks (argv [6]);
	const char *rule = argv [7];
	unsigned rulelen = strlen (rule) + 1;
	//
	// Construct updata
	struct updata updata;
	updata.member_argc = argc - 8;
	updata.member_argv = argv + 8;
	updata.bad = false;
	//
	// Group Iteration
	bool ok = group_iterate (&sender, filters,
			required, forbidden,
			NULL, 0,
			rule, rulelen,
			upcall, &updata);
	if (!ok) {
		log_errno ("Failed to iterate");
		exit (1);
	}
	if (updata.member_argc > 0) {
		fprintf (stderr, "Did not use %d arguments\n", updata.member_argc);
		updata.bad = true;
	} else if (updata.member_argc < 0) {
		fprintf (stderr, "Missed %d arguments\n", -updata.member_argc);
		updata.bad = true;
	}
	if (updata.bad) {
		fprintf (stderr, "Failing on foregoing errors\n");
		exit (1);
	}
	//
	// Cleanup and exit
	group_fini ();
	a2id_fini ();
	exit (0);
}

