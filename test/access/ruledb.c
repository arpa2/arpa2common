/* ruledb.c -- Test program to add/delete rules
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <arpa2/digest.h>
#include <arpa2/access.h>
#include <arpa2/access_comm.h>

#include "arpa2/rules_db.h"



/* Prepare one big call to rules_dbadd.  If argv [3] starts
 * with '@' parse the rest as a Remote Selector and use that;
 * otherwise, just use argv [3] like the others.  Arguments are
 * independent rules to combine to one ruleset.
 */
int main_add (int argc, char *argv []) {
	//
	// Parse argv [1] as the local domain name
	// Parse argv [2] as the local userid
	// Possibly parse argv [3] as Remote Selector
	if (argc < 3) {
		fprintf (stderr, "Usage: %s local.dom luserid [@selector] rule...\n", argv [0]);
		return 1;
	}
	char *domain = argv [1];
	char *xskey  = argv [2];
	bool use_remote = (argc >= 4) && (*argv [3] == '@');
	int rule0 = use_remote ? 4 : 3;
	if (use_remote && (argc != 5)) {
		fprintf (stderr, "You can only specify one rule after @selector\n");
		return 1;
	}
	a2sel_t remote;
	if (use_remote) {
		assert (a2sel_parse (&remote, argv [3] + 1, 0));
	}
	//
	// Determine total lenght of the argv [argi]
	unsigned ruleslen = 0;
	for (int argi = rule0; argi < argc; argi++) {
		int arglen = strlen (argv [argi]) + 1;
		ruleslen += arglen;
log_debug ("Rule has arglen=%d, total now %d", arglen, ruleslen);
	}
	//
	// Construct the ruleset as a contiguous sequence
	char rules [ruleslen];
	ruleslen = 0;
	for (int argi = rule0; argi < argc; argi++) {
		int arglen = strlen (argv [argi]) + 1;
		memcpy (rules + ruleslen, argv [argi], arglen);
log_debug ("Added rule %s", rules + ruleslen);
		ruleslen += arglen;
log_debug ("Rule has arglen=%d, total now %d", arglen, ruleslen);
	}
	assert (ruleslen == sizeof (rules));
	//
	// Prepare the ACLdb key
	rules_dbkey prekey1, prekey2;
	assert (rules_dbkey_domain (prekey1, "", 0, domain));
	assert (rules_dbkey_service (prekey2, prekey1, sizeof (prekey1), access_type_comm));
	//
	// Add the ruleset
	assert (rules_dbadd (prekey2, sizeof (prekey2),
				xskey,
				rules, ruleslen,
				use_remote ? &remote : NULL));
	//
	// If we got this far we were successful
	return 0;
}


int main (int argc, char *argv []) {
	return main_add (argc, argv);
}
