/* Test that www.example.com translates to example.com
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "arpa2/digest.h"
#include "arpa2/identity.h"
#include "arpa2/access.h"


int main (int _argc, char *_argv []) {
	const char *realm = "www.example.com";
	const char *hostnm = "example.com";
	const char *derived = access_host2domain (realm);
	if (derived == NULL) {
		printf ("Unexpected result NULL\n");
		exit (1);
	}
	if (strcmp (hostnm, derived) != 0) {
		printf ("Expected %s, got %s\n", hostnm, derived);
		exit (1);
	}
	return 0;
}

