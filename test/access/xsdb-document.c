/* Test Rules DB Document Access: Compute and compare Access Rights.
 *
 * The commandline provides a Remote Identity, and Access Name
 * exapected Access Rights, * and then lists ACL rules as
 * additional strings.  In case of failures or differences in
 * outcome, the program yields exit(1).
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/rules_db.h>
#include <arpa2/access_document.h>


void cleanup_testdb (void) {
	char *envdir = getenv ("ARPA2_RULES_DIR");
	const char *possible_filenames [] = { "data.mdb", "lock.mdb", NULL };
	int rulesdir = open (envdir, O_DIRECTORY);
	if (rulesdir >= 0) {
		for (const char **filename = possible_filenames; *filename != NULL; filename++) {
			if (unlinkat (rulesdir, *filename, 0) != 0) {
				perror ("Failed to remove file");
			}
		}
	}
	close (rulesdir);
	if (unlinkat (-1, envdir, AT_REMOVEDIR) != 0) {
		perror ("Failed to cleanup Rules DB after test");
		exit (1);
	}
}


bool setup_testdb (char *mkdb_script) {
	static char envdir [100];
	strcpy (envdir, "/tmp/xsdb-docu-XXXXXX");
	char *madedir = mkdtemp (envdir);
	if (madedir == NULL) {
		perror ("Failed to create ARPA2_RULES_DIR");
		return false;
	}
	if (setenv ("ARPA2_RULES_DIR", madedir, 1) != 0) {
		perror ("Failed to setenv ARPA2_RULES_DIR");
		return false;
	}
	atexit (cleanup_testdb);
	if (system (mkdb_script) != 0) {
		perror ("Failed to run script on ARPA2_RULES_DIR");
		return false;
	}
	return true;
}


bool make_svckey (const char *domain, rules_dbkey svckey) {
	bool ok = true;
	rules_dbkey domkey;
	ok = ok && rules_dbkey_domain (domkey,
				"", 0,
				domain);
	ok = ok && rules_dbkey_service (svckey,
				domkey, sizeof (domkey),
				access_type_document);
	return ok;
}


int main (int argc, char *argv []) {
	//
	// Forget any live Rules DB
	unsetenv ("ARPA2_RULES_DIR");
	//
	// Parse arguments
	a2id_t local_exp;
	if (argc != 7) {
		fprintf (stderr, "Usage: %s <domain> <remote> <access_name> RIGHTS <actor> <dbsetup.sh>\n", argv [0]);
		exit (1);
	}
	bool ok = true;
	//
	// Parse hexcode of the service key
	rules_dbkey svckey;
	const int svckeylen = sizeof (svckey);
	ok = ok && make_svckey (argv [1], svckey);
	//
	// Start by running the database setup script
	ok = ok && setup_testdb (argv [6]);
	//
	// Initialise the ARPA2 Common library components
	a2id_init ();
	access_init ();
	//
	// Parse ARPA2 identity fields and RIGHTS
	a2id_t remote;
	access_rights exp_rights = 0;
	char *xsname;
	ok = ok && a2id_parse_remote (&remote, argv [2], 0);
	xsname = argv [3];
	ok = ok && access_parse (argv [4], &exp_rights);
	a2act_t exp_actor;
	if (*argv [5] == '\0') {
		memset (&exp_actor, 0, sizeof (exp_actor));
	} else {
		ok = ok && a2act_parse (&exp_actor, argv [5], strlen (argv [5]), 1);
	}
	//
	// Process rules, triggering callbacks that dump data
	access_rights act_rights;
	a2act_t act_actor;
	ok = ok && access_document (&remote, xsname, svckey, svckeylen, NULL, 0, &act_rights, &act_actor);
	if (!ok) {
		log_errno ("ACL processing failed");
		exit (1);
	}
	if (act_rights != exp_rights) {
		char exp_rstr [27];
		char act_rstr [27];
		access_format (NULL, exp_rights, exp_rstr);
		access_format (NULL, act_rights, act_rstr);
		fprintf (stderr, "Wrong rights found: Expected 0x%08x or %%%s, got 0x%08x or %%%s\n", exp_rights, exp_rstr, act_rights, act_rstr);
		for (char flag = 'A'; flag <= 'Z'; flag++) {
			if ((act_rights & ~exp_rights) & RULES_FLAG (flag)) {
				fprintf (stderr, " - Flag '%c' should not have been set\n", flag);
			}
			if ((exp_rights & ~act_rights) & RULES_FLAG (flag)) {
				fprintf (stderr, " - Flag '%c' should     have been set\n", flag);
			}
		}
		exit (1);
	}
	if (0 != strcmp (act_actor.txt, exp_actor.txt)) {
		fprintf (stderr, "Wrong actor found: Expected %s, got %s\n", exp_actor.txt, act_actor.txt);
		exit (1);
	}
	//
	// Cleanup and leave in success
	access_fini ();
	a2id_fini ();
	exit (0);
}
