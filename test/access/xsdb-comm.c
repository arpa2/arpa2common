/* Test Rules DB Communication Access: black/white/... listing.
 *
 * The commandline provides a local, remote, expected level
 * and then lists ACL rules as additional strings.  The level
 * is the numeric level defined in the include.  In case
 * of failures or differences, the program yields exit(1).
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/rules_db.h>
#include <arpa2/access_comm.h>


void cleanup_testdb (void) {
	char *envdir = getenv ("ARPA2_RULES_DIR");
	const char *possible_filenames [] = { "data.mdb", "lock.mdb", NULL };
	int rulesdir = open (envdir, O_DIRECTORY);
	if (rulesdir >= 0) {
		for (const char **filename = possible_filenames; *filename != NULL; filename++) {
			if (unlinkat (rulesdir, *filename, 0) != 0) {
				perror ("Failed to remove file");
			}
		}
	}
	close (rulesdir);
	if (unlinkat (-1, envdir, AT_REMOVEDIR) != 0) {
		perror ("Failed to cleanup Rules DB after test");
		exit (1);
	}
}


bool setup_testdb (char *mkdb_script) {
	static char envdir [100];
	strcpy (envdir, "/tmp/xsdb-comm-XXXXXX");
	char *madedir = mkdtemp (envdir);
	if (madedir == NULL) {
		perror ("Failed to create ARPA2_RULES_DIR");
		return false;
	}
	if (setenv ("ARPA2_RULES_DIR", madedir, 1) != 0) {
		perror ("Failed to setenv ARPA2_RULES_DIR");
		return false;
	}
	atexit (cleanup_testdb);
	if (system (mkdb_script) != 0) {
		perror ("Failed to run script on ARPA2_RULES_DIR");
		return false;
	}
	return true;
}


bool make_svckey (const char *domain, rules_dbkey svckey) {
	bool ok = true;
	rules_dbkey domkey;
	ok = ok && rules_dbkey_domain (domkey,
				"", 0,
				domain);
	ok = ok && rules_dbkey_service (svckey,
				domkey, sizeof (domkey),
				access_type_comm);
	return ok;
}


int main (int argc, char *argv []) {
	//
	// Forget any live Rules DB
	unsetenv ("ARPA2_RULES_DIR");
	//
	// Parse arguments
	a2id_t local;
	a2id_t remote;
	access_comm_level level_exp;
	a2id_t local_exp;
	a2act_t remote_actor;
	a2act_t actor_out;
	if (argc != 6) {
		fprintf (stderr, "Usage: %s <local> <remote> WHITE|BLACK|GREY|HONEY|<local_changed_for_whitelisting> NONE|""|<remote_actor> <dbsetup.sh>\n", argv [0]);
		exit (1);
	}
	bool ok = true;
	//
	// Start by running the database setup script
	ok = ok && setup_testdb (argv [5]);
	//
	// Setup ARPA2 Identity
	a2id_init ();
	ok = ok && a2id_parse        (&local,  argv [1], 0);
	ok = ok && a2id_parse_remote (&remote, argv [2], 0);
	const char *domain = local.txt + local.ofs [A2ID_OFS_DOMAIN];
	//
	// Setup ARPA2 Access Control
	access_init ();
	rules_dbkey svckey;
	const int svckeylen = sizeof (svckey);
	ok = ok && make_svckey (domain, svckey);
	//
	// Parse further arguments
	if (strcmp (argv [3], "BLACK") == 0) {
		level_exp = access_comm_blacklist;
	} else if (strcmp (argv [3], "GREY") == 0) {
		level_exp = access_comm_greylist;
	} else if (strcmp (argv [3], "HONEY") == 0) {
		level_exp = access_comm_honeypot;
	} else if (strcmp (argv [3], "WHITE") == 0) {
		level_exp = access_comm_whitelist;
		ok = ok && a2id_parse (&local_exp, argv [1], 0);
	} else {
		level_exp = access_comm_whitelist;
		ok = ok && a2id_parse (&local_exp, argv [3], 0);
	}
	if (level_exp != access_comm_whitelist) {
		ok = ok && (0 == strcmp (argv [4], "NONE"));
	} else if (*argv [4] == '\0') {
		memset (&remote_actor, 0, sizeof (remote_actor));
	} else {
		ok = ok && a2act_parse (&remote_actor, argv [4], strlen (argv [4]), 1);
	}
	if (!ok) {
		log_errno ("Wrong identity grammar");
		exit (1);
	}
	//
	// Process rules, triggering callbacks that dump data
	access_comm_level level_out;
	ok = ok && access_comm (&remote, &local, svckey, svckeylen, NULL, 0, &level_out, &actor_out);
	if (!ok) {
		log_errno ("ACL processing failed");
		exit (1);
	}
	if (level_out != level_exp) {
		log_errno ("Wrong access level: Expected %d, got %d", level_exp, level_out);
		exit (1);
	}
	if (level_exp == access_comm_whitelist) {
		if (!a2id_equal (&local, &local_exp)) {
			log_errno ("Local identity wrong: Expected %s, got %s", local_exp.txt, local.txt);
			exit (1);
		}
		if (0 != strcmp (actor_out.txt, remote_actor.txt)) {
			log_errno ("Remote Actor wrong: Expected %s, got %s", remote_actor.txt, actor_out.txt);
			exit (1);
		}
	}
	//
	// Cleanup and leave in success
	access_fini ();
	a2id_fini ();
	exit (0);
}
