/* Test Rules DB Group Access: what Actor Identity to use for an external user?
 *
 * The commandline starts with , followed by pairs of
 *  1. a database setup script
 *  2. a group identity to which externals may belong
 *  3. pairs of
 *      a. external/remote identity
 *      b. Actor Identity (or "" for empty, signaling no-such-member)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2024 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/group.h>
#include <arpa2/rules_db.h>


void cleanup_testdb (void) {
	char *envdir = getenv ("ARPA2_RULES_DIR");
	const char *possible_filenames [] = { "data.mdb", "lock.mdb", NULL };
	int rulesdir = open (envdir, O_DIRECTORY);
	if (rulesdir >= 0) {
		for (const char **filename = possible_filenames; *filename != NULL; filename++) {
			if (unlinkat (rulesdir, *filename, 0) != 0) {
				perror ("Failed to remove file");
			}
		}
	}
	close (rulesdir);
	if (unlinkat (-1, envdir, AT_REMOVEDIR) != 0) {
		perror ("Failed to cleanup Rules DB after test");
		exit (1);
	}
}


bool setup_testdb (char *mkdb_script) {
	static char envdir [100];
	strcpy (envdir, "/tmp/xsdb-actor-XXXXXX");
	char *madedir = mkdtemp (envdir);
	if (madedir == NULL) {
		perror ("Failed to create ARPA2_RULES_DIR");
		return false;
	}
	if (setenv ("ARPA2_RULES_DIR", madedir, 1) != 0) {
		perror ("Failed to setenv ARPA2_RULES_DIR");
		return false;
	}
	atexit (cleanup_testdb);
	if (system (mkdb_script) != 0) {
		perror ("Failed to run script on ARPA2_RULES_DIR");
		return false;
	}
	return true;
}


bool make_svckey_group (const char *domain, rules_dbkey svckey_group) {
	bool ok = true;
	rules_dbkey domkey;
	ok = ok && rules_dbkey_domain (domkey,
				"", 0,
				domain);
	ok = ok && rules_dbkey_service (svckey_group,
				domkey, sizeof (domkey),
				rules_type_group);
	return ok;
}


int main (int argc, char *argv []) {
	//
	// Forget any live Rules DB
	unsetenv ("ARPA2_RULES_DIR");
	//
	// Parse arguments
	bool stxerr = false;
	stxerr = stxerr || (argc < 6);
	stxerr = stxerr || (argc % 3 != 0);
	if (stxerr) {
		fprintf (stderr, "Usage: %s <dbsetup.sh> <GroupID> <ExternID> <ActorID> <%%MARKS>...(triples)\n", argv [0]);
		exit (1);
	}
	bool ok = true;
	//
	// Start by running the database setup script
	ok = ok && setup_testdb (argv [1]);
	//
	// Bail out early if setup failed
	if (!ok) {
		perror ("Failed to make Rules DB");
		exit (1);
	}
	//
	// Setup ARPA2 Identity, Group, Access Control
	a2id_init ();
	group_init ();
	access_init ();
	//
	// Parse the GroupID
	a2id_t grpid;
	bool group_ok = true;
	if (!a2id_parse (&grpid, argv [2], 0)) {
		fprintf (stderr, "%s: Failed to parse %s as ARPA2 Group identity\n", argv [0], argv [2]);
		group_ok = false;
	}
	//
	// Determine the service key for the target domain
	// because that is the one enforcing Access Control
	const char *domain = grpid.txt + grpid.ofs [A2ID_OFS_DOMAIN];
	rules_dbkey svckey_group;
	const int svckeylen_group = sizeof (svckey_group);
	if (ok && !make_svckey_group (domain, svckey_group)) {
		fprintf (stderr, "%s: Failed to create service key group \"@%s\"\n",
				argv [0], domain);
		ok = false;
	}
	//
	// Parse pairs of ExternID and ActorID
	for (int argi = 3; argi < argc; argi += 3) {
		bool parse_ok = true;
		//
		// Parse the ExternID as an ARPA2 Remote Identity
		a2id_t extid;
		if (!a2id_parse_remote (&extid, argv [argi + 0], 0)) {
			fprintf (stderr, "%s: Failed to parse %s as ARPA2 Remote Identity\n", argv [0], argv [argi + 0]);
			parse_ok = false;
		}
		//
		// Parse the ActorID as an ARPA2 Actor Identity (recognising it may be empty)
		a2act_t actid_soll;
		if (*argv [argi + 1] == '\0') {
			memset (&actid_soll, 0, sizeof (actid_soll));
		} else if (!a2act_parse (&actid_soll, argv [argi + 1], 0, 1)) {
			fprintf (stderr, "%s: Failed to parse %s as an ARPA2 Actor Identity with 1 member level\n", argv [0], argv [argi + 1]);
			parse_ok = false;
		}
		//
		// Compute the Group Marks as specified on the commandline
		group_marks marks_soll = 0;
		if (!access_parse (argv [argi + 2], &marks_soll)) {
			fprintf (stderr, "%s: Failed to parse %s as ARPA2 Access Rights string\n", argv [0], argv [argi + 2]);
			parse_ok = false;
		}
		//
		// Find the Actor Identity in the Rules DB using group_findmember()
		// which happens to be the function under test for this program
		a2act_t actid_ist;
		group_marks marks_ist;
		bool test_ok = group_ok && parse_ok &&
				group_findmember (&extid, &grpid,
					svckey_group, svckeylen_group, NULL, 0,
					&actid_ist, &marks_ist);
		if (parse_ok && !test_ok) {
			perror ("Error in group_findmember()");
		}
		//
		// When group_findmember() ran successfully, compare results
		if (test_ok) {
			if (a2act_isempty (&actid_soll)) {
				if (!a2act_isempty (&actid_ist)) {
					fprintf (stderr, "%s: Expected an empty Actor Identity, got \"%s\"\n", argv [0], actid_ist.txt);
					test_ok = false;
				}
			} else if (0 != strcmp (actid_soll.txt, actid_ist.txt)) {
				fprintf (stderr, "%s: Expected Actor Identity \"%s\", got \"%s\"\n", argv [0], actid_soll.txt, actid_ist.txt);
				test_ok = false;
			}
		}
		//
		// Report succss if we still believe in it
		if (test_ok) {
			fprintf (stderr, "%s SUCCESS: %s has Actor Identity \"%s\" in group %s\n", argv [0], extid.txt, actid_ist.txt, grpid.txt);
		}
		//
		// Accumulate the overall OK of this test
		ok = ok && group_ok && parse_ok && test_ok;
	}
	//
	// Cleanup and leave in success or in terror
	access_fini ();
	group_fini ();
	a2id_fini ();
	exit (ok ? 0 : 1);
}
