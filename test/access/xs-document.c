/* Test Document Access: Compute and compare Access Rights.
 *
 * The commandline provides a Remote Identity, and Access Name
 * exapected Access Rights, * and then lists ACL rules as
 * additional strings.  In case of failures or differences in
 * outcome, the program yields exit(1).
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/access_document.h>


int main (int argc, char *argv []) {
	a2id_init ();
	access_init ();
	//
	// Parse arguments
	a2id_t remote;
	access_rights exp_rights = 0;
	char *xsname;
	a2id_t local_exp;
	if (argc < 6) {
		fprintf (stderr, "Usage: %s <remote> <access_name> RIGHTS <actor> <aclrule>...\n", argv [0]);
		exit (1);
	}
	bool ok = true;
	ok = ok && a2id_parse_remote (&remote, argv [1], 0);
	xsname = argv [2];
	ok = ok && access_parse (argv [3], &exp_rights);
	a2act_t exp_actor;
	if (*argv [4] == '\0') {
		memset (&exp_actor, 0, sizeof (exp_actor));
	} else {
		ok = ok && a2act_parse (&exp_actor, argv [4], strlen (argv [4]), 1);
	}
	//
	// Collect the ACL rules into one contiguous block
	unsigned acllen = 0;
	for (int argi = 5; argi < argc; argi++) {
		acllen += strlen (argv [argi]) + 1;
	}
	char *acl = malloc (acllen);
	if (acl == NULL) {
		log_errno ("Rules allocation");
		exit (1);
	}
	acllen = 0;
	for (int argi = 5; argi < argc; argi++) {
		int onelen = strlen (argv [argi]);
		strcpy (acl + acllen, argv [argi]);
		acllen += onelen + 1;
	}
	//
	// Process rules, triggering callbacks that dump data
	access_rights act_rights;
	a2act_t act_actor;
	ok = ok && access_document (&remote, xsname, NULL, 0, acl, acllen, &act_rights, &act_actor);
	if (!ok) {
		log_errno ("ACL processing failed");
		exit (1);
	}
	if (act_rights != exp_rights) {
		char exp_rstr [27];
		char act_rstr [27];
		access_format (NULL, exp_rights, exp_rstr);
		access_format (NULL, act_rights, act_rstr);
		fprintf (stderr, "Wrong rights found: Expected 0x%08x or %%%s, got 0x%08x or %%%s\n", exp_rights, exp_rstr, act_rights, act_rstr);
		for (char flag = 'A'; flag <= 'Z'; flag++) {
			if ((act_rights & ~exp_rights) & RULES_FLAG (flag)) {
				fprintf (stderr, " - Flag '%c' should not have been set\n", flag);
			}
			if ((exp_rights & ~act_rights) & RULES_FLAG (flag)) {
				fprintf (stderr, " - Flag '%c' should     have been set\n", flag);
			}
		}
		exit (1);
	}
	if (0 != strcmp (act_actor.txt, exp_actor.txt)) {
		fprintf (stderr, "Wrong actor found: Expected %s, got %s\n", exp_actor.txt, act_actor.txt);
		exit (1);
	}
	//
	// Cleanup and leave in success
	free (acl);
	acl = NULL;
	access_fini ();
	a2id_fini ();
	exit (0);
}
