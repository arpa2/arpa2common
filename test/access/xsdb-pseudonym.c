/* Test Rules DB Actor Access: can I switch to a Pseudonym?
 *
 * The commandline starts with a database setup script, followed
 * by parameters with a signaling character prefix.
 *
 * =xxx@yyy sets the login identity
 * +xxx@yyy checks the pseudonym with %T , expecting success, switches
 + *xxx@yyy checks the pseudonym with %TA, expecting success, switches
 * -xxx@yyy checks the pseudonym with %T , expecting failure, continue
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2024 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/group.h>
#include <arpa2/rules_db.h>
#include <arpa2/access_actor.h>


void cleanup_testdb (void) {
	char *envdir = getenv ("ARPA2_RULES_DIR");
	const char *possible_filenames [] = { "data.mdb", "lock.mdb", NULL };
	int rulesdir = open (envdir, O_DIRECTORY);
	if (rulesdir >= 0) {
		for (const char **filename = possible_filenames; *filename != NULL; filename++) {
			if (unlinkat (rulesdir, *filename, 0) != 0) {
				perror ("Failed to remove file");
			}
		}
	}
	close (rulesdir);
	if (unlinkat (-1, envdir, AT_REMOVEDIR) != 0) {
		perror ("Failed to cleanup Rules DB after test");
		exit (1);
	}
}


bool setup_testdb (char *mkdb_script) {
	static char envdir [100];
	strcpy (envdir, "/tmp/xsdb-actor-XXXXXX");
	char *madedir = mkdtemp (envdir);
	if (madedir == NULL) {
		perror ("Failed to create ARPA2_RULES_DIR");
		return false;
	}
	if (setenv ("ARPA2_RULES_DIR", madedir, 1) != 0) {
		perror ("Failed to setenv ARPA2_RULES_DIR");
		return false;
	}
	atexit (cleanup_testdb);
	if (system (mkdb_script) != 0) {
		perror ("Failed to run script on ARPA2_RULES_DIR");
		return false;
	}
	return true;
}


bool make_svckey (const char *domain, rules_dbkey svckey_group, rules_type rtype) {
	bool ok = true;
	rules_dbkey domkey;
	ok = ok && rules_dbkey_domain (domkey,
				"", 0,
				domain);
	ok = ok && rules_dbkey_service (svckey_group,
				domkey, sizeof (domkey),
				rtype);
	return ok;
}


int main (int argc, char *argv []) {
	//
	// Forget any live Rules DB
	unsetenv ("ARPA2_RULES_DIR");
	//
	// Parse arguments
	a2id_t current_id;
	a2id_t desired_id;
	memset (&current_id, 0, sizeof (current_id));
	memset (&desired_id, 0, sizeof (desired_id));
	bool stxerr = false;
	stxerr = stxerr || (argc < 4);
	stxerr = stxerr || (*argv [2] != '=');
	for (int argi = 3; argi < argc; argi++) {
		char op = *argv [argi];
		if ((op != '=') && (op != '+') && (op != '*') && (op != '-')) {
			stxerr = true;
		}
	}
	if (stxerr) {
		fprintf (stderr, "Usage: %s <dbsetup.sh> <=localid> <[+*-=]otherid>...\n", argv [0]);
		exit (1);
	}
	bool ok = true;
	//
	// Start by running the database setup script
	ok = ok && setup_testdb (argv [1]);
	//
	// Setup ARPA2 Identity and Group
	a2id_init ();
	group_init ();
	if (ok && !a2id_parse (&current_id, argv [2] + 1, 0)) {
		fprintf (stderr, "%s: Failed to parse current_id \"%s\"\n",
				argv [0], argv[2] + 1);
		ok = false;
	}
	//
	// Setup ARPA2 Access Control
	access_init ();
	//
	// Parse further arguments
	for (int argi = 3; argi < argc; argi++) {
		char op = *argv [argi];
		if (!a2id_parse (&desired_id, argv [argi] + 1, 0)) {
			fprintf (stderr, "%s: Failed to parse identity: \"%s\"\n",
					argv [0], argv [argi] + 1);
			ok = false;
		}
		//
		// Determine the service key for the target domain
		// because that is the one enforcing Access Control
		const char *domain = desired_id.txt + desired_id.ofs [A2ID_OFS_DOMAIN];
		rules_dbkey svckey_group ;
		rules_dbkey svckey_pseudo;
		const int svckeylen_group  = sizeof (svckey_group );
		const int svckeylen_pseudo = sizeof (svckey_pseudo);
		if (ok && !make_svckey (domain, svckey_group , rules_type_group    )) {
			fprintf (stderr, "%s: Failed to create service key group \"@%s\"\n",
					argv [0], domain);
			ok = false;
		}
		if (ok && !make_svckey (domain, svckey_pseudo, rules_type_pseudonym)) {
			fprintf (stderr, "%s: Failed to create service key pseudonym \"@%s\"\n",
					argv [0], domain);
			ok = false;
		}
		//
		// Fetch the Access Rights from the Pseudonym profile
		access_rights pseudo_rights;
		bool gotpright = access_actor_pseudonym (&current_id, &desired_id,
					svckey_pseudo, sizeof (svckey_pseudo),
					NULL, 0,	/* pseudorules/len */
					&pseudo_rights);
		//
		// When the operator is '*' check on %A rights, then map to '+' operator
		if (op == '+') {
			if (!gotpright) {
				fprintf (stderr, "%s ERROR: '+' got failure from access_actor_pseudonym (\"%s\", \"%s\")\n",
						argv [0], current_id.txt, desired_id.txt);
				ok = false;
			} else if ((pseudo_rights & ACCESS_ADMIN) != 0) {
				fprintf (stderr, "%s ERROR: Got %%A in access_actor_pseudonym (\"%s\", \"%s\")\n",
						argv [0], current_id.txt, desired_id.txt);
				ok = false;
			}
		} else if (op == '*') {
			if (!gotpright) {
				fprintf (stderr, "%s ERROR: '*' got failure from access_actor_pseudonym (\"%s\", \"%s\")\n",
						argv [0], current_id.txt, desired_id.txt);
			} else if ((pseudo_rights & ACCESS_ADMIN) != ACCESS_ADMIN) {
				fprintf (stderr, "%s ERROR: No %%A in access_actor_pseudonym (\"%s\", \"%s\")\n",
						argv [0], current_id.txt, desired_id.txt);
				ok = false;
			}
			op = '+';
		} else {
			if (gotpright) {
				fprintf (stderr, "%s ERROR: '%c' got no failure from access_actor_pseudonym (\"%s\", \"%s\")\n",
						argv [0], op, current_id.txt, desired_id.txt);
				ok = false;
			}
		}
		//
		// Test if we can make the transition to the new identity
		bool test = access_actor_svckey (&current_id, &desired_id,
					svckey_group,  sizeof (svckey_group ),
					svckey_pseudo, sizeof (svckey_pseudo));
		//
		// Check whether the test result is as desired
		if ((op == '-') && test) {
			fprintf (stderr, "%s ERROR: Improper success from access_actor (\"%s\", \"%s\")\n",
					argv [0], current_id.txt, desired_id.txt);
			ok = false;
		}
		if ((op == '+') && !gotpright) {
			fprintf (stderr, "%s: ERROR: Did not succeed in access_actor_pseudonym  (\"%s\", \"%s\")\n",
					argv [0], current_id.txt, desired_id.txt);
			ok = false;
		}
		if ((op == '+') && !test) {
			fprintf (stderr, "%s ERROR: Improper failure from access_actor (\"%s\", \"%s\")\n",
					argv [0], current_id.txt, desired_id.txt);
			ok = false;
		}
		//
		// Operations '+' and '=' set the current_id to the desired_id
		if (op != '-') {
			memcpy (&current_id, &desired_id, sizeof (current_id));
		}
	}
	//
	// Cleanup and leave in success or in terror
	access_fini ();
	group_fini ();
	a2id_fini ();
	exit (ok ? 0 : 1);
}
