/* Test Communication Access: black/white/... listing.
 *
 * The commandline provides a local, remote, expected level
 * and then lists ACL rules as additional strings.  The level
 * is the numeric level defined in the include.  In case
 * of failures or differences, the program yields exit(1).
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/except.h>
#include <arpa2/identity.h>
#include <arpa2/access_comm.h>


int main (int argc, char *argv []) {
	a2id_init ();
	access_init ();
	//
	// Parse arguments
	a2id_t local;
	a2id_t remote;
	access_comm_level level_exp;
	a2id_t local_exp;
	a2act_t remote_actor;
	a2act_t actor_out;
	if (argc < 6) {
		fprintf (stderr, "Usage: %s <local> <remote> WHITE|BLACK|GREY|HONEY|<local_changed_for_whitelisting> NONE|""|<remote_actor> <aclrule>...\n", argv [0]);
		exit (1);
	}
	bool ok = true;
	ok = ok && a2id_parse        (&local,  argv [1], 0);
	ok = ok && a2id_parse_remote (&remote, argv [2], 0);
	if (strcmp (argv [3], "BLACK") == 0) {
		level_exp = access_comm_blacklist;
	} else if (strcmp (argv [3], "GREY") == 0) {
		level_exp = access_comm_greylist;
	} else if (strcmp (argv [3], "HONEY") == 0) {
		level_exp = access_comm_honeypot;
	} else if (strcmp (argv [3], "WHITE") == 0) {
		level_exp = access_comm_whitelist;
		ok = ok && a2id_parse (&local_exp, argv [1], 0);
	} else {
		level_exp = access_comm_whitelist;
		ok = ok && a2id_parse (&local_exp, argv [3], 0);
	}
	if (level_exp != access_comm_whitelist) {
		ok = ok && (0 == strcmp (argv [4], "NONE"));
	} else if (*argv [4] == '\0') {
		memset (&remote_actor, 0, sizeof (remote_actor));
	} else {
		ok = ok && a2act_parse (&remote_actor, argv [4], strlen (argv [4]), 1);
	}
	if (!ok) {
		log_errno ("Wrong identity grammar");
		exit (1);
	}
	//
	// Collect the ACL rules into one contiguous block
	unsigned acllen = 0;
	for (int argi = 5; argi < argc; argi++) {
		acllen += strlen (argv [argi]) + 1;
	}
	char *acl = malloc (acllen);
	if (acl == NULL) {
		log_errno ("Rules allocation");
		exit (1);
	}
	acllen = 0;
	for (int argi = 5; argi < argc; argi++) {
		int onelen = strlen (argv [argi]);
		strcpy (acl + acllen, argv [argi]);
		acllen += onelen + 1;
	}
	//
	// Process rules, triggering callbacks that dump data
	access_comm_level level_out;
	ok = ok && access_comm (&remote, &local, NULL, 0, acl, acllen, &level_out, &actor_out);
	if (!ok) {
		log_errno ("ACL processing failed");
		exit (1);
	}
	if (level_out != level_exp) {
		log_errno ("Wrong access level: Expected %d, got %d", level_exp, level_out);
		exit (1);
	}
	if (level_exp == access_comm_whitelist) {
		if (!a2id_equal (&local, &local_exp)) {
			log_errno ("Local identity wrong: Expected %s, got %s", local_exp.txt, local.txt);
			exit (1);
		}
		if (0 != strcmp (actor_out.txt, remote_actor.txt)) {
			log_errno ("Remote Actor wrong: Expected %s, got %s", remote_actor.txt, actor_out.txt);
			exit (1);
		}
	}
	//
	// Cleanup and leave in success
	free (acl);
	acl = NULL;
	access_fini ();
	a2id_fini ();
	exit (0);
}
