/* Parse a hex string and check that it matches an ASCII string
 *
 * This takes a hex string like "404142...4F" and maps it to a
 * (presumably) ASCII string, which it then compares.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/rules_db.h>


int main (int argc, char *argv []) {
	//
	// Check arguments
	if (argc != 3) {
		fprintf (stderr, "Usage: %s hexstring asciistring\n", argv [0]);
		exit (1);
	}
	//
	// Parse the first string
	rules_dbkey testkey;
	bool ok = rules_dbkey_parse (testkey, argv [1], strlen (argv [1]));
	if (!ok) {
		fprintf (stderr, "%s: Failed to parse \"%s\" as Rules DB key\n", argv [0], argv [1]);
		exit (1);
	}
	//
	// Compare the output to the second string
	if (strlen (argv [2]) != sizeof (testkey)) {
		fprintf (stderr, "%s: The comparison string \"%s\" has a bad size\n", argv [0], argv [2]);
		exit (1);
	}
	if (memcmp (argv [2], testkey, sizeof (testkey))) {
		fprintf (stderr, "%s: The comparison string \"%s\" differs from the hex input \"%s\"\n", argv [0], argv [2], argv [1]);
		exit (1);
	}
	//
	// All is well
	return 0;
}

