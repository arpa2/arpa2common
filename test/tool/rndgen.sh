#!/bin/sh
#
# rndgen -- generate random patterns for BASE85 encoder input
#
# The file pattern is in $1.  The sizes are in $2+

DIR="$1"
shift 1
for SZ in $*
do
	echo Generating $SZ random bytes
	dd if=/dev/urandom "of=$DIR/rndgen-$SZ.testdata" bs=1 count=$SZ status=none
done
