/* test/util/base85length.c  --  Test the length of a BASE85 encoding
 *
 * Arguments are numeric lengths, made negative for bad.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */

#include <stdlib.h>
#include <stdbool.h>

#include <stdio.h>

#include <arpa2/util/base85.h>



int main (int argc, char *argv[]) {
	bool ok = true;
	for (int argi=1; argi<argc; argi++) {
		char *endptr = NULL;
		long val = strtol (argv[argi], &endptr, 10);
		if ((endptr == NULL) || (*endptr != '\0')) {
			fprintf (stderr, "ERROR: Improper numeric value in argument \"%s\"\n", argv[argi]);
			ok = false;
		}
		if (val < 0) {
			if (base85_reprlen_ok (-val)) {
				fprintf (stderr, "ERROR: Length should have been improper but is permitted: %ld\n", -val);
				ok = false;
			}
		} else {
			if (!base85_reprlen_ok (val)) {
				fprintf (stderr, "ERROR: Length should have been proper but is rejected: %ld\n", val);
				ok = false;
			}
		}
	}
	exit (ok ? 0 : 1);
}
