/* base85spn.c  --  Run over strings and determine lengths.
 *
 * Use base85_strspn() and base85_strcspn() to split strings.
 * The first encoded byte indicates the length, or 0xff for bad.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#include <arpa2/util/base85.h>


int main (int argc, char* argv[]) {
	bool bad = false;
	for (int argi=1; argi<argc; argi++) {
		const char *args = argv[argi];
		while (*args != '\0') {
			unsigned on  = base85_strspn  (args);
			const char *argspc = args + on;
			if (on < 2) {
				fprintf (stderr, "ERROR: Test lengths must be >= 2, wrong in arg %d pos %d\n", argi, (int) (args-argv[argi]));
				bad = true;
				continue;
			}
			if (on > 250) {
				fprintf (stderr, "ERROR: Test lengths must be <= 250, wrong in arg %d pos %d\n", argi, (int) (args-argv[argi]));
				bad = true;
				continue;
			}
			unsigned off = base85_strcspn (argspc);
			const char *argsplus = argspc + off;
			uint8_t first;
			base85_decode_bytes (args, 2, &first);
			int len = base85_strlen (args);
			if (base85_reprlen_ok (on)) {
				if (on != (unsigned) len) {
					fprintf (stderr, "ERROR: base85_strlen() == %d, base85_spn() == %d for \"%s\"\n", len, on, args);
					bad = true;
				}
				uint8_t on_byte = on;
				char code[2];
				base85_encode_bytes (&on_byte, 1, code);
				if (first != on) {
					fprintf (stderr, "ERROR: First byte should be length %d so use %c%c in \"%.*s\"\n", on, code[0], code[1], on, args);
					bad = true;
				}
			} else {
				if (len != -1) {
					fprintf (stderr, "ERROR: base85_strlen() == %d (should be -1) for wrong length in \"%s\"\n", len, args);
					bad = true;
				}
				if (first != 0xff) {
					uint8_t bad_byte = 0xff;
					char code[2];
					base85_encode_bytes (&bad_byte, 1, code);
					fprintf (stderr, "ERROR: Invalid string length so use %c%c for \"%.*s\" #%d, decoding should have started with 0xff\n", code[0], code[1], on, args, on);
					bad = true;
				}
			}
			args = argsplus;
		}
	}
	exit (bad ? 1 : 0);
}
