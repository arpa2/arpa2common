/* test/util/base85.c  --  Test BASE85 encoding and decoding.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <stdio.h>

#include <arpa2/util/base85.h>



void bytes2hex (const uint8_t* bytes, unsigned len) {
	while (len-- > 0) {
		fprintf (stderr, "%02x", *bytes++);
	}
	fprintf (stderr, "\n");
}


bool hex2bytes (const char* hex, uint8_t* bytes) {
	while (*hex) {
		unsigned hexval;
		if (sscanf (hex, "%02x", &hexval) != 1) {
			return false;
		}
		*bytes++ = hexval;
		hex += 2;
	}
	return true;
}


int main (int argc, char* argv[]) {
	bool ok = true;
	//
	// Iterate over all hex string arguments
	for (int argi=1; argi<argc; argi++) {
		int arglen = strlen (argv[argi]);
		int bytlen = arglen / 2;
		fprintf (stderr, "INPUT: Argument %d contains %s #%d\n", argi, argv[argi], arglen);
		//
		// Map hex to binary
		uint8_t bytes[bytlen+3];
		if (!hex2bytes (argv[argi], bytes)) {
			fprintf (stderr, "ERROR: Not a hex string\n");
			ok = false;
			continue;
		}
		//
		// Map binary to BASE85 ASCII
		unsigned b85len = base85_reprlen (bytlen);
		char ascii85 [b85len + 3];
		memset (ascii85, 0xff, b85len+2);
		ascii85[b85len+2] = '\0';
		base85_encode_bytes (bytes, bytlen, ascii85);
		fprintf (stderr, "BYPUT: Argument %d becomes  %s #%d\n", argi, ascii85, b85len);
		if (strlen(ascii85) != b85len) {
			fprintf (stderr, "ERROR: Encoded to %d bytes, not %d: %s\n", (int)strlen(ascii85), b85len, ascii85);
			ok = false;
			continue;
		}
		if (ascii85[b85len+1] != (char) 0xff) {
			fprintf (stderr, "ERROR: Decoded length overwrites memory after NUL-terminated string\n");
			ok = false;
			continue;
		}
		//
		// Map BASE85 ASCII back to binary
		uint8_t revbin1 [bytlen+3];
		memset (revbin1, 0xff, bytlen+3);
		if (!base85_decode_bytes (ascii85, b85len, revbin1)) {
			fprintf (stderr, "ERROR: Failed reverse decoding #1: %s\n", ascii85);
			ok = false;
		}
		uint8_t revbin2 [bytlen+3];
		memset (revbin2, 0xff, bytlen+3);
		if (!base85_decode_bytes (ascii85, strlen(ascii85), revbin2)) {
			fprintf (stderr, "ERROR: Failed reverse decoding #2: %s\n", ascii85);
			ok = false;
		}
		if (0 != memcmp (revbin1, revbin2, bytlen+3)) {
			fprintf (stderr, "ERROR: Reverse decodings #1 and #2 differ\n");
			ok = false;
		}
		if (0 != memcmp (revbin1, bytes, bytlen)) {
			fprintf (stderr, "ERROR: Reverse decoding #1 is wrong: ");
			bytes2hex (revbin1, bytlen);
			ok = false;
		}
		if (0 != memcmp (revbin2, bytes, bytlen)) {
			fprintf (stderr, "ERROR: Reverse decoding #2 is wrong: ");
			bytes2hex (revbin2, bytlen);
			ok = false;
		}
		if (revbin1[bytlen] != 0xff) {
			fprintf (stderr, "ERROR: Reverse decoding #1 is too long: ");
			bytes2hex (revbin1, sizeof(revbin1));
			ok = false;
		}
		if (revbin2[bytlen] != 0xff) {
			fprintf (stderr, "ERROR: Reverse decoding #2 is too long: ");
			bytes2hex (revbin2, sizeof(revbin2));
			ok = false;
		}
		if (revbin1[base85_reprlen_bytes(b85len)] != 0xff) {
			fprintf (stderr, "ERROR: Reverse decoding #1 writes beyond destination bytes\n");
			ok = false;
		}
		if (revbin2[base85_reprlen_bytes(b85len)] != 0xff) {
			fprintf (stderr, "ERROR: Reverse decoding #2 writes beyond destination bytes\n");
			ok = false;
		}
	}
	//
	// Report overall success or failure
	exit (ok ? 0 : 1);
}
