/* a2md-compare-totp-sha256.c -- Compute TOTP values with a SHA-256 hash.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/digest.h>


int main (int argc, char *argv[]) {
	//
	// Check arguments
	if (argc != 3) {
		fprintf (stderr, "Usage: %s <timectr> <digits>\n", argv[0]);
		exit (1);
	}
	char* timectr = argv[1];
	char *endptr = "";
	uint64_t time64 = strtoull (timectr, &endptr, 16);
	if (*endptr != '\0') {
		fprintf (stderr, "FATAL: Garbage after timectr %s\n", timectr);
		exit (1);
	}
	char* digits  = argv[2];
	uint32_t modulo = 1;
	for (unsigned i=0; digits[i] != '\0'; i++) {
		int32_t modulo2 = modulo * 10;
		if (modulo2 < (int32_t) modulo) {
			fprintf (stderr, "FATAL: Digits out of range: %s", digits);
			exit (1);
		}
		modulo = modulo2;
	}
	//
	// The key is a fixed string as *not* defined in RFC 6238 but as
	// *corrected* in erratum #2, https://www.rfc-editor.org/errata/rfc6238
	uint8_t* key = "12345678901234567890123456789012";
	unsigned keylen = strlen (key);
	//
	// Compute the TOTP value for a fixed time, bypassing TOTP
	// and instead relying on pre-computed T=(time()-0)/30 value
	int32_t value = a2md_hotp (key, keylen, time64);
	if (value < 0) {
		fprintf (stderr, "FATAL: a2md_hotp() failed\n");
		exit (1);
	}
	value %= modulo;
	//
	// Parse the digits into another value
	uint32_t expected = strtoul (digits, &endptr, 10);
	if (*endptr != '\0') {
		fprintf (stderr, "FATAL: Garbage after digits %s\n", digits);
		exit (1);
	}
	//
	// Compare the digits to the computation
	if (expected != (uint32_t) value) {
		fprintf (stderr, "FATAL: Digits are wrong; expected %08lu, got %08lu\n", (long unsigned) expected, (long unsigned) value);
		exit (1);
	}
	//
	// Return success
	return 0;
}
