/* a2md-testvector-hmac-sha256.c -- Compute and compare RFC 4231 test vectors.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>
#include <fcntl.h>

#include <arpa2/digest.h>
#include <arpa2/digest_sha256.h>



bool hex2bytes (const char *hex, uint8_t *bytes) {
	while (*hex) {
		unsigned hexval;
		if (sscanf (hex, "%02x", &hexval) != 1) {
			return false;
		}
		*bytes++ = hexval;
		hex += 2;
	}
	return true;
}


int main (int argc, char *argv []) {
	//
	// Check arguments
	if (argc != 4) {
		fprintf (stderr, "Usage: %s <key> <input> <output>|""\n", argv[0]);
		exit (1);
	}
	const char* key_str = argv[1];
	const char*  in_str = argv[2];
	const char* out_str = argv[3];
	bool expect_ok = true;
	if (strlen (out_str) != 2 * A2MD_OUTPUT_SIZE) {
		if (strlen (out_str) != 0) {
			fprintf (stderr, "FATAL: The <output> length must be %d or, for error, 0\n", 2 * A2MD_OUTPUT_SIZE);
			exit (1);
		}
		expect_ok = false;
	}
	//
	// Computation success in ok, program correctness in krek, fail collects failure
	bool ok   = true;
	bool krek = true;
	bool fail = false;
	//
	// Map hexadecimal strings to bytes
	uint16_t key_len = strlen (key_str) / 2;
	uint16_t  in_len = strlen ( in_str) / 2;
	uint16_t out_len = strlen (out_str) / 2;
	uint8_t key [key_len + 1];
	uint8_t in  [ in_len + 1];
	uint8_t out [out_len + 1];
	if (!hex2bytes (key_str, key)) {
		fprintf (stderr, "ERROR: The <key> is not a hex string: %s\n", key_str);
		krek = false;
	}
	if (!hex2bytes ( in_str,  in)) {
		fprintf (stderr, "ERROR: The <input> is not a hex string: %s\n", in_str);
		krek = false;
	}
	if (!hex2bytes (out_str, out)) {
		fprintf (stderr, "ERROR: The <output> is not a hex string: %s\n", out_str);
		krek = false;
	}
	//
	// Compute the HMAC keyed hash
	uint8_t computed [A2MD_OUTPUT_SIZE];
	a2md_hmac_state state;
	ok = ok && krek;
	ok = ok && a2md_hmac_open (state, key, key_len);
	if (ok != expect_ok) {
		fprintf (stderr, "ERROR: Key opening success %d different from expectations %d\n", ok, expect_ok);
		fail = true;
	}
	ok = ok && a2md_write (state, in, in_len);
	ok = ok && a2md_hmac_close (state, computed);
	if (expect_ok && !ok) {
		fprintf (stderr, "ERROR: HMAC-SHA256() computation failed unexpectedly\n");
		fail = true;
	}
	//
	// Compare the HMAC output to expectations
	if (ok) {
		ok = (0 == memcmp (out, computed, A2MD_OUTPUT_SIZE));
		if (!ok) {
			fprintf (stderr, "ERROR: HMAC-SHA256() output not as expected\n");
			fail = true;
		}
	}
	//
	// Combined result
	fail = fail || !krek;
	if (fail) {
		fprintf (stderr, "FATAL: Overall results are negative, test failed\n");
		exit (1);
	}
	return 0;
}
