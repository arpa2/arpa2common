/* a2id-compare-sha256.c -- Compare a file's contents to its SHA256 internal hash.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>
#include <fcntl.h>

#include <arpa2/digest.h>
#include <arpa2/digest_sha256.h>


int main (int argc, char *argv []) {
	//
	// Check arguments
	char *prog = argv [0];
	if (argc != 3) {
		fprintf (stderr, "%s: Use a filename and a hash as an argument\n", prog);
		exit (1);
	}
	char *file = argv [1];
	char *expected = argv [2];
	if (strlen (expected) != 2 * SHA256_DIGEST_LENGTH) {
		fprintf (stderr, "%s: 2nd argument should be a SHA256 hex digest, got \"%s\"\n", prog, expected);
		exit (1);
	}
	//
	// Open the hash filename
	int fd = open (file, O_RDONLY);
	if (fd < 0) {
		perror ("Failed to open file");
		exit (1);
	}
	//
	// Initialise the hash
	struct a2md_state hash;
	if (!a2md_open (&hash)) {
		fprintf (stderr, "%s: Failed to initialise the hash\n", prog);
		exit (1);
	}
	//
	// Read a block of data at once
	ssize_t blklen = -1;
	uint8_t data [4096];
	while (blklen = read (fd, data, sizeof (data)), blklen > 0) {
		if (!a2md_write (&hash, data, blklen)) {
			fprintf (stderr, "%s: Failed to hash data of size %d\n", prog, (int) blklen);
			exit (1);
		}
	}
	//
	// Close the file or report an error
	if (blklen < 0) {
		perror ("Failed reading from file");
		exit (1);
	}
	close (fd);
	fd = -1;
	//
	// Extract the hash value
	uint8_t actual [SHA256_DIGEST_LENGTH];
	if (!a2md_close (&hash, actual)) {
		fprintf (stderr, "%s: Failed to extract hash output\n", prog);
		exit (1);
	}
	//
	// Compare the hash value to the provided value
	bool same = true;
	for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
		int byte = -1;
		int parsed = sscanf (&expected [2 * i], "%02x", (unsigned int *) &byte);
		if (parsed != 1) {
			fprintf (stderr, "%s: Expected hex value parameter fails from %s\n", prog, &expected [2 * i]);
			exit (1);
		}
		if (byte != (int) actual [i]) {
			fprintf (stderr, "%s: Byte %d was 0x%02x instead of 0x%02x\n", prog, i, actual [i], byte);
			same = false;
		}
		same = same && (byte == (int) actual [i]);
	}
	if (!same) {
		fprintf (stderr, "%s: Different output values found\n", prog);
		exit (1);
	}
	//
	// All went well!
	return 0;
}
