/*
 * Copyright (c) 2018, 2019 Tim Kuijsten
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */

#include <ctype.h>
#include <stdint.h>
#include <stdio.h>

void
hexdump(int d, const uint8_t *data, size_t datalen, size_t maxwidth)
{
	size_t i, j;

	if (datalen < 1)
		return;

	if (maxwidth < 1)
		return;

	for (i = 0; i < datalen;) {
		if (i % maxwidth == 0)
			dprintf(d, "%04zx  ", i);

		dprintf(d, "%02x ", data[i]);

		i++;

		if (i % maxwidth == 0) {
			j = maxwidth;
		} else if (i == datalen) {
			/* pad */
			j = maxwidth - i % maxwidth;
			while (j-- > 0)
				dprintf(d, "   ");

			j = i % maxwidth;
		} else {
			j = 0;
		}

		if (j > 0) {
			dprintf(d, "  ");
			for (; j > 0; j--) {
				if (isgraph(data[i - j]))
					dprintf(d, "%c", data[i - j]);
				else
					dprintf(d, ".");
			}

			dprintf(d, "\n");
		}
	}
}
