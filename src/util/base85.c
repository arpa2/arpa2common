/* base85.c  --  Represent 32-bit chunks as 5 "digits" in BASE85.
 *
 * BASE85 gives nice chunking for binary data of fixed sizes, such as keys.
 * 85^5 = 4437053125 > 4294967296 = 2^32
 * So we can map uint32_t values from and to 5-character arrays.  We will
 * not add a trailing to allow chaining.  We will fill all 5 characters.
 *
 * This is not the same encoding as in RFC1924, which was designed for IPv6.
 * Instead, concerns about shell escapes and URL encoding are followed.
 *
 * The characters dropped from printable ASCII (0x33..0x7e) are 3 quotes,
 * 1 backslash, 1 percent, 1 dollar, 1 ampersand, 1 exclamation mark, 1 bar.
 * This results in 94-3-1-1-1-1-1-1 = 85 codes.  They are used to represent
 * code points 0..84 in the same ordering as used for ASCII.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <string.h>

#include <endian.h>

#include <arpa2/util/base85.h>



// >>> for i in range (33,127):
// ...     c = chr (i)
// ...     if c in '"\'`\\%$&!|':
// ...             continue
// ...     print (c, end='')

// >>> b85= '#()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_abcdefghijklmnopqrstuvwxyz{}~'

// >>> v = 0
// >>> for c in range(256):
// ...     if chr(c) in b85:
// ...             print ('%d, ' % v, end='')
// ...             v += 1
// ...     else:
// ...             print ('-1, ', end='')

// >> b85dec = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, -1, 53, 54, 55, -1, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, -1, 83, 84, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ]

// >>> for c in b85:
// ...     print ('%d, ' % b85dec[ord(c)], end='')
// ... 
// 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84,


/** @brief The sequence of BASE85 characters in their code point order 0..84.
 */
const char base85_characters[86] = "#()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_abcdefghijklmnopqrstuvwxyz{}~";

/** @brief The sequence of BASE85 decoding per byte value, yielding a code point or -1.
 */
const int8_t base85_values[256] = {
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1,  0, -1, -1, -1, -1,  1,  2,  3,  4,  5,  6,  7,  8,
	 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
	25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
	41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, -1, 53, 54, 55,
	-1, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
	71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, -1, 83, 84, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};


/** @brief Get the length of a base85 prefix substring.
 *
 * Note that the return value may not be a valid BASE85 length;
 * test with base85_reprlen_ok() to learn this, or use
 * base85_strlen() to receive -1 for a wrongful BASE85 prefix.
 */
unsigned base85_strspn (const char *s) {
	unsigned retval = 0;
	while (isbase85(*s++)) {
		retval++;
	}
	return retval;
}


/** @brief Get the length of a non-base85 prefix substring.
 */
unsigned base85_strcspn (const char *s) {
	unsigned retval = 0;
	while ((*s != '\0') && !isbase85(*s++)) {
		retval++;
	}
	return retval;
}


/** @brief Get the length of a bsae85 prefix substring, or -1 on error.
 *
 * This function works like base85_strspn(), except that it will only
 * report valid BASE85 string lengths, and -1 otherwise.  See
 * base85_reprlen_ok() for details.
 */
int base85_strlen (const char *s) {
	unsigned len = base85_strspn (s);
	if (base85_reprlen_ok (len)) {
		return (int) len;
	} else {
		return -1;
	}
}


bool base85_decode (const char in[5], uint32_t* out) {
	uint32_t tmp = 0;
	for (int c=0; c<5; c++) {
		uint32_t tmp2 = tmp*85;
		 int32_t tmp3 = base85_values[(uint8_t) *in++];
		if ((tmp2 < tmp) || (tmp3 < 0)) {
			return false;
		}
		tmp = tmp2 + tmp3;
	}
	*out = tmp;
	return true;
}

bool base85_decode_net32 (const char in[5], uint8_t out[4]) {
	uint32_t mid;
	bool rv = base85_decode (in, &mid);
	*(uint32_t*)out = htobe32 (mid);
	return rv;
}

bool base85_decode_bytes (const char *in, unsigned inlen, uint8_t *out) {
	if (!base85_reprlen_ok (inlen)) {
		return false;
	}
	while (inlen >= 5) {
		if (!base85_decode_net32 (in, out)) {
			return false;
		}
		inlen -= 5;
		in    += 5;
		out   += 4;
	}
	if (inlen > 0) {
		char min[5];
		uint8_t mout[4];
		memset (min, base85_last, 5);
		memcpy (min, in, inlen);
		memset (mout, 0, 4);
		if (!base85_decode_net32 (min, mout)) {
			return false;
		}
		memcpy (out, mout, inlen-1);
	}
	return true;
}

void base85_encode (uint32_t in, char out[5]) {
	for (int c=4; c>=0; c--) {
		uint32_t m = in % 85;
		in /= 85;
		out[c] = base85_characters[m];
	}
}

void base85_encode_net32 (const uint8_t in[4], char out[5]) {
	uint32_t mid = *(uint32_t*)in;
	mid = be32toh (mid);
	base85_encode (mid, out);
}

void base85_encode_bytes (const uint8_t* in, unsigned inlen, char *out) {
	while (inlen >= 4) {
		base85_encode_net32 (in, out);
		inlen -= 4;
		in    += 4;
		out   += 5;
	}
	if (inlen > 0) {
		uint8_t min[4];
		memset (min, 0, 4);
		memcpy (min, in, inlen);
		char mout[5];
		base85_encode_net32 (min, mout);
		inlen++;
		memcpy (out, mout, inlen);
	}
	out[inlen] = '\0';
}
