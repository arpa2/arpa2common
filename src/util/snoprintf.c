/* vsnoprintf(), snoprintf() -- The/an oversight in the POSIX interface.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Henri Manson <info@mansoft.nl>
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */

#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

#include <errno.h>


int vsnoprintf(char *buf, size_t size, ssize_t offset, const char *format, va_list ap) {
    if (size < 4) {
        errno = EINVAL;
        return -1;
    }
    if(offset < 0) {
        // error pass-through
        return offset;
    }
    int written;
    if (((ssize_t) size) > offset) {
        written = vsnprintf(buf + offset, size - offset, format, ap);
    } else {
        char buf [1];
        written = vsnprintf(buf, 1, format, ap);
    }
    if (written < 0) {
        return written;
    }
    written += offset;
    if ((size_t) written >= size) { // overflow
        strcpy(buf + size - 4, "...");
    }
    return written;
}

int snoprintf(char *buf, size_t size, ssize_t offset, const char *format, ...) {
    va_list vl;
    va_start(vl, format);
    int written = vsnoprintf(buf, size, offset, format, vl);
    va_end(vl);
    return written;
}
