#
#   SPDX-License-Identifier: BSD-2-Clause
#   SPDX-FileCopyrightText: Copyright 2020 Adriaan de Groot <groot@kde.org>
#   SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>

add_executable(a2id-keygen
    a2id-keygen.c
)
target_link_libraries(a2id-keygen PRIVATE identity)

add_executable(a2id-loadkey
    a2id-loadkey.c
)
target_link_libraries(a2id-loadkey)

add_executable(a2rule
    a2rule.c
)
target_link_libraries(a2rule PRIVATE identity access util_static)

add_executable(base85
    base85.c
)
target_link_libraries(base85 PRIVATE util_static)

add_executable(inout
    inout.c
)
target_link_libraries(inout PRIVATE util_static)

install (TARGETS a2id-keygen a2id-loadkey a2rule base85 inout
	RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

#
# CMake installs to owner/group 0, but the group should be one
# dedicated to the ARPA2 Policy Rules database, "a2ruledb".
# We do that here as a post-install fix.
#
# We install a2rule with SETGID to the a2ruledb group, to control
# that changes to the database are only made by a2rule.  Do not
# let programs or users into this group unless they are trusted
# to always update the database poperly, starting from whatever
# key they have been assigned, or from a password.
#
# Using no password means that any user can change any rule.
# Still, that only applies to the password-less portion; there
# is a distinct tree of Policy Rules for distinct passwords.
#
# Starting with a password allows users to create their own
# section in the database of Policy Rules.  Starting from a
# Domain Key (for domain admins) or Service Key (for services)
# allows the entry into a controlled part of password-protected
# trees of Policy Rules.
#
# See: https://gitlab.com/arpa2/arpa2common/-/issues/40
#
set (POST_INSTALL )
if (WIN32 OR NOT A2RULEDB_DEDICATED_GROUP)
    message (STATUS "Skipping control over dedicated-group access to the Rules DB")
else (WIN32)
    execute_process (COMMAND getent passwd ${A2RULEDB_DEDICATED_GROUP} OUTPUT_QUIET RESULT_VARIABLE _EXIT_PASSWD)
    execute_process (COMMAND getent group  ${A2RULEDB_DEDICATED_GROUP} OUTPUT_QUIET RESULT_VARIABLE _EXIT_GROUP )
    if (${_EXIT_PASSWD} EQUAL 0 AND ${_EXIT_GROUP} EQUAL 0)
        #MANUAL_BY_ADMIN# list (APPEND POST_INSTALL "useradd --system ${A2RULEDB_DEDICATED_GROUP}")
        list (APPEND POST_INSTALL "execute_process(COMMAND chown ${A2RULEDB_DEDICATED_GROUP}:${A2RULEDB_DEDICATED_GROUP} $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR}/a2rule)")
        list (APPEND POST_INSTALL "execute_process(COMMAND chmod g+s               $ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR}/a2rule)")
    else ()
        message (STATUS "Installing without the protection of a dedicated userid and group \"${A2RULEDB_DEDICATED_GROUP}\" for the RuleDB")
    endif ()
endif (WIN32 OR NOT A2RULEDB_DEDICATED_GROUP)

foreach (_CMD ${POST_INSTALL})
    message (STATUS "Will run ${_CMD} during installation")
    install (CODE ${_CMD})
endforeach (_CMD in POST_INSTALL)

