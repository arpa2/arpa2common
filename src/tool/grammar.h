/* a2xs tools grammar definitions
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */



enum parse_variables {

	/* For database selection: */
	//NO_DBENV,	/* Database envdir, default "/var/lib/arpa2/rules/" */
	//NO_DBNAME,	/* Database name in envdir, default NULL */
	VAR_DBTRUNK,	/* Default 0, overlay database versions for DUP_SORT */
	VAR_DBSECRET,	/* Scatter/encryption key, default none */
	VAR_DBPREFIX,	/* Non-standard #comment prefix for get/del */
	VAR_DBDOMKEY,	/* Domain Key, used by domain admins instead DBsecret */
	VAR_DBSVCKEY,	/* Service Key, used by service providers */

	/* For selecting the database lookup keys: */
	//NO_OBJTYPE,	/* For instance "communication" (or abbreviations) */
	VAR_LOCAL,	/* Local identity: Source of domain (and userid) */
	VAR_REMOTE,	/* Remote identity: Starting point for iteration */
	VAR_ITERATE,	/* Remote identity, but intended for looping */

	/* Stored information and added constraints: */
	VAR_RIGHTS,	/* Access rights */
	VAR_LIST,	/* Comm list: black, white, gr[ae]y, honey */
	VAR_ALIAS,	/* Comm setup for particular local aliases/svcarg */
	VAR_ENDALIAS,	/* Comm setup for ending alias */
	VAR_SIGNFLAGS,	/* Comm: required signature flags */
	VAR_NEWUSERID,	/* Comm: set the userid to a desired value, drop aliases */
	VAR_NEWALIAS,	/* Comm: set the alias(es) to a desired value */
	VAR_ACTORID,	/* Comm: set actor identity (no member alias, no domain) */

	/* End marker */
	VAR_ENDMARKER

};

/* Changes for groups */
#define VAR_MEMBER	VAR_LOCAL
#define VAR_IDENTITY	VAR_REMOTE
#define VAR_MARKS	VAR_RIGHTS

/* Changes for pseudonyms */
#define VAR_ALTEREGO	VAR_LOCAL
#define VAR_LOGINID	VAR_REMOTE

/* Changes for documents */
#define VAR_DOMAIN      VAR_LOCAL
#define VAR_COLLECTION  VAR_ITERATE
#define VAR_PATH        VAR_SIGNFLAGS
#define VAR_VOLUME      VAR_NEWALIAS

/* Changes for lets */
#define VAR_USER	VAR_REMOTE
#define VAR_CURRENCY	VAR_ALIAS
#define VAR_BALANCE	VAR_ENDALIAS

/* Changes for (abstract) rulesets */
#define VAR_TYPE	VAR_RIGHTS
#define VAR_NAME	VAR_LOCAL
#define VAR_SELECTOR	VAR_REMOTE
#define VAR_RULE	VAR_LIST

#define VAR_FIRST	 VAR_DBTRUNK
#define VAR_LAST	(VAR_ENDMARKER-1)

#if VAR_FIRST < 0
#   error "First variable reaches before bit 0"
#endif

#if VAR_LAST >= 32
#   error "Last variable reaches beyond bit 31"
#endif


/* Values to be quickly referenced in the array */
// #define VAL_DBENV	values [VAR_DBENV]
// #define VAL_DBNAME	values [VAR_DBNAME]
#define VAL_DBTRUNK	values [VAR_DBTRUNK]
#define VAL_DBSECRET	values [VAR_DBSECRET]
#define VAL_DBPREFIX	values [VAR_DBPREFIX]
#define VAL_DBDOMKEY	values [VAR_DBDOMKEY]
#define VAL_DBSVCKEY	values [VAR_DBSVCKEY]
// #define VAL_OBJTYPE	values [VAR_OBJTYPE]
#define VAL_LOCAL	values [VAR_LOCAL]
#define VAL_REMOTE	values [VAR_REMOTE]
#define VAL_ITERATE	values [VAR_ITERATE]
#define VAL_RIGHTS	values [VAR_RIGHTS]
#define VAL_LIST	values [VAR_LIST]
#define VAL_ALIAS	values [VAR_ALIAS]
#define VAL_ENDALIAS	values [VAR_ENDALIAS]
#define VAL_SIGNFLAGS	values [VAR_SIGNFLAGS]
#define VAL_NEWUSERID	values [VAR_NEWUSERID]
#define VAL_NEWALIAS	values [VAR_NEWALIAS]
#define VAL_ACTORID	values [VAR_ACTORID]

/* Changes for groups */
#define VAL_MEMBER	values [VAR_LOCAL]
#define VAL_IDENTITY	values [VAR_REMOTE]
#define VAL_MARKS	values [VAR_RIGHTS]

/* Changes for pseudonyms */
#define VAL_ALTEREGO	values [VAR_ALTEREGO]
#define VAL_LOGINID	values [VAR_LOGINID]

/* Changes for documents */
#define VAL_DOMAIN      values [VAR_DOMAIN]
#define VAL_COLLECTION  values [VAR_COLLECTION]
#define VAL_PATH        values [VAR_SIGNFLAGS]
#define VAL_VOLUME      values [VAR_NEWALIAS]

/* Chages for lets */
#define VAL_USER	values [VAR_USER]
#define VAL_CURRENCY	values [VAR_CURRENCY]
#define VAL_BALANCE	values [VAR_BALANCE]

/* Changes for (abstract) rulesets */
#define VAL_TYPE	values [VAR_TYPE]
#define VAL_NAME	values [VAR_NAME]
#define VAL_SELECTOR	values [VAR_SELECTOR]
#define VAL_RULE	values [VAR_RULE]


/* Flags to compose in the combinations */
// #define FLAG_DBENV	(1 << VAR_DBENV    )
// #define FLAG_DBNAME	(1 << VAR_DBNAME   )
#define FLAG_DBTRUNK	(1 << VAR_DBTRUNK  )
#define FLAG_DBSECRET	(1 << VAR_DBSECRET )
#define FLAG_DBPREFIX	(1 << VAR_DBPREFIX )
#define FLAG_DBDOMKEY	(1 << VAR_DBDOMKEY )
#define FLAG_DBSVCKEY	(1 << VAR_DBSVCKEY )
// #define FLAG_OBJTYPE	(1 << VAR_OBJTYPE  )
#define FLAG_LOCAL	(1 << VAR_LOCAL    )
#define FLAG_REMOTE	(1 << VAR_REMOTE   )
#define FLAG_ITERATE	(1 << VAR_ITERATE  )
#define FLAG_RIGHTS	(1 << VAR_RIGHTS   )
#define FLAG_LIST	(1 << VAR_LIST     )
#define FLAG_ALIAS	(1 << VAR_ALIAS    )
#define FLAG_ENDALIAS	(1 << VAR_ENDALIAS )
#define FLAG_SIGNFLAGS	(1 << VAR_SIGNFLAGS)
#define FLAG_NEWUSERID	(1 << VAR_NEWUSERID)
#define FLAG_NEWALIAS	(1 << VAR_NEWALIAS )
#define FLAG_ACTORID	(1 << VAR_ACTORID  )

/* Changes for groups */
#define FLAG_MEMBER	(1 << VAR_MEMBER   )
#define FLAG_IDENTITY	(1 << VAR_IDENTITY )
#define FLAG_MARKS	(1 << VAR_MARKS    )

/* CHanges for pseudonyms */
#define FLAG_ALTEREGO	(1 << VAR_ALTEREGO )
#define FLAG_LOGINID	(1 << VAR_LOGINID  )

/* Changes for documents */
#define FLAG_DOMAIN     (1 << VAR_DOMAIN    )
#define FLAG_COLLECTION (1 << VAR_COLLECTION)
#define FLAG_PATH       (1 << VAR_PATH      )
#define FLAG_VOLUME     (1 << VAR_VOLUME    )

/* Changes for lets */
#define FLAG_USER	(1 << VAR_USER      )
#define FLAG_CURRENCY	(1 << VAR_CURRENCY  )
#define FLAG_BALANCE	(1 << VAR_BALANCE   )

/* Changes for (abstract) rulesets */
#define FLAG_TYPE	(1 << VAR_TYPE      )
#define FLAG_NAME	(1 << VAR_NAME      )
#define FLAG_SELECTOR	(1 << VAR_SELECTOR  )
#define FLAG_RULE	(1 << VAR_RULE      )



