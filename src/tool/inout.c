/* inout.c  --  Replace stdin, stdout, stderr, and exec() a command
 *
 * It is hard to test commands with I/O redirection in CMake or Pypeline
 * but this command wraps around an executable to do just that.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>
#include <fcntl.h>

#include <arpa2/socket.h>


bool replace (int curfd, char *arg) {
	//
	// Storage allocation / initialisation
	int newfd = -1;
	struct sockaddr_storage sock;
	char *endptr;
	unsigned long port;
	//
	// Choose a function for the alternative I/O function
	switch (*arg) {
	case '\0':
		/*
		 * Empty string; deliver like /dev/null would
		 */
		arg = "/dev/null";
		// fall through
	case '/':
		/*
		 * Filename; open RDONLY or WRONLY
		 */
		newfd = open (arg, (curfd == 0) ? O_RDONLY : O_WRONLY | O_CREAT | O_TRUNC);
		break;
	case ':':
		/*
		 * Port number; open as ::1 port;
		 * configure stdout/stderr as server, so that
		 * it blocks until the stdin client connects
		 */
		if (socket_parse ("::1", &arg[1], &sock)) {
			if (curfd == 0) {
				(void) socket_client (&sock, SOCK_STREAM, &newfd);
			} else {
				(void) socket_server (&sock, SOCK_STREAM, &newfd);
			}
		} else {
			errno = ERANGE;
		}
		break;
	case '-':
		/*
		 * Keep the stdio as-is
		 */
		newfd = curfd;
		break;
	default:
		/*
		 * Unsupported notation, cause an error
		 */
		errno = EINVAL;
		break;
	}
	//
	// Report an error on failure
	if (newfd < 0) {
		perror ((curfd == 0)
			? "Failed to create alternate stdin"
			: "Failed to create alternate stdout");
		return false;
	}
	//
	// Install the redirection (which closes the original)
	if (dup2 (newfd, curfd) < 0) {
		perror ((curfd == 0)
			? "Failed to create alternate stdin"
			: "Failed to create alternate stdout");
		return false;
	}
	//
	// Return success
	return true;
}


int main (int argc, char* argv[]) {
	//
	// Simplify the command name
	char *slash = strrchr (*argv, '/');
	if (slash != NULL) {
		*argv = slash + 1;
	}
	//
	// Will we replaced stdin, stdout, both?
	bool do_stdin  = (strstr (*argv, "in" ) != NULL);
	bool do_stdout = (strstr (*argv, "out") != NULL);
	bool do_stderr = (strstr (*argv, "err") != NULL);
	//
	// Check arguments
	if (argc < 4) {
		fprintf (stderr, "Usage: %s %s%scommand...\n",
				*argv,
				do_stdin  ? "stdin-file "  : "",
				do_stdout ? "stdout-file " : ""
		);
		exit (1);
	}
	//
	// Replace stdin and/or stdout and/or stderr
	bool ok = true;
	int argi = 1;
	if (do_stdin) {
		ok = replace (0, argv[argi++]) && ok;
	}
	if (do_stdout) {
		ok = replace (1, argv[argi++]) && ok;
	}
	if (do_stderr) {
		ok = ok && replace (2, argv[argi++]);
	}
	//
	// Execute the remainder of the command
	if (ok) {
		execvp (argv[argi], argv+argi);
		perror ("exec() failed\n");
	}
	//
	// We should never land here
	fprintf (stderr, "%s: Failed fatally\n", *argv);
	exit (1);
}

