/* base85.c  --  Commandline encoder/decoder for BASE85.
 *
 * The usual options are added:
 *  -d to decode instead of encode
 *  -i to ignore garbage
 *  -w to wrap after a number of chars, default a multiple of 16
 *
 * We add a few more options:
 *  -o for output file(s)
 *  -p for pretty, 1x for a space per 16 bytes, 2x also per 4
 *  -e for encoding (possibly recode if -d is also set; also default)
 *  -x for hexadecimal as "the other" coding (instead of binary)
 *  -? for help
 *  -h for help
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <arpa2/util/base85.h>



unsigned decode_buffer (uint8_t *buf, unsigned len, bool last, bool binhex, bool ignore_garbage) {
	static char prefix [5];
	static int prefixlen = 0;
	//
	// Insert the prefix if need be
	if (prefixlen > 0) {
		memmove (buf+prefixlen, buf, len);
		memcpy (buf, prefix, prefixlen);
		len += prefixlen;
		prefixlen = 0;
	}
	//
	// Append a NUL character after the buffer
	buf[len] = '\0';
	//
	// Cycle through the buffer
	unsigned inpos = 0;
	unsigned outpos = 0;
	while (inpos < len) {
		//
		// Skip any non-BASE85 characters if we're ignoring them
		unsigned gbglen = binhex
				? strcspn (&buf[inpos], "0123456789abcdefABCDEF")
				: base85_strcspn (&buf[inpos]);
		if ((gbglen > 0) && !ignore_garbage && (gbglen > strspn (&buf[inpos], " \t\r\n\f"))) {
			fprintf (stderr, "Garbage in input\n");
			exit (1);
		}
		inpos += gbglen;
		//
		// Count the BASE85 characters following
		unsigned blklen = binhex
				? strspn (&buf[inpos], "0123456789abcdefABCDEF")
				: base85_strspn (&buf[inpos]);
		//
		// Length computations, in preparation of prefix retention or error
		unsigned nxtpos = inpos + blklen;
		unsigned modlen = binhex
				? (blklen & 0x00000001)
				: (blklen % 5);
		bool incomplete_chunk = (modlen != 0);
		bool impossible_chunk = (modlen == 1);
		//
		// Possibly retain part of the input in the prefix
		if (incomplete_chunk) {
			if ((nxtpos >= len) && (!last)) {
				prefixlen = binhex ? blklen & 0x000000 : blklen % 5;
				blklen -= prefixlen;
				memcpy (prefix, buf + inpos + blklen, prefixlen);
			} else if (impossible_chunk) {
				fprintf (stderr, "Improper input chunk length %d\n", blklen);
				exit (1);
			}
		}
		//
		// Map BASE85 or HEX to binary output
		if (binhex) {
			uint8_t dig0=0x00, dig1=0x00;
			for (unsigned i=0; i<blklen; i++) {
				dig1 = buf[inpos+i];
				if (dig1 <= '9') {
					dig1 -= '0';
				} else {
					dig1 = (dig1 & 0xdf) - 'A' + 10;
				}
				if ((i & 0x00000001) == 0) {
					dig0 = dig1 << 4;
				} else {
					buf[outpos++] = dig0 | dig1;
				}
			}
		} else {
			if (!base85_decode_bytes (buf + inpos, blklen, buf + outpos)) {
				fprintf (stderr, "Error decoding BASE85 bytes\n");
				exit (1);
			}
			outpos += base85_reprlen_bytes (blklen);
		}
		//
		// Advance buffer inpos to the next chunk or to the end
		inpos = nxtpos;
	}
	//
	// Finally, if this is the last block, reset the static counters
	if (last) {
		prefixlen = 0;
	}
	//
	// Return the final outpos as output buffer length
	return outpos;
}


unsigned encode_buffer (uint8_t *buf, unsigned len, bool last, bool binhex, int wrapln, int pretty) {
	//
	// Prefix buffer of work-half-done
	static char prefix [16];
	static unsigned prefixlen = 0;
	//
	// Create a temporary buffer so we can print without overlap
	uint8_t tmpbuf [8196];
	if (prefixlen > 0) {
		memcpy (tmpbuf, prefix, prefixlen);
	}
	memcpy (tmpbuf + prefixlen, buf, len);
	len += prefixlen;
	prefixlen = 0;
	//
	// Sidetrack an incomplete end block if more will follow
	if (!last) {
		prefixlen = len & 0x0000000f;
		if (prefixlen > 0) {
			len -= prefixlen;
			memcpy (prefix, tmpbuf + len, prefixlen);
		}
	}
	//
	// Keep track of separator counters
	static int col = 0;
	static unsigned idx = 0;
	//
	// Iterate producing output in the original buffer
	unsigned inpos = 0;
	unsigned outpos = 0;
	while (inpos < len) {
		//
		// Produce a 4-byte block in BASE85 or HEX if we have enough...
		if (inpos + 4 < len + (last ? 0 : 1)) {
			if (binhex) {
				sprintf (&buf[outpos], "%02x%02x%02x%02x",
						tmpbuf[inpos+0], tmpbuf[inpos+1],
						tmpbuf[inpos+2], tmpbuf[inpos+3]);
				outpos += 8;
			} else {
				base85_encode_net32 (&tmpbuf[inpos], (char*) &buf[outpos]);
				outpos += 5;
			}
			inpos += 4;
			if (pretty >= 2) {
				buf[outpos++] = ' ';
			}
			idx += 4;
			if ((idx % 16) == 0) {
				col++;
				if ((wrapln > 0) && (col >= wrapln)) {
					buf[outpos++] = '\n';
					col = 0;
				} else if (pretty >= 1) {
					buf[outpos++] = ' ';
				}
			}
		// ...otherwise to append the remainder as BASE85 or HEX
		} else {
			if (binhex) {
				for (unsigned ofs=inpos; ofs<len; ofs++) {
					sprintf (buf + outpos, "%02x", tmpbuf[ofs]);
					outpos += 2;
				}
			} else {
				base85_encode_bytes (&tmpbuf[inpos], len-inpos, (char*) &buf[outpos]);
				outpos += base85_reprlen (len-inpos);
			}
			inpos = len;
			col = 1;	/* last; enforce newline below */
		}
	}
	//
	// Add a trailing newline if need be
	if (last && (col > 0) && (wrapln > 0)) {
		buf[outpos++] = '\n';
	}
	//
	// If this is the last block, reset the static counters
	if (last) {
		col = 0;
		idx = 0;
		prefixlen = 0;
	}
	//
	// Return the output position as length
	return outpos;
}


static const char* short_opts = "edxo:iw:ph?";

static const struct option long_opts[] = {
	{ "decode",         no_argument, NULL, 'd' },
	{ "encode",         no_argument, NULL, 'e' },
	{ "hex",            no_argument, NULL, 'x' },
	{ "out",      required_argument, NULL, 'o' },
	{ "ignore-garbage", no_argument, NULL, 'i' },
	{ "wrap",           no_argument, NULL, 'w' },
	{ "help",           no_argument, NULL, '?' },
	{ "pretty",         no_argument, NULL, 'p' },
	//TODO// "version",
	/* end marker */
	{ NULL, 0, NULL, 0 }
};

int main (int argc, char* argv[]) {
	//
	// Simplify the program name
	char *slash = strrchr (*argv, '/');
	if (slash != NULL) {
		*argv = slash+1;
	}
	//
	// Parse arguments
	char* outv[argc];
	int   outc = 0;
	bool help   = false;
	bool stxerr = false;
	bool decode = false;
	bool encode = false;
	bool ignore = false;
	bool binhex = false;
	int  wrapln = -1;
	int  pretty = 0;
	int c;
	while (c = getopt_long (argc, argv, short_opts, long_opts, NULL), c != -1) {
		switch (c) {
		case 'e':
			stxerr = stxerr || encode;
			encode = true;
			break;
		case 'd':
			stxerr = stxerr || decode;
			decode = true;
			break;
		case 'x':
			stxerr = stxerr || binhex;
			binhex = true;
			break;
		case 'i':
			stxerr = stxerr || ignore;
			ignore = true;
			break;
		case 'w':
			stxerr = stxerr || (wrapln >= 0);
			wrapln = atoi (optarg);
			stxerr = stxerr || (wrapln < 0) || (wrapln > 1000);
			break;
		case 'p':
			pretty++;
			break;
		case 'o':
			outv[outc++] = optarg;
			break;
		case 'h':
		case '?':
			help = true;
			break;
		default:
			fprintf (stderr, "Unknown option -%c\n", (char) c);
			stxerr = true;
			break;
		}
	}
	if (encode && decode && binhex) {
		/* Inconsistent instruction */
		fprintf (stderr, "Please choose your mixed mode from -de, -dx, -xe\n");
		stxerr = true;
	}
	//
	// Apply defaults
	if (!decode) {
		encode = true;
	}
	if ((pretty > 0) && (wrapln <= 0)) {
		wrapln = 2;
	}
	//
	// Fallback to /dev/stdin when no <file>... was provided
	char** inv = &argv[optind];
	int    inc = argc - optind;
	if (inc <= 0) {
		static char *dev_stdin = "/dev/stdin";
		inv = &dev_stdin;
		inc = 1;
	}
	//
	// Fallback to /dev/stdout when no -o <file> were provided
	if (outc == 0) {
		while (outc < inc) {
			outv[outc++] = "/dev/stdout";
		}
	}
	//
	// Require that the number of outputs is 1 or equals the number of inputs
	if ((outc != 0) && (outc != inc)) {
		fprintf (stderr, "Mismatched in/out files, %d in and %d out\n", inc, outc);
		stxerr = true;
	}
	//
	// Possibly print usage information
	if (stxerr || help) {
		fprintf (stderr, "Usage: %s [<option>...] [<file>...]\n"
			"<file>    reads the given file, defaults to standard input\n"
			"<option>  is one of the following:\n"
			" -d       to decode instead of encode\n"
			" -e       to (also) encode as base85\n"
			" -x       to process hexadecimal instead of binary\n"
			" -o<file> to specify an output file for each input file\n"
			" -i       to ignore garbage\n"
			" -w<num>  to wrap after <num> blocks of 16\n"
			" -p       to print a single space Between blocks of 16\n"
			" -pp      to print an extra space between blocks of  4\n"
			" -h, -?   for this usage information\n", *argv);
		exit (stxerr ? 1 : 0);
	}
	//
	// Iterate over input files
	for (int ini=0; ini<inc; ini++) {
		//
		// Open the input file
		int infd = open (inv[ini], O_RDONLY);
		if (infd < 0) {
			perror ("Failed to open input");
			exit (1);
		}
		//
		// Fetch the mode from the input file
		struct stat insb;
		if (fstat (infd, &insb) < 0) {
			perror ("Failed to replicate input file mode");
			exit (1);
		}
		// Open the output file
		int outfd = open(outv[ini], O_WRONLY | O_TRUNC | O_CREAT, insb.st_mode);
		if (outfd < 0) {
			perror ("Failed to open output");
			close (infd);
			exit (1);
		}
		//
		// Read blocks of bytes for processing
		uint8_t buf [10240];
		bool last = false;
		while (!last) {
			//
			// Read up to 4k (extra) bytes into the buffer
			ssize_t gotten = read (infd, buf, 4096);
			if (gotten < 0) {
				perror ("Error reading from file");
				exit (1);
			}
			unsigned len = gotten;
			last = (len < 4096);
			//
			// Now decode and/or encode the buffer in-place
			if (decode || binhex) {
				len = decode_buffer (buf, len, last, !decode, ignore);
			}
			if (encode || binhex) {
				len = encode_buffer (buf, len, last, !encode, wrapln, pretty);
			}
			//
			// Write the output buffer to standard output
			ssize_t written = write (outfd, buf, len);
			if (written < 0) {
				perror ("Failed writing output");
				exit (1);
			} else if (written != len) {
				fprintf (stderr, "Written only %d out of %u bytes\n", (int) written, len);
				exit (1);
			}
		}
		//
		// Close the input and output files
		close (infd);
		close (outfd);
	}
}
