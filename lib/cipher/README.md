# ARPA2 Util for AES128. AES192, AES256

AES128-256 is a fixed depency for consistent block ciphers, with a very
simple use case being the encryption of a UUID to avoid crossover comparisons
between others seeing them.  These are used for long-term consistency, for
example as an account identity.  We cannot be as dynamic as some libraries.

We hereby include the OpenSSL 3.0 code for AES, which is licensed
under the Apache License 2.0.  We modify the work to simplify it.

The Apache License 2.0 is included and applies to all files in this
directory.
