/* access/document.c -- Document Access Control
 *
 * This API for Document Access finds the rights of Remote Identity
 * to work on a Folder or Document.   This is handled as a special
 * form of Access Rules, using the ((user and) volume and) path
 * to a Document or Folder as the Access Name.
 *
 * Inasfar as ARPA2 Reservoir is used, the form starts with
 * `/<colluuid>/` to determine Access Rights to Collections;
 * anything else is stripped.  Other forms that start with one
 * slash only are considered aliases in the ARPA2 Reservoir
 * and yield only `%KV`, so merely permission to know about
 * the existence of this alias.
 *
 * Other forms need to start with `//` and may be continued
 * as `//<user>@<volume>/<path>` or `//<volume>/<path>`, as
 * desired.  These forms are used as literal Access Names.
 *
 * There are currently no facilities for attributes and
 * triggers, but future extensions to this call could allow
 * such forms too.
 *
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>

#include <sys/types.h>
#include <regex.h>

#include "arpa2/digest.h"

#include "arpa2/identity.h"
#include "arpa2/rules.h"
#include "arpa2/access.h"
#include "arpa2/access_document.h"
#include "com_err/arpa2access.h"


access_type access_type_document = {
	0x51, 0xaf, 0x06, 0x8f, 0x49, 0xdd, 0x3f, 0xd4,
	0xa9, 0x4d, 0x37, 0x05, 0x20, 0x73, 0xe9, 0x8e,
};


/* Request structure for Document Access Rights towards a
 * remote ARPA2 Identity.  Rights are the standard set
 * defined in <arpa2/access.h> which is included herein.
 *
 * The most concrete ~selector wins; the %RIGHTS defined
 * at this level are OR-ed, with %V as the zero point.
 * Generic logic interprets the Access Name andreturns %VK
 * for aliases in ARPA2 Reservoir or raises an error for
 * bad grammar.
 */
struct _docreq {
	struct rules_request req;
	const a2id_t *remote;
	a2act_t *optout_actor;
	const char *actorstr;
	unsigned    actorlen;
	access_rights rights;
	uint16_t remote_steps;	/* Initial value: INFINITE_STEPS */
	bool remote_deselect;
	bool see_errno;
};

/* Use 0xffff to mark remote_steps infinite
 */
#define INFINITE_STEPS 0xffff


/* Internal routine.  Match the lowercase Collection UUID as part
 * of an Access Name "/<collUUID>/..." used for ARPA2 Reservoir.
 */
static const char *rex_xsname_reservoir = "^/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/";
//
bool _match_xsname_reservoir (char *xsname) {
	static bool rex_mapped = false;
	static regex_t rex_compiled;
	if (!rex_mapped) {
		int rv = regcomp (&rex_compiled, rex_xsname_reservoir, REG_EXTENDED);
		assert (rv == 0);
		rex_mapped = true;
	}
	regmatch_t pmatch [1];
	return (regexec (&rex_compiled, xsname, 0, pmatch, 0) == 0);
}


/* Internal Routine.  Process a set of Flags as Access Rights.
 */
static bool _docrights (struct rules_request *req) {
	//
	// Access additional fields
	struct _docreq *dreq = (struct _docreq *) req;
	access_rights newflags = req->flags;
	//
	// See if we ran into an acceptable Remote Selector
	if (dreq->remote_deselect) {
		/* Continue the search */
		return true;
	}
	//
	// Possibly learn the suggested Actor Identity
	// Compartmentalise rights flags by Actor Identity
	if (dreq->actorlen == 0) {
		dreq->actorstr = req->varray_str    [ACCESS_DOCUMENT_ACTOR];
		dreq->actorlen = req->varray_strlen [ACCESS_DOCUMENT_ACTOR];
		dreq->rights = 0;
		log_debug ("Rule supplied Actor Identity %.*s", dreq->actorlen, dreq->actorstr);
	} else if (req->varray_strlen [ACCESS_DOCUMENT_ACTOR] > 0) {
		log_warning ("_docrights() multiple matches: old =g%.*s, new %.*s",
			dreq->actorlen,
			dreq->actorstr,
			req->varray_strlen [ACCESS_DOCUMENT_ACTOR],
			req->varray_str    [ACCESS_DOCUMENT_ACTOR]);
		newflags = 0;
	} else {
		dreq->rights = 0;
	}
	//
	// Extend the set of Access Rights with the new ones
	dreq->rights |= newflags;
	//
	// Continue the search
	return true;
}


/* Internal Routine.  Process a Remote Selector.
 *
 * When it mismatches, terrorise the remainder of the rule
 * up to another Remote Selector offering.
 */
bool _docselector (struct rules_request *req, const a2sel_t *pattern) {
	//
	// Access additional fields
	struct _docreq *dreq = (struct _docreq *) req;
	//
	// Compute the abstractions for this new pattern
	uint16_t new_steps;
	if (!a2sel_abstractions (dreq->remote, pattern, &new_steps)) {
		/* There is no specialisation relation */
		dreq->remote_deselect = true;
	} else if (new_steps < dreq->remote_steps) {
		/* This is a better match than seen so far */
		dreq->rights = ACCESS_VISITOR;
		dreq->remote_steps = new_steps;
		dreq->remote_deselect = false;
	} else if (new_steps > dreq->remote_steps) {
		/* Stop because we already had a better match */
		dreq->remote_deselect = true;
	} else {
		/* This matches the best match so far, continue */
		dreq->remote_deselect = false;
	}
	//
	// Return success
	return true;
}


/* Internal Rule: End a rule (and prepare for a potential next).
 *
 * When a Remote Selector selected the rule, this is where it is disabled again.
 */
bool _docendrule (struct rules_request *req) {
	//
	// Access additional fields
	struct _docreq *dreq = (struct _docreq *) req;
	//
	// End the selector-caused deselection
	dreq->remote_deselect = (req->optcb_selector != NULL);
	//
	// Return success
	return true;
}


/* Internal Rule: End a ruleset for Document Access.
 *
 * This routine sets the finally selected values.
 */
bool _docupdate (struct rules_request *req) {
	struct _docreq *dreq = (struct _docreq *) req;
	//
	// Stop now if we failed before
	if (dreq->see_errno) {
		return true;
	}
	//
	// Provide the optout_actor if it is requested
	if (dreq->optout_actor == NULL) {
		/* Skip the field */
	} else if (dreq->actorlen == 0) {
		/* Erase the field */
		memset (dreq->optout_actor, 0, sizeof (a2act_t));
	} else {
		log_debug ("Parsing %.*s as an Actor Identity", dreq->actorlen, dreq->actorstr);
		/* Provide the field from the =g<scene>+<actor> string */
		if (!a2act_parse (dreq->optout_actor, dreq->actorstr, dreq->actorlen, 1)) {
			dreq->see_errno = true;
			return true;
		}
	}
	//
	// Return success
	return true;
}

bool access_document (const a2id_t *remote, char *xsname,
                        const uint8_t *opt_svckey, unsigned svckeylen,
                        const char *opt_acl, unsigned acllen,
                        access_rights *out_rights,
			a2act_t *optout_actor) {
	//
	// Parse the Access Name
	unsigned xsnamelen = strlen (xsname);
	char xsnameuuid [40];
	if (xsname [0] != '/') {
		/* Bad grammar */
		errno = A2XS_ERR_ACCESS_NAME_GRAMMAR;
		return false;
	} else if (xsname [1] == '/') {
		/* Operator-defined volume */
		;
	} else if (_match_xsname_reservoir (xsname)) {
		/* ARPA2 Reservoir, something below a Collection */
		xsnamelen = 38;
		memcpy (xsnameuuid, xsname, xsnamelen);
		xsnameuuid [xsnamelen] = '\0';
		xsname = xsnameuuid;
	} else {
		/* ARPA2 Reservoir, alias so return %KV */
		*out_rights = ACCESS_KNOW | ACCESS_VISITOR;
		return true;
	}
	//
	// Initialise the document access request structure
	struct _docreq dreq;
	memset (&dreq, 0, sizeof (dreq));
        /* Unknown.  Optional.  --  dreq.req.opt_domain = ldom;  */
        dreq.req.opt_type   = access_type_document;
        dreq.req.opt_name   = xsname;
	dreq.remote = remote;
	dreq.optout_actor = optout_actor;
	dreq.rights = ACCESS_VISITOR;
	dreq.actorstr = "";
	dreq.req.optcb_flags      = _docrights;
	dreq.req.optcb_endruleset = _docupdate;
	dreq.req.optcb_endrule    = _docendrule;   /* idempotent   in db */
	//
	// Process the document access request
	bool ok = true;
	if (opt_acl == NULL) {
		ok = ok && rules_dbiterate (&dreq.req,
				opt_svckey, svckeylen,
				xsname,
				(const a2sel_t *) remote);
	} else {
		dreq.req.optcb_selector   = _docselector;
		dreq.remote_steps = INFINITE_STEPS;
		dreq.remote_deselect = true;
		ok = ok && rules_process (&dreq.req,
				opt_acl, acllen,
				true);
	}
	//
	// Learn if errno should be returned
	ok = ok && !dreq.see_errno;
	//
	// Turn not having found a ruleset into blacklisting
	if ((!ok) && (errno == A2XS_ERR_MISSING_RULESET)) {
		dreq.rights = ACCESS_FORBIDDEN;
		memset (optout_actor, 0, sizeof (a2act_t));
		ok = true;
	}
	//
	// Report the result
	*out_rights = ok ? dreq.rights : ACCESS_FORBIDDEN;
	return ok;
}

