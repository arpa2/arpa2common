/* lets.c -- LETS as an Access Type
 *
 * This API answers whether a Remote Identity may operate on
 * a currency or balance.  This is handled as a special form
 * of Access Rules.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */



#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "arpa2/digest.h"

#include "arpa2/identity.h"
#include "arpa2/rules.h"
#include "arpa2/rules_db.h"
#include "arpa2/access.h"
#include "arpa2/access_lets.h"

#include <com_err/arpa2access.h>


/* The UUID of the LETS Access Type, in binary.
 * Registered on http://uuid.arpa2.org/
 */
access_type access_type_lets = {
	0x72, 0x59, 0x4b, 0x3d, 0xe2, 0x9c, 0x3f, 0x2a,
	0x81, 0xc9, 0x42, 0x95, 0x6c, 0xeb, 0x13, 0x47
};


