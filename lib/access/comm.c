/* comm.c -- Communication as an Access Type
 *
 * This API answers whether a Remote Identity may communicate
 * with a Local Identity.  This is handled as a special form
 * of Access Rules, using the local userid (or +servicename)
 * as the Access Name and the local domain as Access Domain,
 * and interpreting Access Rights as black/white/grey lists
 * or honey pot treat for a given Remote Selector.
 *
 * Internally interpreted attributes may setup for alias
 * constraints, overrides and so on.  There is no current
 * use for triggers.  These internals may evolve with new
 * versions of this API.
 *
 * This API works for users as well as +services.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */



#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "arpa2/digest.h"

#include "arpa2/identity.h"
#include "arpa2/rules.h"
#include "arpa2/rules_db.h"
#include "arpa2/access.h"
#include "arpa2/access_comm.h"

#include <com_err/arpa2access.h>


/* The UUID of the Communication Access Type, in binary.
 * Registered on http://uuid.arpa2.org/
 */
access_type access_type_comm = {
	0xb4, 0xf0, 0xfc, 0x38, 0xd4, 0xd7, 0x3b, 0xb9,
	0xad, 0x69, 0x5b, 0xf7, 0x5e, 0xfc, 0x46, 0xdd
};


/* Request structure for Communication Access Rights from a
 * remote ARPA2 Selector (or mostly an ARPA2 Identity) to a
 * local ARPA2 Identity.  The aim is to get white listing,
 * but if that is unattainable grey or even black listing
 * may be returned.  Even worse, the suggestion may follow
 * to give a client a honey pot treat (worse than black list)
 * if possible, when they are counted as severe attacker.
 *
 * The local identity may be changed to match white list
 * acceptance, but the selection process tries to reduce the
 * changes to a minimum while finding white list entries.
 * In future versions, grey and black listed entries and
 * honeypot redirections may also change the local identity.
 * So, always treat the local identity as having changed.
 *
 * Attributes may impose a requirement on signature flags,
 * and these must then be met by the local identity.  The
 * signature itself is assumed to have been tested with
 * a2id_verify() upon entry.  This function is designed to
 * only block invalid identities, but it will pass unsigned
 * identities as well as properly signed identities; this
 * allows us to defer constraints on the signature to this
 * time of Communication Access.
 */
struct _commreq {
	struct rules_request req;
	a2id_t *local;
	a2act_t *optout_actor;
	const a2id_t *remote;
	access_comm_level *out_level;
	const char *whitelist_alias_str;
	unsigned    whitelist_alias_len;
	const char *whitelist_userid_str;
	unsigned    whitelist_userid_len;
	const char *whitelist_actor_str;
	unsigned    whitelist_actor_len;
	uint16_t remote_steps;	/* Initial value: INFINITE_STEPS */
	bool remote_deselect;
	bool see_errno;
};

/* Use 0xffff to mark remote_steps infinite
 */
#define INFINITE_STEPS 0xffff


/* Internal routine: Collect desirable access rights.
 *
 * The main goal is to find the lowest possibe access_comm_level
 * in out_level.  This starts high, with access_comm_undefined.
 * When a lower level is found, prior settings are removed.
 *
 * Before working on this however, the offering is tested to see
 * if it matches the request:
 *  - the actual remote           must satisfy any ACL constraint pattern
 *  - the actual local sigflags   must satisfy any ACL constraints =s...
 *  - the actual local alias/args must satisfy any ACL constraints =a...
 *
 * When an offering is both a good match and an improvement or not
 * a deterioration of the out_level, then:
 *  - the out_level is set as offered in the access_rights
 *  - for whitelisting, the whitelist_alias is set to any =o...
 *  - for whitelisting, the whitelist_userid is set to any =n...
 *
 * @return Always return true for future compatibility.
 */
static bool _commrights (struct rules_request *req) {
	//
	// Access additional fields
	struct _commreq *creq = (struct _commreq *) req;
	//
	// Skip if the rule has been deselected at this point
	if (creq->remote_deselect) {
		return true;
	}
	//
	// Skip if this is not going to improve the out_level
	access_comm_level new_level;
	if (req->flags & ACCESS_COMM_WHITELIST) {
		new_level = access_comm_whitelist;
	} else if (req->flags & ACCESS_COMM_GREYLIST) {
		new_level = access_comm_greylist;
	} else if (! (req->flags & ACCESS_COMM_HONEYPOT)) {
		new_level = access_comm_blacklist;
	} else {
		new_level = access_comm_honeypot;
	}
	log_debug ("_commrights() level: have %d, require <=%d",
				new_level,
				*creq->out_level);
	if (new_level > *creq->out_level) {
		/* Unsuitable offer -- signal not yet done */
		return true;
	}
	//
	// Require that all signature flags are met (require 0 in lieu of s=...)
	const char *sigstr = req->varray_str [ACCESS_COMM_SIGFLAGS];
	char *sigend = NULL;
	unsigned long sigreq = strtoul (sigstr, &sigend, 0);
	if (sigend != sigstr + req->varray_strlen [ACCESS_COMM_SIGFLAGS]) {
		log_error ("_commrights() wrong syntax for signature: s=%.*s",
				req->varray_strlen [ACCESS_COMM_SIGFLAGS],
				req->varray_str    [ACCESS_COMM_SIGFLAGS]);
		return true;
	}
	log_debug ("_commrights() signature flags: have 0x%x, require 0x%lx",
				creq->local->sigflags,
				sigreq);
	if ((sigreq & ~ (unsigned long) creq->local->sigflags) != 0) {
		/* Unsuitable offer -- signal not yet done */
		return true;
	}
	//
	// Require that alias constraints are not broken,
	// but meet the 'a' required alias.  If the only
	// (or added) purpose is to override an alias,
	// use 'o' instead.  For services, the 'a' could
	// be rather important to find the proper rights.
	// A trailer '@' requires no (further) aliases or
	// service arguments.
	const char *ali = req->varray_str    [ACCESS_COMM_ALIAS];
	unsigned alilen = req->varray_strlen [ACCESS_COMM_ALIAS];
	//
	// Determine offsets and check optional '@' alias terminator
	if (alilen > 0) {
		const char *txt = creq->local->txt;
		const uint16_t *ofs = creq->local->ofs;
		//
		// Distinguish service and userid
		uint16_t ofsali, ofsend;
		if (ofs [A2ID_OFS_PLUS_SERVICE] < ofs [A2ID_OFS_PLUS_SVCARGS]) {
			/* The local is a +servicename */
			ofsali = ofs [A2ID_OFS_PLUS_SVCARGS];
			ofsend = ofs [A2ID_OFS_USERID];
		} else {
			/* The local is a userid */
			ofsali = ofs [A2ID_OFS_PLUS_ALIASES];
			ofsend = ofs [A2ID_OFS_OPEN_END];
		}
		log_debug ("_commrights() alias/args: have \"%.*s\", require \"%.*s\"",
				ofsend - ofsali, txt + ofsali,
				alilen,          ali);
		//
		// Possibly require no added aliases/svcargs
		if (ali [alilen-1] == '@') {
			alilen--;
			unsigned plusalilen = (alilen > 0) ? alilen + 1 : 0;
			if (ofsali + plusalilen != ofsend) {
				/* No final alias, not acceptable */
				return true;
			}
		}
		//
		// Continue testing for all alias constraints but =a@
		if (alilen > 0) {
			//
			// Require the presence of aliases/svcargs
			if (txt [ofsali++] != '+') {
				/* No aliases, not acceptable */
				/* TODO: No alias ok when =a empty? */
				return true;
			}
			//
			// Check the size of aliases/svcargs
			if (ofsali + alilen > ofsend) {
				/* Constraint is longer */
				return true;
			} else if ((ofsali + alilen < ofsend)
					&& (txt [ofsali + alilen] != '+')) {
				/* Attempt to match last alias partially */
				return true;
			}
			//
			// Check the string data of aliases/svcargs
			if (0 != memcmp (txt + ofsali, ali, alilen)) {
				/* Different alias, skip it */
				return true;
			}
		}
	}
	//
	// We found a match -- celebrate with a debug message
	log_debug ("_commrights() found a suitable Commununication Access Rule");
	//
	// When the new level is lower, reset whitelist variables.
	if (new_level < *creq->out_level) {
		creq->whitelist_alias_len  = 0;
		creq->whitelist_userid_len = 0;
		creq->whitelist_actor_len  = 0;
	}
	//
	// Select the most specific new userid/service
	if (new_level != access_comm_whitelist) {
		/* skip any =n... variable */ ;
	} else if (creq->whitelist_userid_len == 0) {
		creq->whitelist_userid_str = req->varray_str    [ACCESS_COMM_NEWUSER];
		creq->whitelist_userid_len = req->varray_strlen [ACCESS_COMM_NEWUSER];
	} else if (req->varray_strlen [ACCESS_COMM_NEWUSER] > 0) {
		log_warning ("_commrights() multiple matches: old =n%.*s, new =n%.*s",
			creq->whitelist_userid_len,
			creq->whitelist_userid_str,
			req->varray_strlen [ACCESS_COMM_NEWUSER],
			req->varray_str    [ACCESS_COMM_NEWUSER]);
	}
	//
	// Select the most specific new alias/args
	if (new_level != access_comm_whitelist) {
		/* skip any =o... variable */ ;
	} else if (creq->whitelist_alias_len == 0) {
		creq->whitelist_alias_str = req->varray_str    [ACCESS_COMM_NEWALIAS];
		creq->whitelist_alias_len = req->varray_strlen [ACCESS_COMM_NEWALIAS];
	} else if (req->varray_strlen [ACCESS_COMM_NEWALIAS] > 0) {
		log_warning ("_commrights() multiple matches: old =o%.*s, new =o%.*s",
			creq->whitelist_alias_len,
			creq->whitelist_alias_str,
			req->varray_strlen [ACCESS_COMM_NEWALIAS],
			req->varray_str    [ACCESS_COMM_NEWALIAS]);
	}
	//
	// Select the most specific new actor
	if (new_level != access_comm_whitelist) {
		/* skip any =g... variable */ ;
	} else if (creq->whitelist_actor_len == 0) {
		creq->whitelist_actor_str = req->varray_str    [ACCESS_COMM_ACTOR];
		creq->whitelist_actor_len = req->varray_strlen [ACCESS_COMM_ACTOR];
	} else if (req->varray_strlen [ACCESS_COMM_ACTOR] > 0) {
		log_warning ("_commrights() multiple matches: old =g%.*s, new %.*s",
			creq->whitelist_actor_len,
			creq->whitelist_actor_str,
			req->varray_strlen [ACCESS_COMM_ACTOR],
			req->varray_str    [ACCESS_COMM_ACTOR]);
	}
	//
	// In any case, the output level is the new level
	*creq->out_level = new_level;
	//
	// Return success
	return true;
}


/* Internet Rule: Process a Remote Selector.
 *
 * When it mismatches, terrorise the remainder of the rule
 * up to another Remote Selector offering.
 */
bool _commselector (struct rules_request *req, const a2sel_t *pattern) {
	//
	// Access additional fields
	struct _commreq *creq = (struct _commreq *) req;
	//
	// Compute the abstractions for this new pattern
	uint16_t new_steps;
	if (!a2sel_abstractions (creq->remote, pattern, &new_steps)) {
		/* There is no specialisation relation */
		creq->remote_deselect = true;
	} else if (new_steps < creq->remote_steps) {
		/* This is a better match than seen so far */
		*creq->out_level = access_comm_undefined;
		creq->remote_steps = new_steps;
		creq->remote_deselect = false;
	} else if (new_steps > creq->remote_steps) {
		/* Stop because we already had a better match */
		creq->remote_deselect = true;
	} else {
		/* This matches the best match so far, continue */
		creq->remote_deselect = false;
	}
	//
	// Return success
	return true;
}


/* Internal Rule: End a rule (and prepare for a potential next).
 *
 * When a Remote Selector deselected the rule, this is where its terror ends.
 */
bool _commendrule (struct rules_request *req) {
	//
	// Access additional fields
	struct _commreq *creq = (struct _commreq *) req;
	//
	// End the selector-caused deselection
	creq->remote_deselect = false;
	//
	// Return success
	return true;
}


/* Internal Routine: End of ruleset for Access Communication.
 *
 * When returning a whitelist entry, there may be a new userid/service
 * from =n... and there may be new alias/args from =o...
 *
 * This routine sets the finally selected values in the local identity.
 */
bool _commupdate (struct rules_request *req) {
	//
	// Access additional fields
	struct _commreq *creq = (struct _commreq *) req;
	//
	// Stop now if we failed before
	if (creq->see_errno) {
		return true;
	}
	//
	// Provide the optout_actor if it is requested
	if (creq->optout_actor == NULL) {
		/* Skip the field */
	} else if (creq->whitelist_actor_len == 0) {
		/* Erase the field */
		memset (creq->optout_actor, 0, sizeof (a2act_t));
	} else {
		/* Provide the field from the =g<scene>+<actor> string */
		char actstr [sizeof (creq->optout_actor->txt) + 5];
		unsigned actlen = snprintf (actstr, sizeof (creq->optout_actor->txt) + 3, "%.*s%s",
			creq->whitelist_actor_len, creq->whitelist_actor_str,
			creq->local->txt + creq->local->ofs [A2ID_OFS_AT_DOMAIN]);
		if (!a2act_parse (creq->optout_actor, actstr, actlen, 1)) {
			creq->see_errno = true;
			return true;
		}
	}
	//
	// Quick references to local identity txt and ofs
	char     *ltxt = creq->local->txt;
	uint16_t *lofs = creq->local->ofs;
	//
	// Replace the userid or service if it is overridden for white listing
	// When this is done, the alias/args are reset as a side-effect
	if (creq->whitelist_userid_len > 0) {
		//
		// Decide where to start overwriting
		int ovridx, endidx;
		if (*creq->whitelist_userid_str == '+') {
			/* The local is a +servicename */
			ovridx = A2ID_OFS_PLUS_SERVICE;
			endidx = A2ID_OFS_PLUS_SVCARGS;
		} else {
			/* The local is a userid */
			ovridx = A2ID_OFS_USERID;
			endidx = A2ID_OFS_PLUS_ALIASES;
		}
		unsigned newend = lofs [ovridx] + creq->whitelist_userid_len;
		//
		// Now actually shift the alias end to the new offset
		bool ok = a2sel_textshift (creq->local, endidx, newend, true);
		if (!ok) {
			creq->see_errno = true;
			return true;
		}
		//
		// Replace the text
		memcpy (ltxt + lofs [ovridx], creq->whitelist_userid_str, creq->whitelist_userid_len);
		//
		// If this is a +service, we setup an extra offset
		if (ovridx == A2ID_OFS_PLUS_SERVICE) {
			/* The local is a +servicename */
			lofs [A2ID_OFS_SERVICE] = lofs [A2ID_OFS_PLUS_SERVICE] + 1;
		}
		//
		// Now remove any content after endidx up to A2ID_OFS_PLUS_SIG
		ok = a2sel_textshift (creq->local, A2ID_OFS_PLUS_SIG, newend, true);
		if (!ok) {
			/* Highly unlikely to fail; we're just removing */
			creq->see_errno = true;
			return true;
		}
	}
	//
	// Replace the alias if it is overridden for white listing
	// Prefix with '+' if an alias is set; remove a '+' if it is removed
	if (creq->whitelist_alias_len > 0) {
		//
		// Decide where to start overwriting
		int ovridx;
		if (lofs [A2ID_OFS_PLUS_SERVICE] < lofs [A2ID_OFS_PLUS_SVCARGS]) {
			/* The local is a +servicename */
			ovridx = A2ID_OFS_PLUS_SVCARGS;
		} else {
			/* The local is a userid */
			ovridx = A2ID_OFS_PLUS_ALIASES;
		}
		int validx = ovridx + 1;
		int endidx = ovridx + 2;
		int newend = lofs [ovridx] + 1 + creq->whitelist_alias_len;
		//
		// Now actually shift the alias end to the new offset
		bool ok = a2sel_textshift (creq->local, endidx, newend, true);
		if (!ok) {
			creq->see_errno = true;
			return true;
		}
		//
		// Override the alias and set corresponding offsets
		lofs [validx] = lofs [ovridx] + 1;
		lofs [endidx] = lofs [validx] + creq->whitelist_alias_len;
		ltxt [lofs [ovridx]] = '+';
		memcpy (ltxt + lofs [validx], creq->whitelist_alias_str, creq->whitelist_alias_len);
	}
	//
	// Return success
	return true;
}


bool access_comm (const a2id_t *remote, a2id_t *local,
			const uint8_t *opt_svckey, unsigned svckeylen,
			const char *opt_acl, unsigned acllen,
			access_comm_level *out_level,
			a2act_t *optout_actor) {
	*out_level = access_comm_undefined;
	bool ok = true;
	//
	// The Access Name is different for user and service
	char     *ltxt = local->txt;
	uint16_t *lofs = local->ofs;
	char    *nameptr;
	unsigned namelen;
	if (ltxt [lofs [A2ID_OFS_PLUS_SERVICE]] == '+') {
		nameptr = ltxt                         + lofs [A2ID_OFS_PLUS_SERVICE];
		namelen = lofs [A2ID_OFS_PLUS_SVCARGS] - lofs [A2ID_OFS_PLUS_SERVICE];
	} else {
		nameptr = ltxt                         + lofs [A2ID_OFS_USERID];
		namelen = lofs [A2ID_OFS_PLUS_ALIASES] - lofs [A2ID_OFS_USERID];
	}
	//
	// Copy the Access Name on stack to use as the Rules Name
	char name [namelen + 1];
	memcpy (name, nameptr, namelen);
	name [namelen] = '\0';
	//
	// Find the local domain name -- the one to grant access
	char *ldom = ltxt + lofs [A2ID_OFS_DOMAIN];
	//
	// Fill the request structure to allow svckey derivation
	struct _commreq creq;
	memset (&creq, 0, sizeof (creq));
	creq.req.opt_domain       = ldom;
	creq.req.opt_type         = access_type_comm;
	creq.req.opt_name         = name;
	creq.out_level            = out_level;
	creq.local                = local;
	creq.optout_actor         = optout_actor;
	creq.remote               = remote;
	creq.remote_steps         = INFINITE_STEPS;
	creq.whitelist_userid_str = "";
	creq.whitelist_alias_str  = "";
	creq.whitelist_actor_str  = "";
	creq.req.optcb_flags      = _commrights;
	creq.req.optcb_selector   = _commselector;  /* never occurs in db */
	creq.req.optcb_endrule    = _commendrule;   /* idempotent   in db */
	creq.req.optcb_endruleset = _commupdate;
	//
	// If opt_svckey is needed but NULL, setup the default
	rules_dbkey domkey;
	rules_dbkey svckey;
	if ((opt_svckey == NULL) && (opt_acl == NULL)) {
		if (!rules_dbkey_domain (domkey,
				"", 0,
				local->txt + local->ofs [A2ID_OFS_DOMAIN])) {
			return false;
		}
		if (!rules_dbkey_service (svckey,
				domkey, sizeof (domkey),
				access_type_comm)) {
			return false;
		}
		opt_svckey = svckey;
		svckeylen = sizeof (svckey);
	}
	//
	// Lookup the Ruleset or process the one given
	if (opt_acl == NULL) {
		ok = ok && rules_dbiterate (&creq.req,
				opt_svckey, svckeylen,
				name,
				(const a2sel_t *) remote);
	} else {
		ok = ok && rules_process (&creq.req,
				opt_acl, acllen,
				true);
	}
	//
	// Learn if errno should be returned
	ok = ok && !creq.see_errno;
	//
	// Turn not having found a ruleset into blacklisting
	if ((!ok) && (errno == A2XS_ERR_MISSING_RULESET)) {
		*out_level = access_comm_blacklist;
		memset (optout_actor, 0, sizeof (a2act_t));
		ok = true;
	}
	//
	// Produce the expected out_level and return value
	if (!ok) {
		*out_level = access_comm_undefined;
	} else if (*out_level == access_comm_undefined) {
		*out_level = access_comm_blacklist;
	}
	return ok;
}

