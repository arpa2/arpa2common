/* database.c -- DBlogic for Rules under Access Control, Groups, ...
 *
 * The database RuleDB stores records that each hold a Ruleset,
 * consisting of NUL-terminated Rules with generic words:
 *
 *   #commentlabel =xvalue %FLAGS ^trigger ~selector
 *
 * Lookups are based on an LMDBkey which is a beginning of a secure
 * hash of lookup data; the remainder of the secure hash is stored
 * as a RESTkey in the beginning of the record, which is checked.
 * This logic is concealed within the database.c implementation.
 *
 * There is another useful prefix to the database values, and that
 * is a trunk code.  These are ignored by applications, but serve
 * bulk treatment of database entries, usually related to a certain
 * remote provider that originates or retrieves the data.  The
 * trunk code is provided during rules_opendb() and defaults to 0.
 *
 * The default location is "/var/lib/arpa2/rules/" but may be
 * set in RULES_ENVIRONMENT_PATH and the strongest option is the
 * envvar $ARPA2_RULES_DIR; the name of the database defaults
 * to "RuleDB" byt may be set in RULES_DATABASE_NAME; the size of
 * the database defaults to 1048576000L but may be overridden with
 * RULES_DATABASE_SIZE; the number of databases is maximally 10 but
 * this may be overridden with RULES_DATABASE_COUNT_MAX.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <arpa/inet.h>   // For htonl() mapping trunk numbers into trk

#include <lmdb.h>

#include <arpa2/com_err.h>
#include <com_err/arpa2access.h>
#include <arpa2/except.h>

#include "arpa2/digest.h"
#include "arpa2/identity.h"
#include "arpa2/access.h"

#include "arpa2/rules_db.h"

#include "arpa2/util/abspath.h"



bool rules_dbrollback (struct rules_db *ruledb) {
	mdb_txn_abort (ruledb->txn);
	return true;
}


bool rules_dbcommit (struct rules_db *ruledb) {
	return (0 == mdb_txn_commit (ruledb->txn));
}


bool rules_dbsuspend (struct rules_db *ruledb) {
	mdb_txn_reset (ruledb->txn);
	return true;
}


bool rules_dbresume (struct rules_db *ruledb) {
	return (0 == mdb_txn_renew (ruledb->txn));
}


bool rules_dbopen (struct rules_db *ruledb, bool rdonly, uint32_t trunk) {
	bool gotenv = false;
	bool gottxn = false;
	bool gotdbi = false;
	bool gotcrs = false;
	bool ok = true;
	//
	// Only open the LMDB environment if it is not shared
	// Note: We could use global variables, though we might
	//       have to split reading and writing environments
	if (ruledb->env == NULL) {
		//
		// Open the LMDB environment
		ok = ok && (0 == mdb_env_create (&ruledb->env));
		gotenv = ok;
		//
		// Try to set a max number of databases
		mdb_env_set_maxdbs (ruledb->env, RULES_DATABASE_COUNT_MAX);
		//
		// Try to set a maximum database size, and make it big, 1000 MB
		mdb_env_set_mapsize (ruledb->env, RULES_DATABASE_SIZE);
		//
		// Open the environment
		int dbmode = 0644;
		int dbflags = MDB_NOTLS;
		char *dbenv = getenv ("ARPA2_RULES_DIR");
		if (dbenv == NULL) {
			dbenv = RULES_ENVIRONMENT_PATH;
		}
		if (ok && !is_abspath (dbenv)) {
			log_error ("ARPA2 Rules Dir must be an absolute path, got %s", dbenv);
			ok = false;
		}
		dbflags |= ( rdonly ? MDB_RDONLY : 0 );
		ok = ok && (0 == mdb_env_open (ruledb->env, dbenv, dbflags, dbmode));
	}
	//
	// Open a (or "the") transaction on the database
	int txnflags = rdonly ? MDB_RDONLY : 0;
	ok = ok && (0 == mdb_txn_begin (ruledb->env, NULL, txnflags, &ruledb->txn));
	gottxn = ok;
	//
	// Open the database (with "ruledb" as a default name)
	int dbiflags = MDB_CREATE | MDB_DUPSORT | MDB_INTEGERKEY;
	char *dbiname = "ruledb";
	/* We should only run mdb_dbi_open() in one thread at the same time */
	ok = ok && (0 == mdb_dbi_open (ruledb->txn, dbiname, dbiflags, &ruledb->dbi));
	gotdbi = ok;
	//
	// Open a cursor for the database and transaction
	ok = ok && (0 == mdb_cursor_open (ruledb->txn, ruledb->dbi, &ruledb->crs));
	gotcrs = ok;
	//
	// Setup the trunk number as a big endian 32-bit integer
	ruledb->trk.netint32 = htonl (trunk);
	//
	// Return on success
	if (ok) {
		return ok;
	}
	//
	// Cleanup on error
	if (gotcrs) {
		mdb_cursor_close (ruledb->crs);
		ruledb->crs = NULL;
	}
	if (gotdbi) {
		/* Probably need to mutex with mdb_dbi_open */
		mdb_dbi_close (ruledb->env, ruledb->dbi);
		/* Not a pointer... ruledb->dbi = NULL; */
	}
	if (gottxn) {
		mdb_txn_abort (ruledb->txn);
		ruledb->txn = NULL;
	}
	if (gotenv) {
		/* Close the environment; we could also keep it for reuse */
		mdb_env_close (ruledb->env);
		ruledb->env = NULL;
	}
	//
	// Return failure
	return ok;
}


bool rules_dbclose (struct rules_db *ruledb) {
	/* Close the cursor */
	mdb_cursor_close (ruledb->crs);
	ruledb->crs = NULL;
	/* Probably need to mutex with mdb_dbi_open */
	mdb_dbi_close (ruledb->env, ruledb->dbi);
	/* Not a pointer... ruledb->dbi = NULL; */
	rules_dbrollback (ruledb);
	ruledb->txn = NULL;
	/* Cleanup our own administrative data */
	ruledb->got = false;
	memset (ruledb->dig, 0, sizeof (ruledb->dig));
	/* Close the environment, but keep its data for later reuse */
	return true;
}


bool rules_dbget (struct rules_db *ruledb, rules_dbkey digkey, MDB_val *out_dbdata) {
	log_data ("Reading key", digkey, A2MD_OUTPUT_SIZE, false);
	bool ok = true;
	//
	// Copy the key into the RuleDB structure
	memcpy (ruledb->dig, digkey, sizeof (ruledb->dig));
	//
	// Prepare the LDBMkey
	MDB_val dbkey;
	dbkey.mv_data = digest2lmdbkey (ruledb->dig);
	dbkey.mv_size = RULES_LMDBKEY_SIZE;
	//
	// Iterate for entries with the RESTkey + value
	MDB_cursor_op crsop = MDB_SET_KEY;
	bool notfound = true;
	MDB_val curdata;
	while (ok && notfound) {
		//
		// Retrieve the dbloop/dbnext Ruleset
		int dbrv = mdb_cursor_get (ruledb->crs, &dbkey, &curdata, crsop);
		if (dbrv != 0) {
			errno = A2XS_ERR_DATABASE_READFAIL;
			// accept not-found conditions
			ok = (dbrv == MDB_NOTFOUND);
			// do not continue
			break;
		}
		//
		// Compare trunk codes other than htonl(TRUNK_ANY) which is 0
		log_data ("Considered", curdata.mv_data, curdata.mv_size, 0);
		notfound = (ruledb->trk.netint32 != 0) &&
			(curdata.mv_size >= RULES_TRUNK_SIZE) && 
			(0 != memcmp (curdata.mv_data,
				ruledb->trk.netbytes,
				RULES_TRUNK_SIZE));
		log_detail ("After trunk comparison, notfound = %s", notfound ? "true" : "false");
		//
		// Compare the RESTkey to the one we wanted
		// Future versions may decrypt-then-check with the RESTkey
		notfound = notfound ||
			(curdata.mv_size < RULES_TRUNK_SIZE + RULES_RESTKEY_SIZE) ||
			(0 != memcmp (((uint8_t *) curdata.mv_data) + RULES_TRUNK_SIZE,
				digest2restkey (ruledb->dig),
				RULES_RESTKEY_SIZE));
		log_detail ("After restkey comparison, notfound = %s", notfound ? "true" : "false");
		crsop = MDB_NEXT_DUP;
	}
	//
	// Reduce the size of what was found
	// When nothing was found, return the empty set
	if (ok) {
		if (notfound) {
			out_dbdata->mv_data = "";
			out_dbdata->mv_size = 0;
		} else {
			out_dbdata->mv_data = ((uint8_t *) curdata.mv_data) + RULES_TRUNK_SIZE + RULES_RESTKEY_SIZE;
			out_dbdata->mv_size =              curdata.mv_size  - RULES_TRUNK_SIZE - RULES_RESTKEY_SIZE;
		}
	}
	//
	// Update the database management to memorise success
	ruledb->got = !notfound;
	//
	// Return success or failure -- where an empty set is ok
	return ok;
}


bool rules_dbset (struct rules_db *ruledb, MDB_val *in0_dbdata, MDB_val *in1_dbdata) {
	bool ok = true;
	log_data ("Writing key", ruledb->dig, A2MD_OUTPUT_SIZE, false);
	//MAYBE_NOTFOUND// assertxt (ruledb->got, "Called rules_dbset() without prior success on rules_dbget()");
	//
	// Ensure the NUL termination on each of the inputs
	assert ((in0_dbdata->mv_size == 0) || (((uint8_t *) in0_dbdata->mv_data) [in0_dbdata->mv_size-1] == 0x00));
	assert ((in1_dbdata->mv_size == 0) || (((uint8_t *) in1_dbdata->mv_data) [in1_dbdata->mv_size-1] == 0x00));
	//
	// Simplified special case for record deletion
	if ((in0_dbdata->mv_size == 0) && (in1_dbdata->mv_size == 0)) {
		if (ruledb->got) {
			ok = ok && (0 == mdb_cursor_del (ruledb->crs, 0));
			ruledb->got = false;
		}
		return ok;
	}
	//
	// Construct the LMDBkey
	MDB_val dbkey;
	dbkey.mv_data = digest2lmdbkey (ruledb->dig);
	dbkey.mv_size = RULES_LMDBKEY_SIZE;
	//
	// Construct the RESTkey + value
	uint8_t dbval_data [RULES_TRUNK_SIZE + RULES_RESTKEY_SIZE + in0_dbdata->mv_size + in1_dbdata->mv_size];
	memcpy (dbval_data,
			ruledb->trk.netbytes,
			RULES_TRUNK_SIZE);
	memcpy (dbval_data + RULES_TRUNK_SIZE,
			digest2restkey (ruledb->dig),
			RULES_RESTKEY_SIZE);
	memcpy (dbval_data + RULES_TRUNK_SIZE + RULES_RESTKEY_SIZE,
			in0_dbdata->mv_data,
			in0_dbdata->mv_size);
	memcpy (dbval_data + RULES_TRUNK_SIZE + RULES_RESTKEY_SIZE + in0_dbdata->mv_size,
			in1_dbdata->mv_data,
			in1_dbdata->mv_size);
	//
	// Construct the MDB_val for RESTkey + value
	MDB_val dbval;
	dbval.mv_data =         dbval_data ;
	dbval.mv_size = sizeof (dbval_data);
	//
	// Write to the database
	int dbflags = ruledb->got ? MDB_CURRENT : 0;
	ok = ok && (0 == mdb_cursor_put (ruledb->crs, &dbkey, &dbval, dbflags));
	//
	// Update the flag that states whether we got data
	ruledb->got = ok;
	//
	// Return success or failure
	return ok;
}


bool rules_dbkey_domain (rules_dbkey domkey,
			const uint8_t *opt_dbkey, int dbkeylen,
			const char *xsdomain) {
	a2md_state st;
	bool ok  = true;
	assert ((dbkeylen == 0) || (opt_dbkey != NULL));
	//
	// Initialise the hash state
	ok = ok && a2md_open (&st);
	bool gothash = ok;
	//
	// Write an (empty?) database key; write the accessed domain
	ok = ok && a2md_write_msg (&st, opt_dbkey, dbkeylen);
	ok = ok && a2md_write_msg (&st, xsdomain, strlen (xsdomain));
	//
	// Produce output if ok; but close if gothash
	ok = gothash && a2md_close (&st, ok ? domkey : NULL) && ok;
	log_detail ("rules_dbkey_domain: dbkeylen %d, xsdomain %s, domkey %02x %02x",
			dbkeylen, xsdomain, domkey [0], domkey [1]);
	//
	// Return whether we were successful
	return ok;
}


bool rules_dbkey_service (rules_dbkey svckey,
			const uint8_t *domkey, unsigned domkeylen,
			const uint8_t xstype [16]) {
	a2md_state st;
	bool ok  = true;
	//
	// Initialise the hash state
	ok = ok && a2md_open (&st);
	bool gothash = ok;
	//
	// Write the input key and the Access Type
	ok = ok && a2md_write_msg (&st, domkey,  domkeylen);
	ok = ok && a2md_write_msg (&st, xstype,         16);
	//
	// Produce output if ok; but close if gothash
	ok = gothash && a2md_close (&st, ok ? svckey : NULL) && ok;
	//
	// Return whether we were successful
	log_detail ("rules_dbkey_service: domkey %02x %02x, xstype %02x %02x, svckey %02x %02x",
			domkey [0], domkey [1], xstype [0], xstype [1], svckey [0], svckey [1]);
	return ok;
}


bool rules_dbkey_selector (rules_dbkey reqkey,
			const uint8_t *svckey, unsigned svckeylen,
			const char *xsname,
			const a2sel_t *selector) {
	a2md_state st;
	bool ok  = true;
	//
	// Initialise the hash state
	ok = ok && a2md_open (&st);
	bool gothash = ok;
	//
	// Write the input key, the Access Name and Selector
	ok = ok && a2md_write_msg (&st, svckey,       svckeylen);
	ok = ok && a2md_write_msg (&st, xsname, strlen (xsname));
	ok = ok && a2md_write     (&st, selector->txt,
	                                selector->ofs [A2ID_OFS_END]);
	//
	// Produce output if ok; but close if gothash
	ok = gothash && a2md_close (&st, ok ? reqkey : NULL) && ok;
	//
	// Return whether we were successful
	log_detail ("rules_dbkey_selector: svckey %02x %02x, xsname %s, selector %s, reqkey %02x %02x",
			svckey [0], svckey [1], xsname, selector->txt, reqkey [0], reqkey [1]);
	return ok;
}


bool rules_dbkey_parse (rules_dbkey outkey,
                        const char *hexkey, int hexkeylen) {
	bool ok = true;
	char twohex [3];
	twohex [2] = '\0';
	//
	// Check the size of the input to match the (present) outkey size
	if (hexkeylen == 0) {
		hexkeylen = strlen (hexkey);
	}
	ok = ok && (2 * (RULES_LMDBKEY_SIZE + RULES_RESTKEY_SIZE) == hexkeylen);
	//
	// Parse one output byte at a time
	uint8_t *outptr = (uint8_t *) outkey;
	while (ok && (hexkeylen > 0)) {
		//
		// Read two hex characters
		twohex [0] = *hexkey++;
		twohex [1] = *hexkey++;
		ok = ok && (twohex [1] != 'x') && (twohex [1] != 'X');
		char *endptr = NULL;
		unsigned long outbyte = strtoul (twohex, &endptr, 16);
		ok = ok && (endptr == twohex + 2);
		//
		// Store the output byte and shift to the next
		*outptr++ = (uint8_t) outbyte;
		hexkeylen -= 2;
	}
	//
	// Return success or failure
	if (!ok) {
		errno = EINVAL;
	}
	return ok;
}

