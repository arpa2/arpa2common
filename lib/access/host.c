/* Mapping host names to domain names.
 *
 * A placeholder for the general idea of mapping host names to
 * domains that for ARPA2 Identity and Access Control.  A simple
 * implementation may involve stripping a level, more advanced
 * solutions may query LMDB which is kept up to date with LDAP
 * via a SteamWorks Pulley backend.
 *
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#include <stdlib.h>
#include <string.h>



/* Lookup the realm for a fully qualified host name.  The realm is
 * often a trailer of the hostname, though local conventions may
 * differ; a domain may be used as a hostname, and shared servers
 * may not even show overlap with the domain.  The mechanisms for
 * determining this may involve DNS records (with DNSSEC security)
 * or a configuration database, or merely a textual mapping.
 *
 * This function is used by protocols that communicate host names
 * instead of domain names.  A typical example is HTTP.  On such
 * systems, aliases must not interfere with the specified host
 * names; the best chance for that is to not lookup the client-sent
 * host name, but the configured canonical host name (web servers
 * tend to have a canonical ServerName and additionally allow
 * ServerAlias names that are also recognised as Host: header).
 *
 * This function returns NULL if no realm could be found for the
 * supplied host name.
 */
const char *access_host2domain (const char *fqdn_hostname) {
	const char *ptr = strchr (fqdn_hostname, '.');
	if (ptr == NULL) {
		return NULL;
	}
	ptr++;
	return ptr;
}

