/* access/actor.c -- Access Control for assuming an Actor Identity.
 *
 * This API allows applications to switch a user identity to an actor,
 * usually with the result that a subset of access rights are found.
 * Since these switches may be optional parts of the protocol, the API
 * trivially permits switching to the same identity.
 *
 * The customary approach is that a login_identity is established
 * through authentication, and possibly already switched to an
 * authorisation identity, which may subsequently be switched to ever
 * more specific identities as a result of protocol or service coding.
 * Each time, the original identity is posed as current_id and the new
 * identity poses as requested_id.  Each of these switches must be
 * validated via Actor Access, and failure denies the requested action
 * to the user.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>

#include <string.h>

#include <arpa2/identity.h>
#include <arpa2/group.h>
#include <arpa2/rules.h>
#include <arpa2/rules_db.h>
#include <arpa2/access_actor.h>


/** @brief The Rules Type for pseudonyms.
 *
 * This is the registered UUID value
 * for use with pseudonyms.  See http://uuid.arpa2.org/ for details.
 *
 * Rights are `ACCESS_OPERATE` or `%T` and `ACCESS_ADMIN` or `%A`.
 */
rules_type rules_type_pseudonym = {
	0x6b, 0xb8, 0x22, 0x7b, 0x59, 0x0c, 0x32, 0x89,
	0x8e, 0xe5, 0xf0, 0x84, 0x1e, 0x55, 0xae, 0x8e
};



/* Request structure for Pseudonym Access.  Continues the basic
 * `struct rules_request` for customary parser handling, along with
 * a collected `access_flags` field that provides the output.
 */
struct _pseudoreq {
	struct rules_request req;
	rules_flags collected_rights;
};


/* Collect upcall data, that is Access Rights flags, from the various
 * upcalls that are theoretically (but not practically) possible for the
 * given combination of (Remote) Selector (or current_id) and the
 * Access Name (the requested pseudonym in canonical form).
 *
 * This callback function has prototype rules_endrule_upcall and is applied
 * to the extended rules_request structure above called `_pseudoreq`.
 * It combines (with logical OR) the flags the remain at the end of a rule.
 */
static bool access_actor_pseudonym_upcall (struct rules_request *self) {
	//
	// Map the update to a pointer to access_rights
	struct _pseudoreq *psr = (struct _pseudoreq *) self;
	//
	// Add the end-of-rule Access Rights to the collected_rights
	psr->collected_rights |= psr->req.flags;
	//
	// Return true for future compatibility
	return true;
}


/** @brief Test the rights for a switch from current_id to a pseudonym.
 *
 * This function evaluates a ruleset for Pseudonym Actor Access, and
 * returns the Acess Rights found.  Usually this consists of ACCESS_OPERATE
 * or `%T` and possible ACCESS_ADMIN or `%A`, where the former allows the
 * switch to the pseudonym and the latter grants changes to who may use
 * the pseudonym.  Very often, both flags will be assigned together.
 *
 * @param[in] current_id is the identity that has been validated
 * through authentication and possible preceding calls to
 * \a access_actor().  It may be foreign and is therefore assumed
 * to have been parsed with \ref a2id_parse_remote() rather than
 * \ref a2id_parse().  If it is a local identity, it should not be
 * a service identity (starting with a `+` character).  It is not
 * common to include later `+` characters in the user name, but it
 * may be used if so desired.
 *
 * @param[in] requested_id is the pseudonym that the user is trying
 * to use in subsequent actions.  This may or may not have aliases, but
 * any database lookups would not involve user name aliases.  This is a
 * local name and is assumed to have been parsed with \ref a2id_parse().
 *
 * @param[in] opt_svckey_pseudo may be NULL or a Service Key for
 * pseudonym database access.  When set to NULL, the default is used.
 *
 * @param[in] svckeypseudolen specifies the length in bytes of the
 * \a opt_svckey_pseudo.  It is only meaningful when that is not NULL.
 *
 * @param[in] opt_pseudorules may be NULL or a concatenation of
 * zero-terminated strings with rules for the group.  When not NULL,
 * it is an alternative to the \a opt_svckey_pseudo parameter.
 *
 * @param[in] pseudoruleslen is the total length of \a opt_pseudorules,
 * strings, in bytes, including the very last zero terminator.
 *
 * @param[out] out_rights is set to the Access Rights according to the
 * policy for the pseudonym in the \a requested_id.  It is meaningful
 * even when the return value is false, which case it only holds
 * ACCESS_VISITOR or %V.  When a pseudonym was found, then any
 * Access Rules that made it to the end of any rule in the ruleset
 * are combined with that basic result.  Usually, this means that
 * ACCESS_OEPRATE or %T is added to grant the use of the pseudonym,
 * and/or that ACCESS_ADMIN or %A is added to grant changes to the
 * people who may change the \a current_id values permitted for the
 * pseudonym.
 *
 * @returns true only when the Policy Rule for the Pseudonym in the
 * \a requested_id was found and evaluated properly, regardless of
 * value, or false otherwise.
 */
bool access_actor_pseudonym (const a2id_t *current_id, const a2id_t *requested_id,
		const uint8_t *opt_svckey_pseudo, unsigned svckeypseudolen,
		const char *opt_pseudorules, unsigned pseudoruleslen,
		access_rights *out_rights) {
	//
	// The end result is helpful for proper exit after shared cleanup
	bool success = false;
	*out_rights = ACCESS_VISITOR;
	//
	// Do not allow a service to behave as a pseudonym identity
	if (requested_id->ofs [A2ID_OFS_SERVICE] > 0) {
		goto fail;
	}
	//
	// Database operational information, and cleanup flags
	bool db_open  = false;
	bool db_found = false;
	struct rules_db db_rules;
	MDB_val db_ruleset;
	memset (&db_rules  , 0, sizeof (db_rules  ));
	memset (&db_ruleset, 0, sizeof (db_ruleset));
	//
	// Clone the requested_id before changing it
	a2id_t requested_pseudonym;
	memcpy (&requested_pseudonym, requested_id, sizeof (requested_pseudonym));
	//
	// Remove the aliases, if any
	if (!a2sel_textshift (&requested_pseudonym,
				A2ID_OFS_AT_DOMAIN,
				requested_pseudonym.ofs [A2ID_OFS_PLUS_ALIASES],
				true)) {
		goto fail;
	}
	//
	// We have now reduced the problem of detecting the pseudonym
	// or any of its aliases in requested_id to detecting the
	// pseudonym itself in requested_pseudonym
	;
	//
	// If needed, provide for a service key
	if ((opt_svckey_pseudo == NULL) && (opt_pseudorules == NULL)) {
		//
		// Compute the domain key
		rules_dbkey domkey;
		rules_dbkey svckey;
		char *domain = requested_pseudonym.txt + requested_pseudonym.ofs [A2ID_OFS_DOMAIN];
		if (!rules_dbkey_domain (domkey, "", 0, domain)) {
			goto fail;
		}
		//
		// Compute the service key
		if (!rules_dbkey_service (svckey,
					domkey, sizeof (domkey),
					rules_type_pseudonym)) {
			goto fail;
		}
		//
		// Replace the missing svckey with the computed default
		opt_svckey_pseudo = svckey;
		svckeypseudolen = sizeof (svckey);
	}
	//
	// We now have proper values in opt_svckey_pseudo and svckeypseudolen
	;
	//
	// Compute the entire Rules DB key and retrieve the ruleset
	if (opt_pseudorules == NULL) {
		//
		// Compute the request key
		rules_dbkey reqkey;
		if (!rules_dbkey_selector (reqkey,
					opt_svckey_pseudo, svckeypseudolen,
					requested_pseudonym.txt,
					current_id)) {
			goto fail;
		}
		//
		// Open the Rules DB
		db_open = rules_dbopen_rdonly (&db_rules);
		//
		// Retrieve *only* this ruleset from the Rules DB
		log_data ("Rules DB lookup key", reqkey, sizeof (reqkey), false);
		db_found = rules_dbget (&db_rules, reqkey, &db_ruleset);
		db_found = db_found && (db_ruleset.mv_size > 0);
		if (!db_found) {
			goto fail;
		}
		//
		// Replace the missing pseudorules with the db_ruleset
		opt_pseudorules = db_ruleset.mv_data;
		pseudoruleslen  = db_ruleset.mv_size;
	}
	//
	// We now have proper values in opt_pseudorules and pseudoruleslen
	;
	//
	// Process the ruleset, now in opt_pseudorules, with the ruleset parser
	struct _pseudoreq psr;
	memset (&psr, 0, sizeof (psr));
	psr.req.optcb_endrule = access_actor_pseudonym_upcall;
	if (!rules_process (&psr.req, opt_pseudorules, pseudoruleslen, !db_found)) {
		goto fail;
	}
	//
	// Store the collected rights into the output word
	*out_rights |= psr.collected_rights;
	//
	// We now have certainty that the rights were obtained, so that
	// we may transit to the pseudonym or any of its aliases
	success = true;
	//
	// Cleanup any open resources
fail:
	if (db_found) {
		; // Not really used
	}
	if (db_open) {
		rules_dbclose (&db_rules);
	}
	//
	// Return the findings
	return success;
}


struct group_upcall_data {
	const a2id_t *current_id;
	const a2id_t *requested_id;
	bool found;
};



/* Test if an group member (mapping group identity to external identity)
 * matches the request from an external identity to act as group member.
 */
static bool access_actor_group_upcall (void *updata, group_marks marks,
			const a2act_t *_sender, const a2act_t *recipient,
			const char *delivery, unsigned deliverylen) {
	//
	// Map the upcall data structure
	struct group_upcall_data *data = updata;
	bool maybe = !data->found;
	//
	// The recipient is the group-internal address, which must equal
	// the requested_id (representing the "inner address").
	maybe = maybe && (0 == strcasecmp (recipient->txt, data->requested_id->txt));
	//
	// The delivery/len is the group-external address, which must equal
	// the current_id (representing the "outer address").
	maybe = maybe && (deliverylen == data->current_id->ofs [A2ID_OFS_END]);
	maybe = maybe && (0 == strncasecmp (delivery, data->current_id->txt, deliverylen));
	//
	// Collect the overall result
	if (maybe) {
		data->found = true;
	}
	//
	// For future compatability
	return true;
}


/* @brief Full-blown check if a user may act from another identity.
 *
 * Users have authenticated to obtain an (initial) identity,
 * and may optionally step to other identities from there.
 * During this process, they are assigned a new identity,
 * sometimes referred to as their Actor Identity.  It will
 * replace the original identity, and be further considered
 * as if it was the original identity.  It may for instance
 * be used for logging, for additional access_actor() calls
 * but also in other Access Control calls.
 *
 * It will often suffice to fill only the first two parameters
 * and fill the rest with NULL and 0 values, and to check the
 * return value for approval.
 *
 * @param[in] current_id is the identity that has been validated
 * through authentication and possible preceding calls to
 * \a access_actor().  It may be foreign and is therefore assumed
 * to have been parsed with \ref a2id_parse_remote() rather than
 * \ref a2id_parse().
 *
 * @param[in] requested_id is the identity that the user is
 * asking to use in subsequent actions.  When approved, this will
 * take the place of \a current_id.  It is a local name and is
 * assumed to have been parsed with \ref a2id_parse().  Parsing the
 * string with \ref a2act_parse() only makes sense for the form of
 * a group member, and that will be tried internally for that case.
 *
 * @param[in] opt_svckey_group may be NULL or a Service Key for
 * group database access.  When set to NULL, the default is used.
 *
 * @param[in] svckeygrouplen specifies the length in bytes of the
 * \a opt_svckey_group.  It is only meaningful when that is not NULL.
 *
 * @param[in] opt_grouprules may be NULL or a concatenation of
 * zero-terminated strings with rules for the group.  When not NULL,
 * it is an alternative to the \a opt_svckey parameter.
 *
 * @param[in] groupruleslen is the total length of \a opt_grouprules
 * in bytes, including the very last zero terminator.
 *
 * @param[in] opt_svckey_pseudo may be NULL or a Service Key for
 * pseudonym database access.  When set to NULL, the default is used.
 *
 * @param[in] svckeypseudolen specifies the length in bytes of the
 * \a opt_svckey_pseudo.  It is only meaningful when that is not NULL.
 *
 * @param[in] opt_pseudorules may be NULL or a concatenation of
 * zero-terminated strings with rules for the group.  When not NULL,
 * it is an alternative to the \a opt_svckey_pseudo parameter.
 *
 * @param[in] pseudoruleslen is the total length of \a opt_pseudorules,
 * strings, in bytes, including the very last zero terminator.
 *
 * @returns When the \a requested_id may replace the \a current_id
 *	then true is returned, otherwise false.
 */
bool access_actor_general (const a2id_t *current_id, const a2id_t *requested_id,
		const uint8_t *opt_svckey_group, unsigned svckeygrouplen,
		const char *opt_grouprules, unsigned groupruleslen,
		const uint8_t *opt_svckey_pseudo, unsigned svckeypseudolen,
		const char *opt_pseudorules, unsigned pseudoruleslen) {
	//
	// Ban domain selectors (that would not be a proper ARPA2 Identity)
	if (current_id->ofs [A2ID_OFS_DOT_DOMAIN] != current_id->ofs [A2ID_OFS_DOMAIN]) {
		return false;
	}
	//
	// Trivially permit a requested_id that is more specialised
	// or equal to the current_id (support services adding args)
	if (a2sel_subseteq (requested_id, current_id)) {
		return true;
	}
	//
	// Do not permit switching from or to services
	if ((current_id->ofs [A2ID_OFS_SERVICE] > 0) || (requested_id->ofs [A2ID_OFS_SERVICE] > 0)) {
		return false;
	}
	//
	// Permit switching to a group membership when the requested_id
	// takes on a form <group>[+<subgroup>]+<member>@<domain>
	a2act_t member;
	if (a2act_parse (&member, requested_id->txt, requested_id->ofs [A2ID_OFS_END], 1)) {
		struct group_upcall_data data = {
			.current_id = current_id,
			.requested_id = requested_id,
			.found = false
		};
		const char *filter [2] = { member.txt, NULL };
		//
		// Iterate over group members, and set data.found for a proper match
		if (group_iterate (&member, filter,
					GROUP_PROVE, 0,
					opt_svckey_group, svckeygrouplen,
					opt_grouprules, groupruleslen,
					access_actor_group_upcall, &data)) {
			//
			// If we found a matching member, permit the identity switch
			if (data.found) {
				return true;
			}
		}
	}
	//
	// Permit switching to a pseudonum (as found in the first word)
	access_rights pseudo_rights = ACCESS_VISITOR;
	if(access_actor_pseudonym (current_id, requested_id,
				opt_svckey_pseudo, svckeypseudolen,
				opt_pseudorules, pseudoruleslen,
				&pseudo_rights)) {
		//
		// The pseudonym was found.  Any aliases on it were removed
		// before checking, but they are now silently allowed because
		// they form aliases on the pseudonym, which is alright.
		// All we need to do now is check for ACCESS_OPERATE or %T.
		if ((pseudo_rights & ACCESS_OPERATE) == ACCESS_OPERATE) {
			return true;
		}
		//
		// Without ACCESS_OPERATE we skip the Pseudonym check
		;
	}
	//
	// No evidence of a switchable identity was found, so reject
	return false;
}
