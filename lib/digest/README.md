# ARPA2 Util for SHA256

SHA256 is a fixed depency for consistent key derivations and such.
We cannot be as dynamic as underlying libraries.

We hereby include the OpenSSL 3.0 code for SHA256, which is licensed
under the Apache License 2.0.  We modify the work to simplify it.

The Apache License 2.0 is included and applies to all files in this
directory.
