# ARPA2 Identity error codes for com_err, shared in errno
#
# Only extensions made AT THE END OF THE TABLE give
# binary-compatible encodings.
#
# Please add a brief description and start it with
# A2ID or a longer form like ARPA2 Identity/Selector
# to help easy discovery of the component that caused
# an error.
#
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>


error_table A2ID

error_code A2ID_ERR_SIZE,
	"ARPA2 Identity or Selector too long"

error_code A2ID_ERR_IDENTITY_SYNTAX,
	"ARPA2 Identity syntax error"

error_code A2ID_ERR_SELECTOR_SYNTAX,
	"ARPA2 Selector syntax error"

error_code A2ID_ERR_MISSING_SIGNATURE,
	"ARPA2 Identity should have a signature"

error_code A2ID_ERR_NOKEY,
	"ARPA2 Identity signatures need a key"

error_code A2ID_ERR_SIGN,
	"ARPA2 Identity signing failed"

error_code A2ID_ERR_INVALID_SIGNATURE,
	"ARPA2 Identity has an invalid signature"

error_code A2ID_ERR_KEY_ADD,
	"ARPA2 Identity failed to add key"

error_code A2ID_ERR_EXPIRED,
	"ARPA2 Identity expired"

error_code A2ID_ERR_SIGFLAGS_LOOSENESS,
	"ARPA2 Identity lacks required signature flags"

error_code A2ID_ERR_EXPIREDAY_LOOSENESS,
	"ARPA2 Identity expires later than required"

error_code A2ID_ERR_SIGDATA,
	"ARPA2 Identity error in signature data"

error_code A2ID_ERR_INCOMPATIBLE_DOMAIN,
	"ARPA2 Identity/Selector mismatch in domain"

error_code A2ID_ERR_INCOMPATIBLE_USER,
	"ARPA2 Identity/Selector mismatch in username"

error_code A2ID_ERR_INCOMPATIBLE_SERVICE,
	"ARPA2 Identity/Selector mismatch in service"

error_code A2ID_ERR_ITERATOR_DONE,
	"ARPA2 Selector ended Iteration"

error_code A2ID_ERR_ITERATOR_FAILED,
	"ARPA2 Selector failed Iteration"

error_code A2ID_ERR_ACTOR_SYNTAX,
	"ARPA2 Actor Identity syntax error"

error_code A2ID_ERR_MEMBER_SYNTAX,
	"ARPA2 Group Member Identity syntax error"

end

