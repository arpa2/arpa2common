/** @defgroup arpa2cipher Block Ciphers.
 * @{
 *
 * Very simple wrappers that enable us to switch between backends.
 * Functions and symbols are prefixed with a2bc_ and A2BC_.
 *
 * The functions support authentication including additional data.
 * If this is desired, such data is supplied as non-NULL value in
 * the encryption or decryption calls.
 *
 * These wrappers are static inline functions.  This allows using a
 * different instantiation in different files, by defining a single
 * symbol A2BC_STYLE before including this file.  If it seems more
 * efficient to avoid repeated code, then it may actually be a good
 * idea to define functions that are meaningful to the _application_
 * and wrap those around these _technically_ useful inliners.
 *
 * SPDX-FileCopyrightText: Copyright 2020-2024 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */



#ifndef ARPA2_CIPHER_H
#define ARPA2_CIPHER_H


#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <arpa2/except.h>
#include <arpa2/digest.h>

/* We can usually include libarpa2cipher and thereby avoid
 * deprecation warnings (and future erros) which make no sense to
 * our requirement of a consistent symmetric encryption system.
 */
//NONEED// #include <arpa2/cipher_aes.h>


#ifdef __cplusplus
extern "C" {
#endif


/** @defgroup arpa2cipher_base Block Encryption and Decryption API
 * @{
 *
 * This is the customary interface to block cipher functions.
 * It gets the work done, but lets you bend down to check all the
 * bits to avoid problems.
 *
 * NOTE WELL, this is a basic building block for experts.
 * That is, people who are able and willing to craft their own
 * key extraction, mode, message authentication, iv, salt, added
 * data and so on.  Do not skim over these terms if you have no
 * idea what they mean.
 */



/** @brief Keys will be stored in a structure, which is considered opaque.
 */
struct a2bc_key;



/** @brief Setup a key for the block cipher, both to encrypt and decrypt.
 *
 * The use of separating between key settings and using them is
 * that it allows reuse of work that translates between individual
 * calls to block cipher work in a2bc_encrypt() and a2bc_decrypt().
 *
 * This is a moderately friendly function, in that it does not
 * show distinction between encryption and decryption keys, or
 * their schedules.  And it stores the size of the key in its
 * internals.  The A2BC_KEY_SIZE is set high enough to allow
 * all of this.  Given the compactness of symmetric keys this
 * is not at all likely to cause dismay.
 *
 * They key provides its own size, and a usable setup would match
 * it with A2MD_OUTPUT_SIZE (that is, make it usable as a key).
 *
 * NOTE WELL, this is a basic building block for experts.
 * That is, people who are able and willing to craft their own
 * key extraction, mode, message authentication, iv, salt, added
 * data and so on.  Do not skim over these terms if you have no
 * idea what they mean.
 *
 * The function returns true on success and false on failure.
 */
static inline bool a2bc_setkey (
		const uint8_t *entropy,          /* tightly packed */
		uint16_t entropylen,    /* allows A2MD_OUTPUT_SIZE */
		struct a2bc_key *keyout);         /* A2BC_KEY_SIZE */


/** @brief Block encryption operation, just the basic essential.
 *
 * The key must have been prepared using a2bc_setkey() and it
 * will be setup using the configured underlying crypto.
 * The operation works on one key and one block only.
 *
 * To reverse this action, use a2bc_decrypt() with the same key.
 *
 * The plaintxt and ciphertxt regions may overlap, but be careful
 * about sequential calls on contiguous regions.
 *
 * NOTE WELL, this is a basic building block for experts.
 * That is, people who are able and willing to craft their own
 * key extraction, mode, message authentication, iv, salt, added
 * data and so on.  Do not skim over these terms if you have no
 * idea what they mean.
 *
 * The function does not fail.
 */
static inline void a2bc_encrypt (
		const struct a2bc_key *keyin,  /* from a2bc_setkey */
		const void *plaintxt,     /* A2BC_BLOCK_SIZE bytes */
		void *ciphertxt);         /* A2BC_BLOCK_SIZE bytes */


/** @brief Block decryption operation, just the basic essential.
 *
 * The key must have been prepared using a2bc_setkey() and it
 * will be setup using the configured underlying crypto.
 * The operation works on one key and one block only.
 *
 * To reverse this action, use a2bc_decrypt() with the same key.
 *
 * The plaintxt and ciphertxt regions may overlap, but be careful
 * about sequential calls on contiguous regions.
 *
 * NOTE WELL, this is a basic building block for experts.
 * That is, people who are able and willing to craft their own
 * key extraction, mode, message authentication, iv, additional
 * data and so on.  Do not skim over these terms if you have no
 * idea what they mean.
 *
 * The function does not fail.
 */
static inline void a2bc_decrypt (
		const struct a2bc_key *keyin,  /* from a2bc_setkey */
		const void *ciphertxt,    /* A2BC_BLOCK_SIZE bytes */
		void *plaintxt);          /* A2BC_BLOCK_SIZE bytes */


/** @} */



/* Listing of values for the A2BC styles */
#define A2BC_STYLE_NULL 1
#define A2BC_STYLE_LIBSSL_AES 2


#ifndef A2BC_STYLE_DEFAULT
#define A2BC_STYLE_DEFAULT A2BC_STYLE_LIBSSL_AES
#endif

#ifndef A2BC_STYLE
#define A2BC_STYLE A2BC_STYLE_DEFAULT
#endif




/***** NULL STYLE BLOCK CIPHER *****/


/** @defgroup arpa2cipher_null NULL Crypto for Block Encryption and Decryption
 * @{
 */


#if A2BC_STYLE == A2BC_STYLE_NULL


#warning "A2BC_STYLE_NULL selected: ___no___encryption___ or validation will be used"

#define A2BC_KEY_SIZE 1
#define A2BC_BLOCK_SIZE 16

struct a2bc_key {
	char x [1];
};


static inline bool a2bc_setkey (const uint8_t *entropy, uint16_t entropylen, struct a2bc_key *keyout) {
	if (entropylen != A2BC_KEY_SIZE) {
		return false;
	}
	keyout->x = 'X';
	return true;
}

static inline void a2bc_encrypt (const struct a2bc_key *keyin, const void *plaintxt, void *ciphertxt) {
	if (keyin->x == 'X') {
		memcpy (ciphertxt, plaintxt, A2BC_BLOCK_SIZE);
	}
}

static inline void a2bc_decrypt (const struct a2bc_key *keyin, const void *ciphertxt, void *plaintxt) {
	if (keyin->x == 'X') {
		memcpy (plaintxt, ciphertxt, A2BC_BLOCK_SIZE);
	}
}


/** @} */




/***** LIBSSL WITH AES BLOCK CIPHER *****/


#elif A2BC_STYLE == A2BC_STYLE_LIBSSL_AES

/** @defgroup arpa2cipher_libssl Block Encryption and Decryption Internals from LibSSL
 * @{
 */


#if   A2MD_OUTPUT_SIZE >= 32
#define A2BC_KEY_SIZE 32
#elif A2MD_OUTPUT_SIZE >= 24
#define A2BC_KEY_SIZE 24
#elif A2MD_OUTPUT_SIZE >= 16
#define A2BC_KEY_SIZE 16
#else
#error "Your A2MD_OUTPUT_SIZE is too small to use for A2BC_KEY_SIZE"
#endif

#if A2MD_OUTPUT_SIZE != A2BC_KEY_SIZE
#warning "Not all your A2MD_OUTPUT_SIZE bits are usable as A2BC_KEY_SIZE"
#endif

#define A2BC_BLOCK_SIZE 16


/* START OpenSSL code fragment */
struct aes_key_st {
# ifdef AES_LONG
    unsigned long rd_key[4 * (14 + 1)];
# else
    unsigned int rd_key[4 * (14 + 1)];
# endif
    int rounds;
};
typedef struct aes_key_st AES_KEY;
/* END OpenSSL code fragment */

/*
 *
 * Mixed definitions from OpenSSL with ARPA2 naming
 *
 */

/** @brief Internal encryption key setting for LibSSL core AES128/196/256.
 */
int a2bc_aes_encrypt_setkey(const unsigned char *userKey, const int bits, AES_KEY *key);

/** @brief Internet decryption key adaptor for LibSSL core AES128/196/256.
 *
 * This assumes that a2bc_aes_encrypt() was called to prepare the \a key.
 * It then shuffles bits around such that it becomes a decryption key.
 */
int a2bc_aes_decrypt_updkey(const unsigned char *userKey, const int bits, AES_KEY *key);

/** @brief Internal single-block encryption for 16-byte blocks using LibSSL core AES/128/196/256.
 */
void a2bc_aes_encrypt(const unsigned char *in, unsigned char *out, const AES_KEY *key);

/** @brief Internal single-block decryption for 16-byte blocks using LibSSL core AES/128/196/256.
 */
void a2bc_aes_decrypt(const unsigned char *in, unsigned char *out, const AES_KEY *key);


/** @Brief ARPA2 combined key structure for encryption and decryption
 */
struct a2bc_key {
	AES_KEY ekey, dkey;
};


static inline bool a2bc_setkey (const uint8_t *entropy, uint16_t entropylen, struct a2bc_key *keyout) {
	if ((entropylen != 16) && (entropylen != 24) && (entropylen != 32)) {
		goto fail;
	}
	int status;
	status = a2bc_aes_encrypt_setkey (entropy, entropylen * 8, &keyout->ekey);
	if (status < 0) {
		goto fail;
	}
	memcpy (&keyout->dkey, &keyout->ekey, sizeof (AES_KEY));
	status = a2bc_aes_decrypt_updkey (entropy, entropylen * 8, &keyout->dkey);
	if (status < 0) {
		goto fail;
	}
	return true;
fail:
	memset (keyout, 0, sizeof (struct a2bc_key));
	return false;
}

static inline void a2bc_encrypt (const struct a2bc_key *keyin, const void *plaintxt, void *ciphertxt) {
	a2bc_aes_encrypt (plaintxt, ciphertxt, &keyin->ekey);
}

static inline void a2bc_decrypt (const struct a2bc_key *keyin, const void *ciphertxt, void *plaintxt) {
	a2bc_aes_decrypt (ciphertxt, plaintxt, &keyin->dkey);
}


/** @} */



/***** NO BLOCK CIPHER STYLE SELECTED *****/


#else /* A2BC_STYLE */


#error "Invalid value for A2BC_STYLE or A2BC_STYLE_DEFAULT"


#endif /* A2BC_STYLE */



/***** GENERIC DEFINITIONS *****/




#ifdef __cplusplus
}
#endif

#endif /* ARPA2_CIPHER_H */

/** @} */
