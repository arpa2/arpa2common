/** @defgroup arpa2digest Message Digest
 * @{
 *
 * Very simple wrappers that enable us to switch between backends.
 * Functions and symbols are prefixed with a2md_ and A2MD_.
 *
 * The operations mimic the style of processing a file.  Operations
 * monitor sizes, and it is possible to write size-prefixed and even
 * tagged messages to support parseable input.
 *
 * These wrappers are static inline functions.  This allows using a
 * different instantiation in different files, by defining a single
 * symbol A2MD_STYLE before including this file.  If it seems more
 * efficient to avoid repeated code, then it may actually be a good
 * idea to define functions that are meaningful to the _application_
 * and wrap those around these _technically_ useful inliners.
 *
 * @TODO: Keyed hashing with a2md_open_keyed ()
 * @TODO: Markers in an otherwise linear byte sequence
 *
 * SPDX-FileCopyrightText: Copyright 2020-2024 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */



#ifndef ARPA2_DIGEST_H
#define ARPA2_DIGEST_H


#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <endian.h>
#include <time.h>

#include <arpa2/except.h>

/* We can usually include libarpa2digest and thereby avoid
 * deprecation warnings (and future errors) which make no sense to
 * our requirement of a consistent key derivation system.
 */
#include <arpa2/digest_sha256.h>


#ifdef __cplusplus
extern "C" {
#endif


/** @defgroup arpa2digest_base Standard Functionality
 * @{
 *
 * This is the customary interface to a message digest function.
 * It gets the work done, but lets you bend down to check all the
 * bits to avoid problems.
 *
 * We advise you to see these as imported basic functions, and
 * suggest you look at the Extended Functionality for ways of
 * using message digests that make it easy to use them well.
 */


/** @brief An abstract state buffer type -- instantatie for action.
 */
struct a2md_state;


/** @brief Open a digest by initialising pre-allocated state.
 *
 * This overwrites the current state, so it may be used
 * as initialisation and/or as a reset.
 */
static bool a2md_open (struct a2md_state *state);


/** @brief Close a digest by wiping its state.
 *
 * When opt_output is not NULL, first write the final digest state
 * to it.  The size of this output is A2MD_OUTPUT_SIZE.
 */
static bool a2md_close (struct a2md_state *state, uint8_t *opt_output);


/** @brief Write a block of data.
 *
 * The size will be checked to be positive.  The interface
 * mimics that of writing to a file.
 *
 * Be careful with this one.  It is the standard interface
 * to hash functions, and treats the input as one long flow
 * that belongs together.  Fine if that is what you want.
 * But you cannot assume structure separating the data from
 * data supplied in subsequent calls.  If you want to have
 * more structure, use a2md_write_msg() and possibly also
 * a2md_write_tag().
 */
static bool a2md_write (struct a2md_state *state, const void *data, int size);



/** @} */

/** @defgroup arpa2digest_extended Extended Functions
 * @{
 *
 * The extended functions added here make it easier to work with
 * message digests, and help to avoid problems.  As a general
 * rule, aim to write things into a message digest that you could
 * parse back.  This helps to avoid alternate paths to write the
 * same overall input into a hash, which would undermine the whole
 * idea of using one (in a very difficult to detect manner).
 *
 * To allow this approach, you usually end up prefixing lengths
 * and/or tags, or formatted ASCII representations.  That is
 * precisely what these extensions allow you to do easily.
 */


/** @brief Clone a digest so the values may fork.
 */
static bool a2md_clone (struct a2md_state *target, const struct a2md_state *source);


/** @brief Write a block of data with a maximum size.
 *
 * The size will also be checked to be possible.
 */
static bool a2md_write_max (struct a2md_state *state, const void *dataptr, int datalen, int datalen_max);


/** @brief Write a binary message with a 16-bit length prefix.
 *
 * This means that the start of the next block cannot
 * be mistaken, by effectively marking the message end.
 * The concern here is to ensure that if we ever wound
 * back our hash input, it should be parseable; that
 * assures that our input cannot arrive in other forms
 * and open a hole to a security attack.
 *
 * Separately written messages will not be silently
 * concatenated into an overall byte sequence like
 * with plain a2md_write().  The flow is a sequence
 * however, and must have a fixed order of messages.
 * If you also want to branch, or make choices of
 * how to proceed the message sequence, you can use
 * a2md_write_tag() where your grammar branches.
 */
static bool a2md_write_msg (struct a2md_state *state, const void *dataptr, int datalen);


/** @brief Write a type code that can toggle the flow if the
 * input were to be parsed.
 *
 * This branches in the
 * middle of a linear succession of messages, and
 * is part of a strategy of making the hash input
 * parseable, so it cannot have input delivered in
 * different calls, but with the same hash output.
 *
 * The interpretation of the tag is entirely yours;
 * it may even be specific to the point where it
 * occurs, or it may setup between variants much
 * later in the process.  The one concern is that
 * it must not be confused with length prefixes at
 * the same point in the flow.  A safe way to avoid
 * that is to always insert the tag and refrain
 * from skipping it for default cases with distinct
 * message lengths.
 */
static bool a2md_write_tag (struct a2md_state *state, uint16_t tag);


/** @brief Write formatted text into the state.
 *
 * Though it may
 * be combined with tagging and messages, it is also
 * possible to fill a digest with just a2md_printf().
 * The concern should be the same as with the other
 * approaches, namely, what is written into it ought
 * to be parseable to ensure that no multiple ways of
 * writing the input could lead to the same output.
 */
static inline bool a2md_printf (struct a2md_state *state, const char *format, ...);


/** @brief Produce a message digest of a desired size up to 65535.
 *
 * This is produced from a clone, so the original digest
 * sequence is not interrupted.  The interface mimics
 * reading bytes from a file.
 *
 * If you need multiple outputs that should be unrelated,
 * use a2md_read_key() instead.
 */
static inline bool a2md_read (const struct a2md_state *state, uint8_t *output, uint16_t output_size);


/** @brief Produce a message digest of a desired size up to 65535.
 *
 * This is produced from a clone, and scattered with a key
 * genertion tag, so that the output can produce material
 * for various uses.  For example, a number of independently
 * used keys.
 *
 * You can choose the key tags as you like; they should just
 * be different if the output is to be different.  The digest
 * will be individually closed after adding each key tag, so
 * it should not be possible to derive one from another.
 *
 * If you do not need the variation that the key brings,
 * you may want to use a2md_read() instead.
 */
static inline bool a2md_read_key (const struct a2md_state *state, uint16_t key, uint8_t *output, uint16_t output_size);


/* We define a number of standard key codes for convenience,
 * but without any force to actually use them.  The names
 * are suggestive of:
 *  - two uses; SIGNing and ENCRyption
 *  - three parties: UPstream, DOWNstream and MIDdle man
 *  - communication between parties /and/ storage to self
 *
 * Your only concern is that keys are always different
 * when differnt uses are made from the same digest state.
 * Using these names are just /one/ way of doing that.
 */
#define A2MD_STDKEY_SIGN_UP2DOWN	0xffff
#define A2MD_STDKEY_SIGN_DOWN2UP	0xfffe
#define A2MD_STDKEY_SIGN_UP2MID		0xfffd
#define A2MD_STDKEY_SIGN_MID2UP		0xfffc
#define A2MD_STDKEY_SIGN_MID2DOWN	0xfffb
#define A2MD_STDKEY_SIGN_DOWN2MID	0xfffa
#define A2MD_STDKEY_SIGN_UP2UP		0xfff9
#define A2MD_STDKEY_SIGN_DN2DN		0xfff8
#define A2MD_STDKEY_SIGN_MID2MID	0xfff7
#define A2MD_STDKEY_SIGN		0xfff6
//
#define A2MD_STDKEY_ENCR_UP2DOWN	0xffef
#define A2MD_STDKEY_ENCR_DOWN2UP	0xffee
#define A2MD_STDKEY_ENCR_UP2MID		0xffed
#define A2MD_STDKEY_ENCR_MID2UP		0xffec
#define A2MD_STDKEY_ENCR_MID2DOWN	0xffeb
#define A2MD_STDKEY_ENCR_DOWN2MID	0xffea
#define A2MD_STDKEY_ENCR_UP2UP		0xffe9
#define A2MD_STDKEY_ENCR_DN2DN		0xffe8
#define A2MD_STDKEY_ENCR_MID2MID	0xffe7
#define A2MD_STDKEY_ENCR		0xffe6



/* Listing of values for the A2MD styles */
#define A2MD_STYLE_LIBSSL_SHA256 1
#define A2MD_STYLE_SODIUM_BLAKE2B 2


#ifndef A2MD_STYLE_DEFAULT
//BOTHERSOME// #define A2MD_STYLE_DEFAULT A2MD_STYLE_SODIUM_BLAKE2B
#define A2MD_STYLE_DEFAULT A2MD_STYLE_LIBSSL_SHA256
#endif

#ifndef A2MD_STYLE
#define A2MD_STYLE A2MD_STYLE_DEFAULT
#endif



/***** LIBSSL WITH SHA256 SECURE HASH *****/


#if A2MD_STYLE == A2MD_STYLE_LIBSSL_SHA256

#define A2MD_ALGORITHM "sha256"

/* Minimal include files for generic hashing, fallback to OpenSSL's SHA256 */
#ifndef _ARPA2_UTIL_SHA256
#include <openssl/sha.h>
#endif

/* Wrap the context required for OpenSSL's SHA-256 */
struct a2md_state {
	SHA256_CTX ctx;
};

/* The buffer size in OpenSSL SHA256 */
#define A2MD_STATE_SIZE (sizeof (a2md_state))

/* The internal size of the hash routine; this may match the implementation buffer size */
#define A2MD_BLOCK_SIZE 64

/* Sodium allows us to choose the output size ourselves -- we want a static */
#define A2MD_OUTPUT_SIZE SHA256_DIGEST_LENGTH

static inline bool a2md_open (struct a2md_state *state) {
	SHA256_Init (&state->ctx);
	return true;
}

static inline bool a2md_write (struct a2md_state *state, const void *dataptr, int datalen) {
	bool ok = true;
	ok = ok && (datalen >= 0);
	if (ok) {
		SHA256_Update (&state->ctx, dataptr, datalen);
	}
	return ok;
}

static inline bool a2md_close (struct a2md_state *state, uint8_t *opt_output) {
	bool ok = true;
	if (opt_output != NULL) {
		SHA256_Final (opt_output, &state->ctx);
	}
	memset (&state->ctx, 0, sizeof (state->ctx));
	return ok;
}


/***** LIBSODIUM WITH BLAKE2B AS GENERIC HASH *****/


#elif A2MD_STYLE == A2MD_STYLE_SODIUM_BLAKE2B


#define A2MD_ALGORITHM "blake2"

/* Minimal include files for generic hashing with Sodium's Blake2b */
#include <sodium/core.h>
#include <sodium/utils.h>
#include <sodium/crypto_generichash_blake2b.h>

/* Sodium uses functions for dynamic state sizing -- we want a static */
#define A2MD_STATE_SIZE (sizeof (crypto_generichash_blake2b_state))

/* Sodium allows us to choose the output size ourselves -- we want a static */
#define A2MD_OUTPUT_SIZE 32

/* Sodium wraps this into dynamic sizing logic -- we want a static */
struct a2md_state {
	crypto_generichash_blake2b_state *raw;
};

static inline bool a2md_open (struct a2md_state *state) {
	/* return from sodium? */
	state->raw = sodium_malloc (A2MD_STATE_SIZE);
	log_detail ("Sodium mallocated %p", state->raw);
	if (state->raw == NULL) {
		return false;
	}
	crypto_generichash_blake2b_init (state->raw, NULL, 0, A2MD_STATE_SIZE);
	return true;
}

static inline bool a2md_write (struct a2md_state *state, const void *dataptr, int datalen) {
	bool ok = true;
	ok = ok && (datalen >= 0);
	/* return from sodium? */
	crypto_generichash_blake2b_update (state->raw, dataptr, datalen);
	return ok;
}

static inline bool a2md_close (struct a2md_state *state, uint8_t *opt_output) {
	bool ok = true;
	if (opt_output != NULL) {
		/* return from sodium? */
		crypto_generichash_blake2b_final (state->raw, opt_output, A2MD_OUTPUT_SIZE);
	}
	sodium_memzero (state->raw, A2MD_STATE_SIZE);
	sodium_free (state->raw);
	state->raw = NULL;
	return ok;
}


#else /* A2MD_STYLE */



/***** NO MESSAGE DIGEST STYLE SELECTED *****/



#error "Invalid value for A2MD_STYLE or A2MD_STYLE_DEFAULT"



#endif



/***** GENERIC DEFINITIONS *****/


typedef struct a2md_state a2md_state;


static bool a2md_clone (struct a2md_state *target, const struct a2md_state *source) {
	memcpy (target, source, sizeof (struct a2md_state));
	return true;
}

static inline bool a2md_write_max (struct a2md_state *state, const void *dataptr, int datalen, int datalen_max) {
	bool ok = true;
	ok = ok && (datalen <= datalen_max);
	ok = ok && a2md_write (state, dataptr, datalen);
	return ok;
}

static inline bool a2md_write_tag (struct a2md_state *state, uint16_t tag) {
	uint8_t tag16  [2];
	tag16 [0] = ( tag >> 8) & 0xff;
	tag16 [1] = ( tag     ) & 0xff;
	return a2md_write (state, &tag16, 2);
}

static inline bool a2md_write_msg (struct a2md_state *state, const void *dataptr, int datalen) {
	bool ok = true;
	ok = ok && a2md_write_tag (state, datalen);
	ok = ok && a2md_write_max (state, dataptr, datalen, 65535);
	return ok;
}

static inline bool a2md_printf (struct a2md_state *state, const char *format, ...) {
	bool ok = true;
	va_list ap0, ap1;
	va_start (ap0, format);
	va_copy (ap1, ap0);
	int txtlen = vsnprintf (NULL, 0, format, ap0);
	ok = ok && (txtlen >= 0);
	va_end (ap0);
	char txtbuf [ ok ? txtlen + 3 : 10];
	va_start (ap1, format);
	ok = ok && (txtlen == vsnprintf (txtbuf, txtlen + 2, format, ap1));
	va_end (ap1);
	ok = ok && a2md_write (state, txtbuf, txtlen);
	return ok;
}

static inline bool a2md_read (const struct a2md_state *state, uint8_t *output, uint16_t output_size) {
	uint16_t blkN = output_size / A2MD_OUTPUT_SIZE;
	if (blkN * A2MD_OUTPUT_SIZE < output_size) {
		blkN++;
	}
	bool ok = true;
	for (uint16_t blk = 0; blk <= blkN; blk++) {
		struct a2md_state cp;
		uint8_t buf [A2MD_OUTPUT_SIZE];
		memset (buf, 0, A2MD_OUTPUT_SIZE);
		ok = ok && a2md_clone (&cp, state);
		ok = ok && a2md_write_tag (&cp, blk);
		ok = ok && a2md_close (&cp, buf);
		uint16_t outlen = A2MD_OUTPUT_SIZE;
		if (outlen > output_size) {
			outlen = output_size;
		}
		memcpy (output, buf, outlen);
		output += outlen;
		output_size -= outlen;
	}
	ok = ok && (output_size == 0);
	return ok;
}

static inline bool a2md_read_key (const struct a2md_state *state, uint16_t key, uint8_t *output, uint16_t output_size) {
	uint16_t blkN = output_size / A2MD_OUTPUT_SIZE;
	if (blkN * A2MD_OUTPUT_SIZE < output_size) {
		blkN++;
	}
	bool ok = true;
	for (uint16_t blk = 0; blk <= blkN; blk++) {
		struct a2md_state cp;
		uint8_t buf [A2MD_OUTPUT_SIZE];
		memset (buf, 0, A2MD_OUTPUT_SIZE);
		ok = ok && a2md_clone (&cp, state);
		ok = ok && a2md_write_tag (&cp, key);
		ok = ok && a2md_write_tag (&cp, blk);
		ok = ok && a2md_close (&cp, buf);
		uint16_t outlen = A2MD_OUTPUT_SIZE;
		if (outlen > output_size) {
			outlen = output_size;
		}
		memcpy (output, buf, outlen);
		output += outlen;
		output_size -= outlen;
	}
	ok = ok && (output_size == 0);
	return ok;
}

/** @} */


/** @defgroup arpa2digest_hmac HMAC keyed hashing functions
 * @{
 *
 * These functions work on a different type, namely two instead of
 * one a2md_state, but the pointer to the first may be used to
 * add message bytes to the HMAC function.  The only place where
 * the double structure is used is in a2md_hmac_open() and in
 * a2md_hmac_close() functions before and after a2md_xxx() calls.
 */


typedef a2md_state a2md_hmac_state[2];


/** @brief Open a HMAC keyed digest by initialising pre-allocated state.
 *
 * HMAC \a state holds two consecutive a2md_state structures, the first of
 * which is the inner element.  This allows the other a2md_xxx() routines
 * to be used on the same \a state value.  Close with a2md_hmac_close().
 *
 * The keylen is measured in bytes.  It must not exceed A2MD_BLOCK_SIZE.
 *
 * This overwrites the current state, so it may be used
 * as initialisation and/or as a reset.
 */
static inline bool a2md_hmac_open (struct a2md_state *state, uint8_t *key, uint16_t keylen) {
	bool ok = true;
	//
	// Prepare the key material for the hashes
	if (keylen > A2MD_BLOCK_SIZE) {
		return false;
	}
	uint8_t keymat [A2MD_BLOCK_SIZE];
	memcpy (keymat, key, keylen);
	if (keylen < A2MD_BLOCK_SIZE) {
		memset (keymat + keylen, 0, A2MD_BLOCK_SIZE - keylen);
	}
	//
	// XOR 0x36 to apply the inner pad
	for (unsigned i=0; i<A2MD_BLOCK_SIZE; i++) {
		keymat[i] ^= 0x36;
	}
	ok = ok && a2md_open  (&state[0]);
	ok = ok && a2md_write (&state[0], keymat, A2MD_BLOCK_SIZE);
	//
	// XOR 0x5c and remove XOR 0x36 to apply the outer pad
	for (unsigned i=0; i<A2MD_BLOCK_SIZE; i++) {
		keymat[i] ^= 0x36 ^ 0x5c;
	}
	ok = ok && a2md_open  (&state[1]);
	ok = ok && a2md_write (&state[1], keymat, A2MD_BLOCK_SIZE);
	//
	// Clear the key material
	memset (keymat, 0, A2MD_BLOCK_SIZE);
	//
	// Return success/failure
	return ok;
}

/** @brief Close a HMAC keyed digest by wiping its state.
 *
 * HMAC \a state holds two consecutive a2md_state structure, the first of
 * which is the inner element.  This allows the other a2md_xxx() routines
 * to be used on the same \a state value.  Opened with a2md_hmac_open().
 *
 * When opt_output is not NULL, first write the final digest state
 * to it.  The size of this output is A2MD_OUTPUT_SIZE.
 */
static inline bool a2md_hmac_close (struct a2md_state *state, uint8_t *opt_output) {
	bool ok = true;
	uint8_t intermediate [A2MD_OUTPUT_SIZE];
	ok = ok && a2md_close (&state[0], intermediate);
	ok = ok && a2md_write (&state[1], intermediate, A2MD_OUTPUT_SIZE);
	ok = ok && a2md_close (&state[1], opt_output);
	return ok;
}


/** @} */


/** @defgroup arpa2digest_hmac HOTP, TOTP and OCRA functions
 * @{
 *
 * These functions implement the standardised digits sequences, but
 * centered around the A2MD_ digest algorithm.  Note that the HOTP
 * function does not formally allow this, whereas TOTP and OCRA do
 * have such flexibility.
 */


/** @brief Compute HOTP(K,C) = Truncate(HMAC-H(K,C))
 *
 * Based on the hash function \a H(), compute the HMAC bassed on the
 * HOTP key \k K and the 64-bit counter value \a C, as standardised
 * in RFC 4226.  If \a H() is not \a SHA1(), then rely on the first
 * 160 bits of the result: scatter position based on byte 19 and read
 * the 4 bytes from that offset, clear bit 31 and compute module the
 * desired power of 10 (usually 10^6 or 10^8).
 *
 * We return a 31-bit unsigned integer, all it takes next is a modulo
 * operation with 10^6 or 10^8 or anything other, as desired, to find
 * the HOTP output digits.  The return value is -1 on error.
 */
static inline int32_t a2md_hotp (uint8_t *key, uint16_t keylen, uint64_t ctr) {
	bool ok = true;
	//
	// Compute the HMAC-H() with the default am2d_xxx() digest  as H()
	uint8_t hmacout [A2MD_OUTPUT_SIZE];
	a2md_hmac_state state;
	ok = ok && a2md_hmac_open (state, key, keylen);
	uint64_t ctrbytes = htobe64 (ctr);
	ok = ok && a2md_write (state, &ctrbytes, 8);
	ok = ok && a2md_hmac_close (state, hmacout);
	//
	// Retrieve the 32-bit value from the deesired offset
	int pos = hmacout[A2MD_OUTPUT_SIZE-1] & 0x0f;
	uint32_t sample = * (uint32_t*) &hmacout[pos];
	sample = be32toh (sample);
	sample &= 0x7fffffff;
	//
	// Return the 31-bit unsigned integer from which digits are drawn,
	// or return -1 on error
	return ok ? (int32_t) sample : -1;
}

#define a2md_hotp6(key,keylen,ctr) (a2md_hotp((key),(keylen),(ctr)) %   1000000)
#define a2md_hotp7(key,keylen,ctr) (a2md_hotp((key),(keylen),(ctr)) %  10000000)
#define a2md_hotp8(key,keylen,ctr) (a2md_hotp((key),(keylen),(ctr)) % 100000000)


/** @brief Compute TOTP(K,C) = Truncate(HMAC-H(K,floor((T-T0)/P)))
 *
 * The TOTP specification in RFC 6238 is explicit about permission to
 * use \a SHA256() or \a SHA512() instead of \a SHA1().  In that sense
 * it is more modern.  The counter value \a C is specifically defined
 * as the current time (beyond a start point, by default the epoch)
 * divided by a period (that defaults to 30s).
 *
 * Return a non-negative 31-bit integer on success, or -1 on error.
 */
static inline int32_t a2md_totp (uint8_t *key, uint16_t keylen, time_t t0, uint16_t seconds) {
	bool ok = true;
	time_t now = (time_t) -1;
	ok = ok && (time (&now) != (time_t) -1);
	uint64_t ctr = (now-t0) / seconds;
	return a2md_hotp (key, keylen, ctr);
}

#define a2md_totp_default(key,keylen) (a2md_totp((key),(keylen),0,30))

/** Careful about a2md_totp6(), a2md_totp7() and a2md_totp8() shorthands!
 * They squander the error outcome -1.
 */
#define a2md_totp6(key,keylen,t0,seconds) ((a2md_totp((key),(keylen),(t0),(seconds))) %   1000000)
#define a2md_totp7(key,keylen,t0,seconds) ((a2md_totp((key),(keylen),(t0),(seconds))) %  10000000)
#define a2md_totp8(key,keylen,t0,seconds) ((a2md_totp((key),(keylen),(t0),(seconds))) % 100000000)

/** Careful about a2md_totp6_default(), a2md_totp7_default() and
 * a2md_totp8_default() shorthands!  They squander the error outcome -1.
 */
#define a2md_totp6_default(key,keylen) ((a2md_totp((key),(keylen),0,30) %   1000000))
#define a2md_totp7_default(key,keylen) ((a2md_totp((key),(keylen),0,30) %  10000000))
#define a2md_totp8_default(key,keylen) ((a2md_totp((key),(keylen),0,30) % 100000000))


/** @} */


#ifdef __cplusplus
}
#endif

#endif /* ARPA2_DIGEST_H */

/** @} */
