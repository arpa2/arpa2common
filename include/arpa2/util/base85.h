/** @addtogroup arpa2util ARPA2 Utility Collection
 * @{
 * @defgroup base85 Encode 32-bit words into 5 ASCII chars using ARPA2 BASE85 encoding.
 * @{
 * BASE85 gives nice chunking for binary data of fixed sizes, such as keys and hashes:
 * 85^5 = 4437053125 > 4294967296 = 2^32
 * so we can map uint32_t values from and to 5-character arrays.  The logic is
 * expanded with 4 byte integers in network byte order and generic byte sequences.
 *
 * This uses another ASCII encoding than RFC1924, as that was tailored for IPv6.
 * Instead, concerns about quoting and escapes in shells and URLS are addressed.
 */

/* SPDX-FileCopyrightText: Copyright 2024 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_UTIL_BASE85_H
#define ARPA2_UTIL_BASE85_H


#include <stdbool.h>
#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif


/** @brief The zero character in BASE85 representation.
 */
#define base85_zero '#'


/** @brief The highest character code in BASE85 representations.
 *
 * Note that BASE85 could encode values in excess of a 32-bit range,
 * so a sequence of these values would actually cause an overflow.
 * To encode all-ones words, please use an encoding operation.
 */
#define base85_last '~'


/** @brief The sequence of ARPA2 BASE85 characters in their code point order 0..84.
 *
 * The characters dropped from printable ASCII (0x33..0x7e) are 3 quotes,
 * 1 backslash, 1 percent, 1 dollar, 1 ampersand, 1 exclamation mark, 1 bar.
 * This results in 94-3-1-1-1-1-1-1 = 85 codes.  They are used to represent
 * code points 0..84 in the same ordering as used for ASCII.
 *
 * The character set contains:
 *
 * "#", "(", ")", "*", "+", ",", "-", ".", "/", "0".."9", ":", ";", "<", "=",
 * ">", "?", "@", "A".."Z", "[", "]", "^", "_", "a".."z", "{", "}", "~".
 *
 * This variable is terminated with ASCII NUL, so it can be used as a string.
 * The characters are in-order, so their offset into the string coincides
 * with the 85 values they represent.  See base65_values for faster mapping.
 *
 * The code contains so many code points that it cannot be represented fully
 * on a shell; it must always be quoted, but avoids the harshest problems.
 * The symbols removed have therefore been selected for quoting or escaping
 * in shell or URL possible without running into the symbols used for that.
 *
 * The same applies to URLs, where old implementations and some characters
 * not being considered in RFC 3986 make it impossible because there is no
 * explicit permission for 1 caret, 2 square brackets, 2 curly braces, not
 * even in the (most liberal) format that follows "?" and precedes "#".
 * These 6 symbols (also counting 1 pound sign) could be mapped to the few
 * extra characters usable in that part: "?", "!", "$", "'", "=" and "&",
 * but "?" is not always parsed correctly and "&" is usually reserved to
 * separate query parameters and may therefore also be parsed incorrectly.
 * This is not a recipe for stable transport of BASE85; always use percent
 * encoding for URL data and benefit from the absense of space and percent.
 */
extern const char base85_characters[86];


/** @brief The sequence of ARPA2 BASE85 decoding per byte value, yielding a code point or -1.
 */
extern const int8_t base85_values[256];


/** @brief Test whether the character is a valid base85 character.
 */
#define isbase85(c) (base85_values[(uint8_t)(c)] >= 0)


/** @brief Get the length of a base85 prefix substring.
 *
 * Note that the return value may not be a valid BASE85 length;
 * test with base85_reprlen_ok() to learn this, or use
 * base85_strlen() to receive -1 for a wrongful BASE85 prefix.
 */
unsigned base85_strspn (const char *s);


/** @brief Get the length of a non-base85 prefix substring.
 */
unsigned base85_strcspn (const char *s);


/** @brief Get the length of a bsae85 prefix substring, or -1 on error.
 *
 * This function works like base85_strspn(), except that it will only
 * report valid BASE85 string lengths, and -1 otherwise.  See
 * base85_reprlen_ok() for details.
 */
int base85_strlen (const char *s);


/** @brief Compute the size of a BASE85 string to represent binary bytes.
 *
 * Formally multiply by 1.25 and round up.  This would be exact in floating
 * point calculations (an advantage over BASE64) but the range would depend
 * on the variant used, so we choose to use integer math.
 */
#define base85_reprlen(n) ((n)+(((n)+3)/4))


/** @brief Test if the size of a BASE85 string is valid.
 *
 * Due to the rounding up, the size jumps up by 2 instead of one as soon
 * as its length % 5 == 1.
 */
#define base85_reprlen_ok(n) (((n)%5)!=1)


/** @brief Compute the size of the bytes from a BASE85 string.
 *
 * We shall assume that the length was tested.
 */
#define base85_reprlen_bytes(n) (((n)*4)/5)


/** @brief Encode an unsigned 32-bit integer into 5 characters in BASE85.
 *
 * This represents the input in 5 characters.  It does not add a NUL
 * character to terminate it as a string, so this can be used on a
 * sequence of 32-bit chunks.  If network byte order is used, consider
 * using base85_encode_net32() or base85_encode_bytes() instead.
 * 
 * @returns Nothing, this function cannot fail.
 */
void base85_encode (uint32_t in, char out[5]);


/** @brief Decode 5 characters in BASE85 into a 32-bit unsigned integer.
 *
 * This looks up each of the characters and maps it to a 32-bit integer.
 * The output can only be relied upon when the function returns success.
 * If network byte order is used, consider using base85_decode_net32()
 * or base85_decode_bytes() instead.
 *
 * @returns true only if the characters are all valid BASE85 characters.
 */
bool base85_decode (const char in[5], uint32_t* out);


/** @brief Encode 32 bits at a pointer in network byte order into 5 characters in BASE85.
 *
 * @returns Nothing, this function cannot fail.
 */
void base85_encode_net32 (const uint8_t in[4], char out[5]);


/** @brief Decode 5 characters in BASE85 into 32 bits in network byte order.
 *
 * @returns true only if the characters are all valid BASE85 characters.
 */
bool base85_decode_net32 (const char in[5], uint8_t out[4]);


/** @brief Encode a sequence of bytes of given length into a NUL-terminated output string.
 *
 * This function takes in a sequence \a in of any length \a inlen, which
 * need not be a multiple of 4.  The string is terminated with a NUL
 * character.
 *
 * @returns Nothing, this function cannot fail.
 */
void base85_encode_bytes (const uint8_t* in, unsigned inlen, char* out);


/** @brief Decode a character sequence of given length to output bytes.
 *
 * This function takes in a string \a in of any length \a inlen but only
 * succeeds decoding it when base85_reprlen_ok() is true.  This makes it
 * a general parsing function; the \a inlen specifically does not have
 * to be a multiple of 5, but unusable \a inlen values do exist.
 *
 * Proper \a inlen values can come from strlen() or base85_strspn(), but
 * since base85_strlen() is signed it must not be passed in directly.
 *
 * @returns true only if the characters are all valid BASE85 characters
 *	and if the \a inlen is a valid BASE85 string length.
 */
bool base85_decode_bytes (const char* in, unsigned inlen, uint8_t* out);



#ifdef __cplusplus
}
#endif

#endif /* ARPA2_UTIL_BASE85_H */

/** @} */

/** @} */
