/** @ingroup arpa2access
 * @{
 * @defgroup arpa2access_actor_h Access Control for Acting-on-behalf-of
 * @{
 * Our blog discusses much background in the
 * [Access Control series](http://internetwide.org/tag/access.html)
 * and in the
 * [Identity series](http://internetwide.org/tag/identity.html).
 *
 * Actors are Identities that a user may assume instead of their
 * current identity.  For example, after login as john@example.com,
 * it may be useful to act on behalf of sales@example.com.  Whether
 * this is permitted is posed as a question to the Actor API.
 */

/* SPDX-FileCopyrightText: Copyright 2024 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_ACCESS_ACTOR_H
#define ARPA2_ACCESS_ACTOR_H


#include <stdbool.h>
#include <stdint.h>

#include "arpa2/identity.h"
#include "arpa2/rules.h"
#include "arpa2/access.h"


#ifdef __cplusplus
extern "C" {
#endif


/** @defgroup arpa2access_actor_types Actor Access Types
 * @{
 * Actors are a few kinds of things; aliases, pseudonyms
 * and group members are alternative identities that a user
 * may switch to.
 *
 * Aliases are so simple that they are usually coded in-place.
 * It is also incorporated into \ref access_actor(), both for
 * aliases to the current identity and after switching to a
 * pseudonym (but not after switching to a group).  A separate
 * \ref access_actor_pseudonym() is available for evaluation of
 * the right of administrative access to the Pseudonym.
 *
 * Pseudonyms are not really difficult either, and they are
 * only really part of the \ref access_actor() logic and the
 * a2rule(1) program.  Their \a rules_type is defined here.
 *
 * Groups are rather complicated, also because they allow
 * iteration with filters, and membership tests, so they
 * have a life of their own, but are also integrated into
 * \a access_actor().
 */


/** @brief The Rules Type for pseudonyms.
 *
 * This is the registered UUID value
 * for use with pseudonyms.  See http://uuid.arpa2.org/ for details.
 *
 * Rights are `ACCESS_OPERATE` or `%T` and `ACCESS_ADMIN` or `%A`.
 */
extern rules_type rules_type_pseudonym;


/** @} */



/** @defgroup arpa2access_actor_h_ops Actor Access Operations
 * @{
 * Actor Access determines whether a user with a known
 * identity may take on another identity, a so-called
 * Actor Identity.  This may be used to substitute a
 * login identity for one that has another acl profile,
 * for instance because it is part of a group.
 *
 * Actor Access has no Access Type of its own; it relies on
 * the service keys for groups and pseudonyms (and on their
 * usual defaults).
 */



/** @brief Generic check if a user may act from another identity.
 *
 * Functions with simpler signatures for simpler use cases are
 * available as \ref access_actor() and \ref access_actor_svckey().
 * It is ILL-ADVISED to use the explicit rulesets in this one.
 *
 * Users have authenticated to obtain an (initial) identity,
 * and may optionally step to other identities from there.
 * During this process, they are assigned a new identity,
 * sometimes referred to as their Actor Identity.  It will
 * replace the original identity, and be further considered
 * as if it was the original identity.  It may for instance
 * be used for logging, for additional access_actor() calls
 * but also in other Access Control calls.
 *
 * It will often suffice to fill only the first two parameters
 * and fill the rest with NULL and 0 values, and to check the
 * return value for approval.
 *
 * @param[in] current_id is the identity that has been validated
 * through authentication and possible preceding calls to
 * \a access_actor().
 *
 * @param[in] requested_id is the identity that the user is
 * asking to use in subsequent actions.  When approved, this will
 * take the place of \a current_id.
 *
 * @param[in] opt_svckey_group may be NULL or a Service Key for
 * group database access.  When set to NULL, the default is used.
 *
 * @param[in] svckeygrouplen specifies the length in bytes of the
 * \a opt_svckey_group.  It is only meaningful when that is not NULL.
 *
 * @param[in] opt_grouprules may be NULL or a concatenation of
 * zero-terminated strings with rules for the group.  When not NULL,
 * it is an alternative to the \a opt_svckey parameter.
 *
 * @param[in] groupruleslen is the total length of \a opt_grouprules
 * in bytes, including the very last zero terminator.
 *
 * @param[in] opt_svckey_pseudo may be NULL or a Service Key for
 * pseudonym database access.  When set to NULL, the default is used.
 *
 * @param[in] svckeypseudolen specifies the length in bytes of the
 * \a opt_svckey_pseudo.  It is only meaningful when that is not NULL.
 *
 * @param[in] opt_pseudorules may be NULL or a concatenation of
 * zero-terminated strings with rules for the group.  When not NULL,
 * it is an alternative to the \a opt_svckey_pseudo parameter.
 *
 * @param[in] pseudoruleslen is the total length of \a opt_pseudorules,
 * strings, in bytes, including the very last zero terminator.
 *
 * @returns When the \a requested_id may replace the \a current_id
 *	then true is returned, otherwise false.
 */
bool access_actor_general (const a2id_t *current_id, const a2id_t *requested_id,
                const uint8_t *opt_svckey_group, unsigned svckeygrouplen,
                const char *opt_grouprules, unsigned groupruleslen,
                const uint8_t *opt_svckey_pseudo, unsigned svckeypseudolen,
                const char *opt_pseudorules, unsigned pseudoruleslen);


/** @brief Straightforward check if a user may act from another identity.
 *
 * This is a variant of \ref access_actor_general() intended
 * for the use of an unencrypted Rules DB.  This is a nice
 * initial method, but should normally evolve into a more
 * fully supportive version based on \ref access_actor_svckey().
 *
 * @param[in] current_id is the identity that has been validated
 * through authentication and possible preceding calls to
 * \a access_actor().
 *
 * @param[in] requested_id is the identity that the user is
 * asking to use in subsequent actions.  When approved, this will
 * take the place of \a current_id.
 *
 * @returns When the \a requested_id may replace the \a current_id
 *	then true is returned, otherwise false.
 */
static inline bool access_actor (const a2id_t *current_id, const a2id_t *requested_id) {
	return access_actor_general (
			current_id, requested_id,
			NULL, 0,
			NULL, 0,
			NULL, 0,
			NULL, 0);
}


/** @brief Service-Key-based check if a user may act from another identity.
 *
 * This is a variant of \ref access_actor_general() intended
 * for the use of an encrypted Rules DB.  This is the step up
 * from \ref access_actor() that allows it to work with more
 * secure environments, such as plugin services hosted away from
 * the identity provider, and supplied only with information on a
 * need-to-know basis.
 *
 * @param[in] current_id is the identity that has been validated
 * through authentication and possible preceding calls to
 * \a access_actor().
 *
 * @param[in] requested_id is the identity that the user is
 * asking to use in subsequent actions.  When approved, this will
 * take the place of \a current_id.
 *
 * @param[in] svckey_group is a Service Key for group database access.
 *
 * @param[in] svckeygrouplen specifies the length in bytes of the
 * \a svckey_group.
 *
 * @param[in] svckey_pseudo is a Service Key for pseudonym database
 * access.
 *
 * @param[in] svckeypseudolen specifies the length in bytes of the
 * \a svckey_pseudo.
 *
 * @returns When the \a requested_id may replace the \a current_id
 *	then true is returned, otherwise false.
 */
static inline bool access_actor_svckey (const a2id_t *current_id, const a2id_t *requested_id,
                const uint8_t *svckey_group, unsigned svckeygrouplen,
                const uint8_t *svckey_pseudo, unsigned svckeypseudolen) {
	return access_actor_general (
			current_id, requested_id,
			svckey_group, svckeygrouplen,
			NULL, 0,
			svckey_pseudo, svckeypseudolen,
			NULL, 0);
}


/** @brief Rules-based check if a user may act from another identity.
 *
 * This is an ILL-ADVISED variant of \ref access_actor_general() intended
 * for the use of rules outside of a database.  The reason that this
 * variant is not advised is that it avoids sytem-wide integrated use
 * of groups and pseudonyms, as designed for the InternetWide Architecture.
 *
 * Advised variants are \ref access_actor() for simple uses, and
 * \ref access_actor_svckey().
 *
 * If you need to mix Service Key with supplied Rules, please use
 * \ref access_actor_general() instead.  This is also ill-advised.
 *
 * @param[in] current_id is the identity that has been validated
 * through authentication and possible preceding calls to
 * \a access_actor().
 *
 * @param[in] requested_id is the identity that the user is
 * asking to use in subsequent actions.  When approved, this will
 * take the place of \a current_id.
 *
 * @param[in] grouprules is a concatenation of zero-terminated strings
 * with rules for the group.
 *
 * @param[in] groupruleslen is the total length of \a opt_grouprules
 * in bytes, including the very last zero terminator.
 *
 * @param[in] pseudorules is a concatenation of zero-terminated strings
 * with rules for the group.
 *
 * @param[in] pseudoruleslen is the total length of \a opt_pseudorules,
 * strings, in bytes, including the very last zero terminator.
 *
 * @returns When the \a requested_id may replace the \a current_id
 *	then true is returned, otherwise false.
 */
static inline bool access_actor_ruleset (const a2id_t *current_id, const a2id_t *requested_id,
                const char *grouprules, unsigned groupruleslen,
                const char *pseudorules, unsigned pseudoruleslen) {
	return access_actor_general (
			current_id, requested_id,
			NULL, 0,
			grouprules, groupruleslen,
			NULL, 0,
			pseudorules, pseudoruleslen);
}


/** @brief Test the rights for a switch from current_id to a pseudonym.
 *
 * This function evaluates a ruleset for Pseudonym Actor Access, and
 * returns the Acess Rights found.  Usually this consists of ACCESS_OPERATE
 * or `%T` and possible ACCESS_ADMIN or `%A`, where the former allows the
 * switch to the pseudonym and the latter grants changes to who may use
 * the pseudonym.  Very often, both flags will be assigned together.
 *
 * @param[in] current_id is the identity that has been validated
 * through authentication and possible preceding calls to
 * \a access_actor().  It may be foreign and is therefore assumed
 * to have been parsed with \ref a2id_parse_remote() rather than
 * \ref a2id_parse().  If it is a local identity, it should not be
 * a service identity (starting with a `+` character).  It is not
 * common to include later `+` characters in the user name, but it
 * may be used if so desired.
 *
 * @param[in] requested_id is the pseudonym that the user is trying
 * to use in subsequent actions.  This may or may not have aliases, but
 * any database lookups would not involve user name aliases.  This is a
 * local name and is assumed to have been parsed with \ref a2id_parse().
 *
 * @param[in] opt_svckey_pseudo may be NULL or a Service Key for
 * pseudonym database access.  When set to NULL, the default is used.
 *
 * @param[in] svckeypseudolen specifies the length in bytes of the
 * \a opt_svckey_pseudo.  It is only meaningful when that is not NULL.
 *
 * @param[in] opt_pseudorules may be NULL or a concatenation of
 * zero-terminated strings with rules for the group.  When not NULL,
 * it is an alternative to the \a opt_svckey_pseudo parameter.
 *
 * @param[in] pseudoruleslen is the total length of \a opt_pseudorules,
 * strings, in bytes, including the very last zero terminator.
 *
 * @param[out] out_rights is set to the Access Rights according to the
 * policy for the pseudonym in the \a requested_id.  It is meaningful
 * even when the return value is false, which case it only holds
 * ACCESS_VISITOR or %V.  When a pseudonym was found, then any
 * Access Rules that made it to the end of any rule in the ruleset
 * are combined with that basic result.  Usually, this means that
 * ACCESS_OEPRATE or %T is added to grant the use of the pseudonym,
 * and/or that ACCESS_ADMIN or %A is added to grant changes to the
 * people who may change the \a current_id values permitted for the
 * pseudonym.
 *
 * @returns true only when the Policy Rule for the Pseudonym in the
 * \a requested_id was found and evaluated properly, regardless of
 * value, or false otherwise.
 */
bool access_actor_pseudonym (const a2id_t *current_id, const a2id_t *requested_id,
		const uint8_t *opt_svckey_pseudo, unsigned svckeypseudolen,
		const char *opt_pseudorules, unsigned pseudoruleslen,
		access_rights *out_rights);


/** @} */

#ifdef __cplusplus
}
#endif

#endif /* ARPA2_ACCESS_ACTOR_H */

/** @} */
/** @} */
