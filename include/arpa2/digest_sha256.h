/** @ingroup arpa2digest
 * @{
 * @defgroup arpa2digest_sha256 Internal SHA256 code
 * @{
 * This is a drop-in replacement for OpenSSL functions for SHA256.
 *
 * We have a fixed relation to this hash, which is used for key mapping
 * in our ARPA2 Rules DB.  OpenSSL is phasing out static access to the
 * functions and may well drop the algorithm in future versions, which
 * is a future problem for our libraries.
 *
 * By including this file (and not OpenSSL) the definitions for the
 * usual SHA256_Xxx() functions change to internal a2md_sha256_xxx()
 * calls which are implemented in ARPA2::util_sha256 and which are
 * available dependencies to the libraries that rely on it.
 */

/*
 * SPDX-FileCopyrightText: Copyright 2024 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * These API definitions are based from the OpenSSL include files.
 */


#ifdef OPENSSL_SHA_H
#warning "You included <openssl/sha.h> already, skipping <arpa2/util/sha256.h>"
#define _ARPA2_UTIL_SHA256
#endif


#ifndef _ARPA2_UTIL_SHA256
#define _ARPA2_UTIL_SHA256


#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif



/** @def SHA256_DIGEST_LENGTH
 *
 * @brief The digest length for SHA256 is 32.
 */
#define SHA256_DIGEST_LENGTH    32



/** @def SHA256_CTX
 *
 * @brief This is the context holding SHA256 state
 */
typedef struct SHA256state_st {
    unsigned int h[8];
    unsigned int Nl, Nh;
    unsigned int data[16];
    unsigned int num, md_len;
} SHA256_CTX; 



/** @def a2md_sha256_init
 *
 * @brief Initialise a secure hash.
 *
 * @return 1 for success, 0 for failure.
 */
int a2md_sha256_init (SHA256_CTX *c);


/** @def a2md_sha256_update
 *
 * @brief Update the hash value with new data bytes.
 *
 * @return 1 for success, 0 for failure.
 */
int a2md_sha256_update (SHA256_CTX *c, const void *data, size_t len);


/** @def a2md_sha256_final
 *
 * @brief Retrieve the result from a hashing operation.
 *
 * @return 1 for success, 0 for failure.
 */
int a2md_sha256_final (unsigned char *md, SHA256_CTX *c);



/** @def SHA256_Init
 *
 * @brief Initialise a secure hash.
 *
 * @return 1 for success, 0 for failure.
 */
static inline int SHA256_Init (SHA256_CTX *c) {
	return a2md_sha256_init (c);
}


/** @def SHA256_Update
 *
 * @brief Update the hash value with new data bytes.
 *
 * @return 1 for success, 0 for failure.
 */
static inline int SHA256_Update (SHA256_CTX *c, const void *data, size_t len) {
	return a2md_sha256_update (c, data, len);
}


/** @def SHA256_Final
 *
 * @brief Retrieve the result from a hashing operation.
 *
 * @return 1 for success, 0 for failure.
 */
static inline int SHA256_Final (unsigned char *md, SHA256_CTX *c) {
	return a2md_sha256_final (md, c);
}


#ifdef __cplusplus
}
#endif

#endif /* ARPA2_UTIL_SHA256 */

/** @} */
/** @} */
