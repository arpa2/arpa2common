/** @ingroup arpa2access
 * @{
 * @defgroup arpa2access_rules Policy Rules for ARPA2 Access Control, Groups, ...
 * @{
 * Rules are a generic layer underpinning ARPA2 code for
 * Rules Control, Groups and so on.  Rules specify how
 * to handle a certain user (found with iteration) for a
 * given Rules Domain for an Rules Type ("service" or
 * "application") and Rules Name ("identity" or "instance").
 *
 * Rules are NUL-terminated UTF-8 strings consisting of
 * space-separated words.  Each word starts with a single
 * character to select an abstract function:
 *
 *   - `=xvalue` stores `value` in variable `x` until the end of the Rule
 *   - `%FLAGS` sets flags and then calls rules_flags_upcall()
 *   - `^trigger` triggers rules_trigger_upcall() with the `trigger` string
 *   - `~sel` selects an identity to apply in rules_selector_upcall()
 *   - `#label` is a marker comment for batch processing
 *   - the end of each Rule also triggers rules_endrule_upcall()
 *   - the end of a Ruleset also triggers rules_endruleset_upcall()
 *
 * Rules are concatenated to form Rulesets, and passed as
 * a `char *` to the start and an `unsigned` total length.
 * The terminating NUL for each Rule allows a reset of
 * state between Rules, notably of the `=xvalue` and `%FLAGS`
 * that are retained between the words of a Rule.
 *
 * There are 26 flags, named `A` through `Z` and passed in
 * an integer as bits 0 through 25, respectively.
 *
 * There are 26 variables, named `a` through `z` and passed
 * in an array at indexes 0 through 25, respectively.
 *
 * The form `~sel` may occur in localised Rulesets (near a
 * policy-ruled resource) but not in a database, because
 * then it is part of the database lookup key.
 *
 * The form `#label` is a one-word comment label, has no
 * meaning but may be used to select individual Rules from a
 * Ruleset during bulk traversal.  It can be used to mark a
 * Rule as having originated from a particular source.
 */

/* SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_RULES_H
#define ARPA2_RULES_H


#include <stdbool.h>
#include <stdint.h>

#include <arpa2/identity.h>


#ifdef __cplusplus
extern "C" {
#endif


/** @defgroup arpa2access_rules_h_types Rules Types
 * @{
 * Rules define a number of types.  Several of them
 * will also occur in higher layers, such as Access Control.
 * These higher layers are less abstract and generally add
 * a new type which either wraps or aliases these forms.
 */



/** @brief Rules Domains are UTF-8 representations
 * of a Fully Qualified Domain Name.
 *
 * These names have no initial or trailing dots, they
 * have at least one inner dot and they have been mapped
 * from the DNS wire format Punycode for international
 * names to UTF-8.  The wire form was a valid DNS name.
 *
 * Note the value of this mapping: Code should have only
 * one form to deal with, and Punycode is both specific
 * to one protocol and confusing.  It also cannot be
 * processed like any other internationalised code.  So
 * we choose an internal form that maps to a standard form.
 *
 * See A2ID_BUFSIZE for a discussion of the size that a domain
 * in this form can take.  This is normally supported as part
 * of the a2id_t and a2sel_t definitions in ARPA2 software.
 *
 * The NULL value is invalid as an Rules Domain.  This
 * means that software can use it to indicate absense of a
 * domain in places where it accepts optional parameters.
 *
 * This definition is the same as for access_domain.
 */
typedef char *rules_domain;


/** @brief Rules Types are 128-bit "well-known service
 * identities".
 *
 * We write an Rules Type in UUID form, but code that
 * prepares for taking it up use the canonical form of
 * 16 bytes of which the UUID shows the per-byte hex form
 * with a couple of dashes.  These 16 bytes are used in
 * message digests.
 * 
 * The Rules Type serves as a "well-known identifier"
 * for a class of uses.  For instance, we might have one
 * for the idea of "file storage" that could span many
 * application areas but be the same across vendors and
 * protocols and software suites.  Or we might have one
 * for "git access", regardless of the method being SSH,
 * HTTPS, local file system or other.
 *
 * Rules Types need further refinment to properly
 * localise them.  First of all, the rules_domain has to be
 * distinguished, so policy rights assigned in one domain
 * cannot pass over to another domain.  Furthermore, every
 * Rules Types defines a UTF-8 string format for an
 * rules_name that can distinguish instances within the
 * well-known class below the domain.  In the Git example,
 * the Rules Name could be a name for a repository.
 *
 * The all-zero value is not a valid rules_type.  This
 * means that zeroing an rules_type will not cause presumptious
 * use of a valid rules_type.
 *
 * This definition is the same as for access_type.
 */
typedef uint8_t rules_type [16];


/** @brief A reference to the 16-byte array of a rules_type.
 */
typedef uint8_t *rules_typeref;


/** @brief Rules Names represent instances of a Rules Type.
 *
 * Where the Rules Type is "well-known", it is instantiated by
 * an rules_domain and this Rules Name.  The general Rules Name
 * format is a UTF-8 string with a NUL terminator, but its format
 * is defined by the Rules Type.
 *
 * The purpose of the Rules Name is to allow generic software
 * to fill it, perhaps with user-supplied templates or regular
 * expressions taking content from an application's context.
 * This allows the generation of an Rules Name even in applications
 * that are not aware of the semantics of an Rules Type.  As an
 * example, a generic web server may take elements from the path
 * and/or query paramters and use those in a string templated that
 * is printed into an environment variable to be supplied to some
 * software for Rules Control.
 *
 * The value NULL is not a proper rules_name.  Depending on the
 * definition for the given rules_type, an empty string may be
 * a valid form.  Note that this means that NULL can be used to
 * indicate absense of an Rules Name in places where software
 * can optionally handle Rules Names.
 *
 * This definition is the same as for access_name.
 */
typedef char *rules_name;


/** @brief Rules Variables form a set of 26 variables named
 * `a` through `z` that never have value NULL but may be empty
 * when their length is 0.  They generally point back into a
 * Rule or to a constant string and have a pointer and length.
 * Do not assume a NUL terminator.
 *
 * Rules Variables are emptied at the start of a Rule, also
 * within a Ruleset.  Other than that, as long as they are
 * not set to a new value, the values are kept until the end
 * of a Rule, and is still available when its terminating NUL
 * causes an upcall.  The underlying memory disappears as soon
 * as the underlying memory is given up, which must be assumed
 * at the end of processing a Ruleset.
 *
 * The corresponding lenghts are in rules_varray_len.
 *
 * This definition is not also exported by Access Control.
 */
typedef char const *rules_varray_ptr [26];

/** @brief The corresponding lengths for rules_varray_ptr.
 */
typedef unsigned rules_varray_len [26];


/** @brief Map a lowercase letter \a c to a Rules Variable in
 * the variable array indexes.
 *
 * Only capital letters are mapped, and the caller should be
 * certain not to map anything else to a Rules Variable.
 *
 * This definition is not also exported by Access Control.
 */
#define RULES_VARINDEX(c) (c - 'a')


/** @brief Flags in a bit mask.
 *
 * Rules may set flags named `A` through `Z` in short `%FLAGS`
 * words.  Encode `A` in bit 0 through `Z` in bit 25 of a
 * 32-bit unsigned integer.
 *
 * This definition is the same as access_rights.
 */
typedef uint32_t rules_flags;


/** @brief Map an uppercase letter to a Rules Flag.
 *
 * Use bit 0 for 'A', bit 1 for 'B' and so on.  Only
 * capital letters are mapped, and the caller should be
 * certain not to map anything else to a Rules Flag.
 *
 * This definition is the same as ACCESS_RIGHT().
 */
#define RULES_FLAG(c) ( 1 << (c - 'A') )


/** @brief Triggers reference a ^trigger name.
 *
 * When a Rule contains a `^trigger` word, the rules_trigger
 * points after the '^' at the `trigger` itself.  Note that
 * it is a word, so it ends in either a space or in the NUL
 * character that terminates every Rule.  Upcalls provide
 * the length of the remaining `trigger` word.
 *
 * This definition is not also exported by Access Control.
 */
typedef char const *rules_trigger;


/** @} */


/** @defgroup arpa2access_rules_h_process Rules Processing
 * @{
 * Rules processing comes down to parsing a Ruleset and triggering
 * upcalls for them.  These upcalls process the %FLAGS, ^triggers
 * and ~selectors while taking =variables into account.  Special
 * upcalls for the end of a Rule and the end of a Ruleset can be
 * used to collate and make decisions on what should pass.
 *
 * Processing always passes over the entire Ruleset.  As soon as a
 * syntax error is found, processing breaks off and no further
 * callbacks are made.  The upcall at the end of the entire Ruleset
 * is made exactly when no processing error has occurred.
 *
 * The variables are references into the Ruleset, and once Ruleset
 * processing ends the underlying memory may have gone, so the last
 * opportunity for keeping the values found is in the upcall at the
 * end of the Ruleset.
 *
 * There are two kinds of Ruleset processing, reflecting two kinds
 * of input data.  One for a literal Ruleset found nearby a
 * Resource or stored in an LDAP attribute accessRule.  The other
 * is retrieved from the database by normal ARPA2 Iteration up from
 * an ARPA2 Identity.  In the latter case, the underlying database
 * is assumed to be the default database, which is opened for a
 * short-lived transaction without any need for the calling program
 * to mention the database.  The Service Key suffice to find the
 * desired Rules, after expansion for the identity and its more
 * abstract forms.
 *
 * Applications that build on top of Rules, such as Access Control
 * or Groups, will generally wrap around both forms as Ruleset
 * processing to support both resource-local specifications and
 * database-centralised and bulk-maintainable Rules.
 */


/** @brief Initialise the ARPA2 Rules system.
 *
 * This has a mirror image for cleaup in rules_fini().
 */
void rules_init (void);


/** @brief Finalise the ARPA2 Rules system.
 *
 * This may do cleanup operations.
 */
void rules_fini (void);


/* Forward reference.  */
struct rules_request;


/** @brief Rules upcall for flags; used when %FLAGS are encountered in a Rule.
 *
 * @return Always return true for future compatibility.
 */
typedef bool (*rules_flags_upcall) (
			struct rules_request *self);


/** @brief Rules upcall for triggers; used ^trigger is encountered in a Rule.
 *
 * @return Always return true for future compatibility.
 */
typedef bool (*rules_trigger_upcall) (
			struct rules_request *self,
			rules_trigger trigger_name,
			unsigned tigger_namelen);


/** @brief Rules upcall for selectors; used when ~sel is encountered in a Rule.
 *
 * @return Always return true for future compatibility.
 */
typedef bool (*rules_selector_upcall) (
			struct rules_request *self,
			const a2sel_t *selector);


/** @brief Rules upcall for the end of a Rule.
 *
 * The intention of this upcall is to support a choice between
 * the current Rule and offerings that may have been made before.
 * In other words, to collate and choose.
 *
 * After return from this upcall, the Rule Flags and Variables
 * will be emptied in the rules_request structure.
 *
 * This upcall is at the end of any Rule, even for the last.
 * If it is the last, then rules_endruleset_upcall will be
 * called immediately afterward.
 *
 * @return Always return true for future compatibility.
 */
typedef bool (*rules_endrule_upcall) (
			struct rules_request *self);


/** @brief Rules upcall for the end of a Ruleset.
 *
 * This upcall is made after a complete Ruleset has been
 * parsed successfully.  It is skipped for syntax errors.
 *
 * The intention of this upcall is to given a last opportunity
 * to process any data collected and decided about in the
 * foregoing upcalls.  The Rules Flags and Variables cannot be
 * assumed to present during this call.  The underlying memory
 * for the collected information is however still present, and
 * at the brink of being removed.  Use it or loose it, meaning
 * if you need retention beyond this point you should make a
 * copy in your own data structures.
 *
 * @return Always return true for future compatibility.
 */
typedef bool (*rules_endruleset_upcall) (
			struct rules_request *self);


/** @brief Request processing data.
 *
 * These values are used in rules_process() and rules_dbiterate()
 * as a data exchange and information supply to upcalls.
 * It informs of Rules Variables and Flags, but also shares context
 * data.
 *
 * @var opt_processor may be non-NULL to replace rules_process()
 * as the Ruleset processor function rules_dbiterate().  The
 * function itself may rely on rules_process() and merely wrap
 * around it.
 *
 * @var opt_domain may hold the Rules Domain from the context;
 * @var opt_type may hold the Rules Type from the context;
 * @var opt_name may hold the Rules Name from the context;
 * @var opt_qiter may hold a Quick Iterator from the context.
 *
 * @var optcb_flags may be an upcall for Rules Flags;
 * @var optcb_tigger may be an upcall for Rules Triggers;
 * @var optcb_selector may be an upcall for Selectors in Rules;
 * @var optcb_endrule may be an upcall for Rule ends;
 * @var optcb_endruleset may be an upcall for Ruleset ends.
 *
 * @var flags are set to 0 at the start of a Rule and to a new
 * value at the `%FLAGS` word in a Rule.  They hold the Rules
 * Flags while processing of a Ruleset.
 *
 * @var varray_str are set to an empty string at the start of
 * a Rule and to a new value at the `=xvalue` word in a Rule.
 * They hold the Rules Variables while processing a Ruleset.
 *
 * @var varray_strlen are set to 0 at the start of a Rule and
 * to the length `value` at the `=xvalue` word in a Rule.
 * They hold the Rules Variables while processing a Ruleset.
 */
struct rules_request {
	/* Override rules_process() for use in rules_dbiterate() */
	bool (*rules_process_override) (struct rules_request *req,
			const char *ruleset, unsigned rulesetlen,
			bool permit_selector);
	/* Context fields for the rules_process functions */
	rules_domain opt_domain;
	rules_typeref opt_type;
	rules_name opt_name;
	a2sel_quickiter opt_qiter;
	/* Callback functions, inasfar as supported by the Access Type */
	rules_flags_upcall      optcb_flags;
	rules_trigger_upcall    optcb_trigger;
	rules_selector_upcall   optcb_selector;
	rules_endrule_upcall    optcb_endrule;
	rules_endruleset_upcall optcb_endruleset;
	/* Output fields to upcalls (cleared at the end) */
	char const *flags_rawstr;
	rules_flags flags;
	rules_varray_ptr varray_str;
	rules_varray_len varray_strlen;
};


/** @brief Process a Ruleset, causing upcalls as specified
 * in the \a rules_request.  The \a ruleset is the ext to
 * process, with a NUL character ending individual Rules, and
 * with \a rulesetlen to mark the total length.
 *
 * When \a permit_selector is `true`, the form `~selector` is
 * permitted; an LDAP accessRule as well as localised settings
 * near a serviced resource would contain this form and set
 * \a permit_selector to `true`, while an LMDB rule would parse
 * to a known remote and forbid `~selector` words.
 *
 * It may happen that no upcalls are made.  There may be no
 * Ruleset or it may run into syntax errors.  Specifically
 * the upcall for the end of the Ruleset indicates that no
 * problems occurred.
 *
 * @returns The return value is `true` for succes, or `false`
 *          with `errno` set to a Common Error code on failure.
 */
bool rules_process (struct rules_request *req,
			const char *ruleset, unsigned rulesetlen,
			bool permit_selector);


/** @brief Iterate over an ARPA2 Selector for an identity to
 * find the most concrete Ruleset; once found, process it.
 *
 * Standard ARPA2 Iteration starts with \a iter0 and moves
 * from concrete to abstract.  The first (and most concrete)
 * record found is the only one being processed and it triggers
 * the upcalls that are specified in the \a req input.  When
 * no ruleset is found at all, the function returns false and
 * sets `errno` to Common Error code A2XS_ERR_MISSING_RULESET.
 *
 * The \a svckey with length \a svckeylength identifies the
 * Service Key, usually derived from a Rules Domain, Rules Type
 * and optional Database Secret with rules_dbkey_domain() and
 * rules_dbkey_service().
 *
 * It may happen that no upcalls are made.  There may be no
 * Ruleset or it may run into syntax errors.  Specifically
 * the upcall for the end of the Ruleset indicates that no
 * problems occurred.
 *
 * This function invokes ruleset_process() unless this is
 * overridden with non-NULL \a ruleset_process_override in
 * the \a req parameter.  During the call, \a optcb_selector
 * will be set to NULL and \a opt_qiter is the Quick Iterator
 * that was found in the database and \a opt_name will be set
 * to \a name.
 *
 * @returns The return value is `true` for succes, or `false`
 *          with `errno` set to a Common Error code on failure.
 */
bool rules_dbiterate (struct rules_request *req,
			const uint8_t *svckey, unsigned svckeylen,
			const rules_name name,
			const a2sel_t *iter0);


/** @} */

#ifdef __cplusplus
}
#endif

#endif /* ARPA2_RULES_H */

/** @} */
/** @} */
