/** @ingroup arpa2access
 * @{
 * @defgroup arpa2access_lets_h Access Control for Local Exchange Trading Systems
 * @{
 * Our blog discusses much background in the
 * [Access Control series](http://internetwide.org/tag/access.html)
 * and in the
 * [Identity series](http://internetwide.org/tag/identity.html).
 *
 * This API for LETS Access finds the rights of Remote Identity
 * to work on domain-local currencies or their balances.   This
 * is handled as a special form of Access Rules, using the
 * (currency and (balance identity and) domain for gradually
 * more refined access control.
 *
 * The three forms of the Access Names are:
 *   +lets@domain.name
 *   +lets+CUR@domain.name
 *   +lets+CUR+12345@domain.name
 *
 * There are currently no facilities for attributes and
 * triggers, but future extensions to this call could allow
 * such forms too.
 */

/* SPDX-FileCopyrightText: Copyright 2023 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_ACCESS_LETS
#define ARPA2_ACCESS_LETS


#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#include "arpa2/digest.h"

#include "arpa2/identity.h"
#include "arpa2/rules.h"
#include "arpa2/access.h"
#include "arpa2/access_document.h"


#ifdef __cplusplus
extern "C" {
#endif


/** @defgroup arpa2access_lets_h_types LETS Access Types
 * @{
 * LETS Access uses the standard Access Rights and
 * has a standard Access Type for all volumes and a
 * default volume for ARPA2 Reservoir.
 */


/** @brief Access Type for LETS, as registered on
 * http://uuid.arpa2.org
 */
extern access_type access_type_lets;


/** @} */


/** @defgroup arpa2access_lets_h_ops Document Access Operations
 * @{
 * Rules for Document Access currently have no support for
 * attributes or triggers; they merely inform about the %RIGHTS
 * for Remote Selectors, either from database traversal or
 * specified as ~selector in an explicit ACL Ruleset.
 */


/** @brief Process Document Access Rules
 *
 * @param[in] remote is the ARPA2 Identity for the remote
 * contact trying to access a document or folder.  This is
 * the Identity over which Iteration is done (in case of
 * database lookups) or which is matched against ~selector
 * (in explicit Rules).
 *
 * @param[in] xsname is the Access Name for the document or
 * folder being sought.  For ARPA2 Reservoir, the form is
 * "/<colluuid>/" with a possible continuation that will be
 * removed; for operator-defined volumes, the form is
 * "//<user>@<volume>/<path>" or "//<volume>/<path>" which
 * is not processed.  Both forms need to use proper grammar.
 * This value must not be NULL, but a minimum passable
 * string is "//".
 *
 * @param[in] opt_svckey may be NULL or otherwise provides
 * the Service Key.  NULL requests the default Service Key,
 * to be derived from the domain in \a local and without a
 * Database Secret.
 *
 * @param[in] svckeylen specifies the lenght of \a opt_svckey
 * but is only meaningful if that parameter is not NULL.
 *
 * @param[in] opt_acl may be NULL to perform Iteration on
 * the \a remote to search the database for an ACL Ruleset
 * or, if this parameter is not NULL, it will be used instead.
 *
 * @param[in] acllen specifies the length of \a opt_acl
 * but is only meaningful if that parameter is not NULL.
 *
 * @param[out] out_rights will hold the Access Rights, which
 * always contains `%V` in case of success, but no rights at
 * all in case of an error being returned.
 *
 * @param[out] optout_actor may be NULL to avoid Actors,
 * but will otherwise be filled with an Actor Identity
 * if a valid `=g<scene>+<actor>` attribute is related to
 * the accepting rule.  The test `!a2act_isempty()` can be
 * used to test that an Actor was supplied.  This may for
 * example be a group member, or a local name to be used while
 * processing the document.  Generally, the user address changes
 * from \a remote to \a optout_actor.
 *
 * @returns This function returns false with a com_err in
 * errno in case of technical problems, including errors in
 * the grammar of \a xsname.
 */
bool access_document (const a2id_t *remote, char *xsname,
                        const uint8_t *opt_svckey, unsigned svckeylen,
                        const char *opt_acl, unsigned acllen,
                        access_rights *out_rights,
			a2act_t *optout_actor);


/** @} */

#ifdef __cplusplus
}
#endif

#endif /* ARPA2_ACCESS_DOCUMENT */

/** @} */
/** @} */
