# Choose ARPA2 entropy implementation.
#
# Currently a stub implementation.
#
# Redistribution and use is allowed according to the terms of the two-clause BSD license.
#    SPDX-License-Identifier: BSD-2-Clause
#    SPDX-FileCopyrightText: Copyright 2020, Rick van Rein <rick@openfortress.nl>


macro (choose_a2entropy_impl)

	include (CheckSymbolExists)

	#
	# Choose an a2entropy_fast() implementation
	#

	check_symbol_exists ("getrandom" "sys/random.h" HAVE_GETRANDOM)
	check_symbol_exists ("BCryptGenRandom" "windows.h;bcrypt.h" HAVE_BCRYPTGENRANDOM)
	# check_...

	if (HAVE_GETRANDOM)
		add_compile_definitions(A2ENTROPY_FAST_IMPL=A2ENTROPY_FAST_IMPL_GETRANDOM)
	elseif (HAVE_BCRYPTGENRANDOM)
		add_compile_definitions(A2ENTROPY_FAST_IMPL=A2ENTROPY_FAST_IMPL_BCRYPTGENRANDOM)
	# elseif (HAVE_...)
	#	add_...
	else()
		message (FATAL_ERROR "Could not detect a suitable implementation for a2entropy_fast() in <arpa2/entropy.h>")
	endif()

	#
	# Choose an a2entropy_accounted() implementation
	#

	check_symbol_exists ("getentropy" "unistd.h" HAVE_GETENTROPY)
	# check_...

	if (HAVE_GETENTROPY)
		add_compile_definitions(A2ENTROPY_ACCOUNTED_IMPL=A2ENTROPY_ACCOUNTED_IMPL_GETENTROPY)
	# elseif (HAVE_...)
	#	add_...
	else()
		message (WARNING "Could not detect a suitable implementation for a2entropy_accounted() in <arpa2/entropy.h>")
		add_compile_definitions(A2ENTROPY_ACCOUNTED_IMPL=A2ENTROPY_ACCOUNTED_IMPL_FAIL)
	endif()

endmacro (choose_a2entropy_impl)

