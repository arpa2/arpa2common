.TH A2ID-KEYGEN 1 "June 2021" "ARPA2.net" "System Management Commands"
.SH NAME
a2id-keygen \- Generate a key for ARPA2 Signed Identities
.SH SYNOPSIS
.B a2id-keygen
.IR keyname
.PP
.B a2id-keygen
.IR /path/to/file.key
.SH DESCRIPTION
.PP
Using
.BR a2id-keygen ,
an administrator can install a key for use in the
.BR a2id_sign ()
and
.BR a2id_verify ()
functions.  These functions work on an ARPA2 Identity to
respective add or check a signature.  The signature content
shows up as part of the identity, like
.BR "" "john+cook" "+AABBCC+" "@example.com"
and the contents of this signature fixate the validity of
the ARPA2 Identity to contextual matter, such as the time
(allowing for expiration), the remote identity (fixating
their domain and/or local-part).
.PP
This command allows the system administrator to create a
new key.  The form with
.I /path/to/file.key
gives an absolute path name of a place to store the key
and the form
.I keyname
stores the file into a default location, which usually is
.B /var/lib/arpa2/identity
where keys are stored with a
.B .key
extesion.  Overwriting of an existing key file is not supported and
leads to an error message and failure exit.
.PP
The purpose of the
.B /var/lib/arpa2/identity
directory is to serve as a key repository for the system,
with files only accessible to the root user.
It is customary to have a key named
.B default
stored in
.B /var/lib/arpa2/identity/default.key
to serve most purposes.  Additional keys may be used, for
instance, to permit verification of signatures with older
keys, while the
.B default.key
is used for signing.  So, to cycle keys, move the default
key to another name with the
.BR .key
extension and generate a new default key to replace it.
.PP
Applications must load keys explicitly, and the default
key store is merely a convention to get that started.
The function
.BR a2id_addkey ()
is used to setup a new key.  Programs may open a file
descriptor to the key(s) to pass and harvest them after
dropping privileges.  An even stronger mechanism is to
do this key passing through a pipe, into which it fits
and from which it can only be read once by the program
at a lower privilege.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/arpa2common/
project page for ARPA2 Common.  We use its issue tracker
to track bugs and feature requests.
.SH COPYRIGHT
.PP
Copyright \(co 2021 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turn receives
funding from various sources.  This particular work was funded
through a grant of SIDNfonds, a public interest fund geared
towards a better Internet for all, and paid by administration
fees for .nl domain names.
.SH "SEE ALSO"
.IR a2id-loadkey "(8)."
.PP
ARPA2 Common is documented on
.IR http://common.arpa2.net/
and its code is on
.IR https://gitlab.com/arpa2/arpa2common/
