.TH A2ID-LOADKEY 1 "June 2021" "ARPA2.net" "System Management Commands"
.SH NAME
a2id-loadkey \- Preload a key for ARPA2 Signed Identities
.SH SYNOPSIS
.B a2id-loadkey
.IR keyname " " fd " " program " " args "..."
.PP
.B a2id-loadkey
.IR /path/to/file.key " " fd " " program " " args "..."
.SH DESCRIPTION
.PP
Before running the
.IR program ,
preload the key for ARPA2 Identity Signing into
file descriptor
.IR fd ,
which
.I program
can only read once.  To this end, the key is passed through
a pipeline from
.B a2id-loadkey
to
.IR program .
.PP
It is possible for the
.I program
to impose further restrictions before reading the pipeline,
but it cannot reverse the single attempt to read from it.
.PP
When the form with a
.I keyname
is used, the directory
.B /var/lib/arpa2/identity
is used as a key repository for the system,
with files only accessible to the root user.
An extension
.b .key
is then added to the
.IR keyname .
It is customary to have a key named
.B default
stored in
.B /var/lib/arpa2/identity/default.key
to serve most purposes.
.PP
Applications must load keys explicitly, and the default
key store is merely a convention to get that started.
The function
.BR a2id_addkey ()
is used to setup a new key.  Programs may open a file
descriptor to the key(s) to pass and harvest them after
dropping privileges.  An even stronger mechanism is to
do this key passing through a pipe, into which it fits
and from which it can only be read once by the program
at a lower privilege.
.PP
Multiple keys may be loaded, for
instance, to permit verification of signatures with older
keys, while the
.B default.key
is used for signing.  To achieve this, invoke
.B a2id-loadkey
again as part of the
.IR program .
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/arpa2common/
project page for ARPA2 Common.  We use its issue tracker
to track bugs and feature requests.
.SH COPYRIGHT
.PP
Copyright \(co 2021 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turn receives
funding from various sources.  This particular work was funded
through a grant of SIDNfonds, a public interest fund geared
towards a better Internet for all, and paid by administration
fees for .nl domain names.
.SH "SEE ALSO"
.IR a2id-keygen "(8)."
.PP
ARPA2 Common is documented on
.IR http://common.arpa2.net/
and its code is on
.IR https://gitlab.com/arpa2/arpa2common/
