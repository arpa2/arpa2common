.TH INOUT 1 "September 2024" "ARPA2.net" "User Commands"
.SH NAME
inout \- Replace stdin, stdout, stderr and then run a command
.SH SYNOPSIS
.PP
\fBinout\fR    \fInewin\fR \fInewout\fR        \fIcommand\fR [\fIarg\fR...]
.PP
\fBinouterr\fR \fInewin\fR \fInewout\fR \fInewerr\fR \fIcommand\fR [\fIarg\fR...]
.SH DESCRIPTION
The command
.BI inout (1)\fR,
which may alternatively be installed as
.BR arpa2inout " or " a2inout\fR,
replaces its \fBstdin\fR and \fBstdout\fR file handles with
the ones described by \fInewin\fR and \fInewout\fR respectively,
and then uses
.BI exec (3)
to run \fIcommand\fR \fIarg\fR...
.PP
The name may actually be aliased to hold a mixture of the
substrings \fBin\fR, \fBout\fR and \fBerr\fR to replace the
respective standard file handle with a \fInewxxx\fR handle.
Replacement will always be in this order, and it is advised
to retain that order in the filename, by way of suggestion.
.SH NEW HANDLES
The redirection notations that may be used for
.IR newin ", " newout " and " newerr
vary on the basis of their first character:
.TP
.B -
The file handle is left unchanged.
.TP
.BI / path / to / file
The file is opened readonly in \fInewin\fR or otherwise writeonly.
In the latter case, any prior content will be overwritten.
.TP
.I (empty)
The empty string produces no input to \fInewin\fR and quietly
drops anything sent to it from \fInewout\fR or \fInewerr\fR;
it works like \fB/dev/null\fR.
.TP
.BI : port
The port number is opened as a TCP socket on the \fB::1\fR host.
The socket is a client in \fInewin\fR and a server in the others,
so an output stream will be blocked until it is being read.
This means that some degree of concurrency may be required.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/arpa2common/
project page for ARPA2 Common.  We use its issue tracker
to track bugs and feature requests.
.SH COPYRIGHT
.PP
Copyright \(co 2024 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turn receives
funding from various sources.  Part of this work was funded
through a grant of SIDNfonds, a public interest fund geared
towards a better Internet for all, and paid by administration
fees for .nl domain names.  Other parts were funded by
NLnet, a public interest fund geared towwards open source
excellence, and by the NGI Pointer fund of the European
Union.
.SH "SEE ALSO"
ARPA2 Common is documented on
.IR http://common.arpa2.net/
and its code is on
.IR https://gitlab.com/arpa2/arpa2common/
