.TH BASE85 1 "September 2024" "ARPA2.net" "User Commands"
.SH NAME
base85 \- Encode and decode BASE85 representations of files
.SH SYNOPSIS
.PP
.BR "base85 " [ -e "] [" -p [ p "]] [" -w\fInum\fB "] [" -o "\fIfile\fR]... [" -- "] [" \fIfile ...]
.PP
.BR "base85 -d" " [" -i "] [" -o "\fIfile\fR]... [" -- "] [" \fIfile ...]
.PP
.BR "base85 -de" | -dx | -xe " [" -i "] [" -p [ p "]] [" -w\fInum\fB "] [" -o\fIfile "]... [" -- "] [" \fIfile ...]
.PP
.B base85 -h
.SH DESCRIPTION
The command
.BI base85
encodes data into and decodes data from BASE85 representation.
Encoding is default.  When encoding and decoding are both
requested, then BASE85 will first be decoded and then
re-encoded, for instance with other layout settings.
.PP
The first form encodes into BASE85, which is the default.
The \fB-p\fR or \fB-pp\fR option and \fB-w\fInum\fR influence
the representation around the BASE85 characters.
.PP
The second form decodes from BASE85, with \fB-i\fR to alter
the input process.
.PP
The third form decodes and then encodes, so it works like a
recoder.  The options are a mixture of the ones for the
previous forms, where \fB-x\fR marks the non-BASE85 side for
hexadecimal coding instead of the usual binary coding.
.PP
The fourth form asks for brief usage information.
.PP
Options also have long forms, as listed below.
.PP
The \fIfile\fR... list indicates input files with data to chew
on.  When absent, the standard input is read.  Output is always
sent to standard output.
.SH OPTIONS
The following options are available:
.TP
.BR -e ", " --encode
Ask for encoding into BASE85.  When combined with \fB-d\fR, this
triggers re-encoding.  When neither is specified, \fB-e\fR is
assumed.
.TP
.BR -d ", " --decode
Ask for decoding from BASE85.  When combined with \fB-e\fR, this
triggers re-encoding.
.TP
.BR -x ", " --hex
Use hexadecimal instead of binary input or output for the
non-BASE85 side.  The layout controls apply similar to what
they do when formatting BASE85.
.TP
.BR -o\fIfile ", " --out " \fIfile"
Send output to the \fIfile\fR.  This overrides the default behaviour
of sending all output to standard output.  This option must be used
precisely as often as the number of input files.  When no explicit
input files are named at the end of the command line, standard input
counts as a single input file to be matched with one \fI-o\fR option
or none.
.TP
.BR -p ", " --pretty
Ask for pretty printing.  This inserts a space after every block
of 16 bytes.  Specify the option twice for an extra space after
every chunk of 4 bytes, causing a double space between blocks.
.TP
.BR -w\fInum\fB ", " --wrap=\fInum\fR
Wrap to the next line after \fInum\fR blocks of 16 bytes.
.TP
.BR -i ", " --ignore-garbage
Quietly drop any characters that are not in the BASE85 alphabet.
This option is not required to tolerate certain whitespace
characters, namely space, horizontal tab, carriage return,
line feed, form feed or \fB\\ \\t\\r\\n\\f\fR.
.TP
.BR -h ", " -? ", " --help
Show usage information with short options.
.SH ALGORITHM
BASE85 is more compact than BASE64, and it aligns better with
fixed-size binary data such as keys and hashes.  Where BASE64
aligns to multiples of 3 bytes, BASE85 aligns to multiples of
4 bytes, the latter being common with such kinds of binary data.
.PP
It is however possible to encode any number of bytes with BASE85,
from zero up.  The encoded size is 1.25 times the binary size,
rounded up.  Not all encoded sizes translate to proper input,
but there is no padding to trigger additional implementation
problems; separation can be done with any other ASCII character,
including spaces, tabs, line feeds, carriage return, form feed.
.PP
The alphabet for this ARPA2 encoding in BASE85 is set to
.br

.BR #()*+,-./0 .. 9:;<=>?@A .. Z[]^_a .. z{}~

.br
which, in the order of the ASCII table, represent values 0..84.
This set was chosen to have no commonly used escape or quote
characters, so it should be usable in shells and in URLs.
.PP
Groups of 5 BASE85 character values are raised to powers of 85,
the most significant value first.  When these are mapped to 4 bytes,
it is done in network byte order (but it is just as possible to
load it into a \fBuint32_t\fR for machine processing).
.PP
The BASE85 encoding of zero stands out clearly as a sequence
of \fB#####\fR characters.  It is not true however that a binary
sequence with all bits set stands out just as well as a sequence
of \fB~~~~~\fR characters, because the highest value 85^5 lies
a little over 2^32.  A single byte \fB0xff\fR for example, is
represented in BASE85 as \fBzz\fR.
.PP
Groups shorter than 5 characters are considered to represent a prefix
of the 4 bytes; 2 characters for 1 byte, 3 characters for 2 bytes
and 4 characters for 3 bytes.  A single character does not
contain sufficient information to form a byte.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/arpa2common/
project page for ARPA2 Common.  We use its issue tracker
to track bugs and feature requests.
.SH COPYRIGHT
.PP
Copyright \(co 2024 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turn receives
funding from various sources.  Part of this work was funded
through a grant of SIDNfonds, a public interest fund geared
towards a better Internet for all, and paid by administration
fees for .nl domain names.  Other parts were funded by
NLnet, a public interest fund geared towwards open source
excellence, and by the NGI Pointer fund of the European
Union.
.SH "SEE ALSO"
ARPA2 Common is documented on
.IR http://common.arpa2.net/
and its code is on
.IR https://gitlab.com/arpa2/arpa2common/
